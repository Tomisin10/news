from application.wrappers import instagram, twitter, medium


def task_say_hello():
    print('HELLO from background...')


def task_push_to_social_media(title, body, image_url, tags):
    networks_and_modules = {
        'instagram': instagram,
        'twitter': twitter,
        'medium': medium
    }

    for network, module in networks_and_modules.items():
        module.update_status(title, body, image_url)
