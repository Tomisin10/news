from datetime import datetime, timedelta
import html
import json
import re
from decimal import Decimal

from sqlalchemy import UniqueConstraint
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declared_attr

from application.core import db, errors, logger
from application.core.constants import ACTIVE_STATUS_ID, DELETED_STATUS_ID
from application.core.constants import ARTICLE_FETCH_CAP
from application.core.utils.contexts import (
    current_api_ref, current_request_data)
from application.core.utils.helpers import generate_unique_reference


EMPTY_AMOUNT = Decimal('0.00')
NEW_WALLET_STARTING_BALANCE = EMPTY_AMOUNT
OTP_EXPIRY_PERIOD = timedelta(seconds=120)
AMOUNT_FIELD = db.DECIMAL(12, 2)
PERCENTAGE_FIELD = db.DECIMAL(12, 2)
TENURE_FIELD = db.DECIMAL(12, 2)


def _json_dump_current_request_data():
    try:
        # dump a string representation of the request data (payload) to DB
        return json.dumps(current_request_data())
    except TypeError:
        logger.error('Error JSON dumping request data', exc_info=True)
        raise errors.BadRequest('Could not parse request data')
    finally:
        return None


def _handle_db_commit_error(error_msg, error_to_raise=None):
    logger.error(error_msg, exc_info=True)
    db.session.rollback()
    if error_to_raise is not None:
        pass
        # raise error_to_raise()


def _commit_to_db(error_msg, error_to_raise=None):
    try:
        db.session.commit()
    except:
        _handle_db_commit_error(error_msg, error_to_raise)


class LookUp(object):
    name = db.Column(db.String(64), unique=True)
    description = db.Column(db.String(128))


class HasStatus(object):
    @declared_attr
    def status_id(cls):
        return db.Column(
            db.Integer, db.ForeignKey('statuses.id'),
            default=ACTIVE_STATUS_ID)


class BaseModel(db.Model, HasStatus):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(64), default=generate_unique_reference)
    created_at = db.Column(db.DateTime, default=datetime.now)

    def save(self, _commit=True, _error_msg=None, _raise=SQLAlchemyError):
        db.session.add(self)

        if _commit:
            _commit_to_db(
                error_msg=_error_msg or 'Error persisting item <{0}> '
                                        'to database!'
                                        ''.format(self.__class__.__name__),
                error_to_raise=_raise
            )

    def update(self, _commit=True, _error_msg=None, _raise=SQLAlchemyError,
               **kwargs):

        for k, v in kwargs.items():
            setattr(self, k, v)
        if _commit:
            _commit_to_db(
                error_msg=_error_msg or 'Error updating item <{0}> '
                                        'in database!'
                                        ''.format(self.__class__.__name__),
                error_to_raise=_raise
            )

    def delete(self):
        db.session.delete(self)
        _commit_to_db(
            error_msg='Error updating item <{0}> in database!'
                                        ''.format(self.__class__.__name__)
        )
        # deleted_status_orm = Status.get(name='Deleted')
        # self.update(status_id=deleted_status_orm.id)

    def as_json(self, keys_to_exclude=None):
        return NotImplementedError

    @classmethod
    def get(cls, _desc=False, **kwargs):
        query = cls.query.filter_by(**kwargs)

        if _desc:
            query = query.order_by(cls.id.desc())

        return query.first()

    @classmethod
    def prepare_query_for_active(cls, _desc=True, **params):
        prep_query = (
            cls.query.filter_by(
                **params
            ).filter(
                cls.status_id.in_([ACTIVE_STATUS_ID, None])
            )
        )

        if _desc:
            prep_query.order_by(
                cls.id.desc()
            )

        return prep_query


class APILog(BaseModel):
    __tablename__ = 'api_logs'

    api_ref = db.Column(db.String(64), default=current_api_ref, index=True,
                        unique=True)
    endpoint = db.Column(db.String(1024))
    request_headers = db.Column(db.String(2000))
    request_data = db.Column(db.String(2000),
                             default=_json_dump_current_request_data)
    response_data = db.Column(db.Text)


class Article(BaseModel):
    __tablename__ = 'articles'
    __table_args__ = (
        UniqueConstraint(
            'article_title', 'section_id', name='article_unique_index'),
    )

    article_title = db.Column(db.String(512))
    article_body = db.Column(db.TEXT)
    article_image_url = db.Column(db.TEXT)
    article_video_url = db.Column(db.TEXT)
    article_url = db.Column(db.TEXT)
    author_id = db.Column(db.Integer, db.ForeignKey('authors.id'))
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))
    news_source_id = db.Column(db.Integer, db.ForeignKey('news_sources.id'))

    author = db.relationship(
        'Author', backref=db.backref('articles', uselist=True), lazy='joined')
    section = db.relationship(
        'Section', backref=db.backref('articles', uselist=True), lazy='joined')
    news_source = db.relationship(
        'NewsSource', backref=db.backref('articles', uselist=True),
        lazy='joined')

    @property
    def filtered_title(self):
        title = self.article_title

        if title is None:
            return title

        if '|' in title:
            title = title.split('|')[1]

        regex_pattern = re.compile(r'<.*?>')
        clean_title = regex_pattern.sub('', title)
        clean_title = html.unescape(clean_title)

        return clean_title

    @property
    def filtered_body(self):
        body = str(self.article_body)
        body = html.unescape(body)

        if body is None:
            return

        regex_pattern = re.compile(r'<.*?>')
        clean_body = regex_pattern.sub('', body)

        return clean_body

    def as_json(self):
        return {
            'created_at': self.created_at.isoformat(),
            'id': self.id,
            'title': self.filtered_title,
            'body': self.filtered_body,
            'image_url': self.article_image_url,
            'video_url': self.article_video_url,
            'section': self.section.as_json(),
            'source': self.news_source.as_json(),
            'url': self.article_url,
            'uid': self.uid or self.id
        }


class Author(BaseModel):
    __tablename__ = 'authors'

    fullname = db.Column(db.String(64))
    disruptiv_platform_id = db.Column(
        db.Integer, db.ForeignKey('disruptiv_platforms.id'))


class BriefMessage(BaseModel):
    __tablename__ = 'brief_messages'

    title = db.Column(db.String(64))
    body = db.Column(db.TEXT)
    channel = db.Column(db.Enum('SMS', 'EMAIL', name='message_channel'))

    reader_id = db.Column(db.Integer, db.ForeignKey('readers.id'))

    reader = db.relationship(
        'Reader', backref=db.backref('messages', uselist=True),
        lazy='joined')

    def as_json(self):
        return {
            'title': self.title,
            'body': self.body
        }


class Subscription(BaseModel):
    __tablename__ = 'subscriptions'

    subscription_category = db.Column(
        db.Enum('SECTION', 'SOURCE', name='subscription_categories'))

    event_location_id = db.Column(
        db.Integer, db.ForeignKey('event_locations.id'))
    reader_id = db.Column(db.Integer, db.ForeignKey('readers.id'))
    news_source_id = db.Column(db.Integer, db.ForeignKey('news_sources.id'))
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))

    event_location = db.relationship(
        'EventLocation', backref=db.backref('event_locations', uselist=True),
        lazy='joined'
    )
    reader = db.relationship(
        'Reader', backref=db.backref('subscriptions', uselist=True),
        lazy='joined')
    news_source = db.relationship(
        'NewsSource', backref=db.backref('subscriptions', uselist=True),
        lazy='joined')
    section = db.relationship(
        'Section', backref=db.backref('subscriptions', uselist=True),
        lazy='joined')

    @property
    def url(self):
        if self.brief_source_category.name == 'SECTION':
            return self.section.url
        elif self.brief_source_category.name == 'SOURCE':
            return self.news_source.url

    def as_json(self):
        return {
            'event_location': self.event_location.name,
            'news_source': getattr(self.news_source, 'uid', None),
            'reader': self.reader.uid,
            'section': getattr(self.section, 'uid', None)
        }


class Conversation(BaseModel):
    __tablename__ = 'conversations'

    reader_id = db.Column(db.Integer, db.ForeignKey('readers.id'))

    reader = db.relationship(
        'Reader', backref=db.backref('conversations', uselist=True),
        lazy='joined')


class DisruptivPlatform(BaseModel, LookUp):
    """Records of all Disruptiv's children platforms (e.g Aproko, etc)"""
    __tablename__ = 'disruptiv_platforms'

    disruptiv_platform_category_id = (
        db.Column(db.Integer, db.ForeignKey(
            'disruptiv_platform_categories.id'))
    )

    disruptiv_platform_category = db.relationship(
        'DisruptivPlatformCategory', backref=db.backref(
            'disruptiv_platforms', uselist=True), lazy='joined'
    )

    def as_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }


class DisruptivPlatformCategory(BaseModel, LookUp):
    """Bot, Web, USSD"""
    __tablename__ = 'disruptiv_platform_categories'


class Event(BaseModel, LookUp):
    __tablename__ = 'events'
    __table_args__ = (
        UniqueConstraint('name', 'event_location_id',
                         name='event_constraint'),
    )

    date = db.Column(db.DateTime)
    name = db.Column(db.String(512))
    venue = db.Column(db.TEXT)

    event_location_id = db.Column(
        db.Integer, db.ForeignKey('event_locations.id'))

    event_location = db.relationship(
        'EventLocation', backref=db.backref('events'))

    @property
    def filtered_title(self):
        return self.name

    def as_json(self):
        return {
            'name': self.name,
            'description': self.description,
            'date': self.date.isoformat(),
            'event_location': self.event_location.name
        }


class EventLocation(BaseModel, LookUp):
    __tablename__ = 'event_locations'

    city = db.Column(db.String(32))
    country = db.Column(db.String(32))

    @hybrid_property
    def name(self):
        return '{}, {}'.format(self.city, self.country)


class Job(BaseModel, LookUp):
    __tablename__ = 'jobs'


class Message(BaseModel):
    __tablename__ = 'messages'

    conversation_id = db.Column(db.Integer,
                                db.ForeignKey('conversations.id'))
    origin = db.Column(db.Enum('RECEIVED', 'SENT', name='origin'))
    content = db.Column(db.TEXT)

    conversation = db.relationship(
        'Conversation', backref=db.backref('messages', uselist=True),
        lazy='joined')


class NewsSource(BaseModel, LookUp):
    """URL"""
    __tablename__ = 'news_sources'
    __table_args__ = (
        UniqueConstraint('section_id', 'url',
                         name='news_source_constraint'),
    )

    url = db.Column(db.String(1024))
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))
    category = db.Column(
        db.Enum('NEWS_API', 'RSS', 'WORDPRESS', name='news_sources_types'))

    section = db.relationship(
        'Section', backref=db.backref('news_sources'), lazy='joined')

    @property
    def title(self):
        if self.category == 'NEWS_API':
            title = self.url.split('?sources=')[1].split('&')[0].replace(
                '-', ' ')

        else:
            title = self.url.split('://')[1].split('.')

            title = title[0] if '/' in title[1] else title[1]

        return title

    def as_json(self):
        return {
            'id': self.id,
            'uid': self.uid or self.id,
            'url': self.url,
            'title': self.title,
            'category': self.category,
            'section': self.section.name
        }


class OauthCredentials(BaseModel):
    __tablename__ = 'oauth_credentials'

    app_name = db.Column(db.String(16), nullable=False)
    oauth_token = db.Column(db.String(128), nullable=False)
    oauth_token_secret = db.Column(db.String(128), nullable=False)


class Payment(BaseModel):
    __tablename__ = 'payments'

    amount = db.Column(db.DECIMAL(12, 2), default='0.00')

    username = db.Column(db.String(24))
    product_name = db.Column(db.String(64))
    phone = db.Column(db.String(20))
    currency_code = db.Column(db.String(10))
    meta = db.Column(db.TEXT)
    provider_channel = db.Column(db.String(10))
    environment = db.Column(db.String(10))


class PendingSubscription(BaseModel):
    __tablename__ = 'pending_subscriptions'

    phone = db.Column(db.String(20))

    event_location_id = db.Column(
        db.Integer, db.ForeignKey('event_locations.id'))
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))
    news_source_id = db.Column(db.Integer, db.ForeignKey('news_sources.id'))

    event_location = db.relationship('EventLocation')
    section = db.relationship('Section')
    news_source = db.relationship('NewsSource')


class PushedArticle(BaseModel):
    __tablename__ = 'pushed_articles'
    __table_args__ = (
        db.UniqueConstraint(
            'app_name', 'article_id', name='pushed_articles_unique_index'),
    )

    app_name = db.Column(db.String(12))
    article_id = db.Column(db.Integer, db.ForeignKey('articles.id'))


class Section(BaseModel, LookUp):
    __tablename__ = 'sections'

    @property
    def _articles(self):
        return Article.prepare_query_for_active(
            section_id=self.id
        ).order_by(
            Article.id.desc()
        ).limit(
            ARTICLE_FETCH_CAP
        ).all()

    @property
    def _latest_article(self):
        return Article.prepare_query_for_active(
            section_id=self.id
        ).order_by(
            Article.id.desc()
        ).first()

    def as_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'uid': self.uid or self.id
        }


class SocialMediaAccount(LookUp, BaseModel):
    __tablename__ = 'social_media_accounts'

    meta = db.Column(db.TEXT)

    app_name = db.Column(db.String(64))
    username = db.Column(db.String(128))
    oauth_token = db.Column(db.String(64))
    oauth_token_secret = db.Column(db.String(64))
    client_token = db.Column(db.String(64))
    client_token_secret = db.Column(db.String(64))


class Status(BaseModel, LookUp):
    __tablename__ = 'statuses'


class Tag(BaseModel, LookUp):
    __tablename__ = 'tags'

    article_id = db.Column(db.Integer, db.ForeignKey('articles.id'))

    article = db.relationship(
        'Article', backref=db.backref('tags', uselist=True), lazy='joined')


class USSDConversation(BaseModel, LookUp):
    __tablename__ = 'ussd_conversations'

    phone = db.Column(db.String(16))
    request = db.Column(db.TEXT)
    response = db.Column(db.TEXT)
    session_id = db.Column(db.String(32))


class Reader(BaseModel):
    __tablename__ = 'readers'
    __table_args__ = (
        UniqueConstraint('email', 'phone', 'disruptiv_platform_id',
            name='reader_constraint'),
    )

    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    email = db.Column(db.String(64))
    phone = db.Column(db.String(64))
    meta = db.Column(db.TEXT)

    disruptiv_platform_id = (
        db.Column(db.Integer, db.ForeignKey('disruptiv_platforms.id'),
                  nullable=False))

    disruptiv_platform = db.relationship(
        'DisruptivPlatform', backref=db.backref('readers', uselist=True),
        lazy='joined')

    def as_json(self):
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'phone': self.phone,
            'disruptiv_platform': self.disruptiv_platform.as_json(),
            'uid': self.uid
        }


class VideoSource(BaseModel, LookUp):
    __tablename__ = 'video_sources'

    def as_json(self):
        return {
            'id': self.id,
            'name': self.name
        }


class VideoChannel(BaseModel):
    __tablename__ = 'video_channels'

    name = db.Column(db.String(64))

    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))
    video_source_id = db.Column(db.Integer, db.ForeignKey('video_sources.id'))
    channel_uid = db.Column(db.String(64))

    section = db.relationship(
        'Section', backref=db.backref('video_channels', uselist=True),
        lazy='joined')
    video_source = db.relationship(
        'VideoSource', backref=db.backref('video_channels', uselist=True),
        lazy='joined')

    def as_json(self):
        return {
            'channel_uid': self.channel_uid,
            'name': self.name,
            'section': self.section.as_json(),
            'video_source': self.video_source.as_json()
        }


class Video(BaseModel):
    __tablename__ = 'videos'

    title = db.Column(db.String(128))
    description = db.Column(db.String(512))
    channel_name = db.Column(db.String(64))
    video_uid = db.Column(db.String(128), unique=True)
    thumbnail_url = db.Column(db.TEXT)

    video_channel_id = db.Column(
        db.Integer, db.ForeignKey('video_channels.id'))

    video_channel = db.relationship(
        'VideoChannel', backref=db.backref('videos', uselist=True),
        lazy='joined')

    def as_json(self):
        return {
            'title': self.title,
            'description': self.description,
            'channel_name': self.channel_name,
            'video_uid': self.video_uid,
            'thumbnail_url': self.thumbnail_url
        }
