import json
import random

from application import cache
from application.core.models import Section, Video, VideoChannel, VideoSource
from application.core.constants import CACHE_LIFESPAN
from application.wrappers.youtube import youtube_search


def get_popular_youtube_videos(page, per_page):
    cached = cache.get('popular-youtube-videos')

    if cached is None:
        video_source_youtube_orm = VideoSource.get(name='YouTube')

        all_videos = []
        for section in Section.query.all():
            pagination = Video.prepare_query_for_active().join(
                Video.video_channel
            ).filter(
                VideoChannel.video_source_id == video_source_youtube_orm.id,
                VideoChannel.section_id == section.id
            ).paginate(page, per_page)

            all_videos.extend(pagination.items)

        random.shuffle(all_videos)

        all_video_json = [video.as_json() for video in all_videos]

        cache.set(
            'popular-youtube-videos',
            all_video_json,
            CACHE_LIFESPAN)

        return all_video_json[:per_page]

    return cached


def get_video_headlines(section=None, source=None):
    search_term = 'CNN'

    if section:
        search_term = section.name

    elif source:
        search_term = source.title

    cached_videos = cache.get('{}-videos'.format(search_term))
    if cached_videos is None:
        videos = youtube_search(q=search_term, max_results=25)['videos']
        cache.set('{}-videos'.format(search_term), videos, CACHE_LIFESPAN)
        return videos

    return cached_videos
