from flask.views import MethodView

from application.core import errors
from application.core.models import NewsSource, Reader, Section, Subscription
from application.core.utils.contexts import current_request_data
from application.core.utils.request_response_helpers import api_created_response


class Subscriptions(MethodView):
    @staticmethod
    def validate_subscription_creation_params(params):
        required_params = {'section_uid'}
        missing_params = required_params - set(params.keys())

        if missing_params:
            raise errors.BadRequest(
                'Send all of {}'.format(missing_params))

    def delete(self, reader_uid, subscription_uid):
        pass

    def get(self, reader_uid, subscription_uid):
        pass

    def patch(self, reader_uid, subscription_uid):
        pass

    def post(self, reader_uid):
        request_data = current_request_data()

        self.validate_subscription_creation_params(request_data)

        reader = Reader.get(uid=reader_uid)
        if reader is None:
            raise errors.ResourceNotFound('Reader not found')

        section = Section.get(uid=request_data.get('section_uid'))
        source = NewsSource.get(uid=request_data.get('news_section_uid'))

        print(section, bool(source))

        if bool(section) == bool(source) == False:
            raise errors.ResourceNotFound(
                'Send valid `section_uid` or `news_section_uid`')

        subscription = Subscription(
            reader_id=reader.id
        )

        if section:
            subscription.section = section
        if source:
            subscription.news_source = source

        subscription.save()

        return api_created_response(subscription.as_json())
