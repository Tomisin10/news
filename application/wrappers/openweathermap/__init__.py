import json

import requests


def _convert_kelvin_to_celcius(k_value):
    return round(k_value - 273)


def get_weather(city, country_shortform):
    url = (
        'http://api.openweathermap.org/data/2.5/weather?q={city},'
        '{country_shortform}&appid=b6907d289e10d714a6e88b30761fae22&'
        'apiKey=dce143f0892f2bea61c3c0a72c0fce3f'.format(
            city=city, country_shortform=country_shortform
        )
    )

    r = requests.get(url).json()

    r['main']['temp'] = _convert_kelvin_to_celcius(r['main']['temp'])
    r['main']['temp_max'] = _convert_kelvin_to_celcius(r['main']['temp_max'])
    r['main']['temp_min'] = _convert_kelvin_to_celcius(r['main']['temp_min'])

    return r


if __name__ == '__main__':
    print(get_weather(city='Lagos', country_shortform='NG'))
