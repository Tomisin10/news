import os


BROKER_URL = 'redis://localhost:6379'
CELERY_INCLUDE = ['tasks']
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
DEBUG = True
EVENTBRITE_OAUTH_TOKEN=os.environ['EVENTBRITE_OAUTH_TOKEN']
YOUTUBE_DEVELOPER_KEY=os.environ['YOUTUBE_DEVELOPER_KEY']

INSTAGRAM_USERNAME = 'disruptiv_'
INSTAGRAM_PASSWORD = os.environ['INSTAGRAM_PASSWORD']

SQLALCHEMY_TRACK_MODIFICATIONS = False


if os.environ['RUNNING_MODE'] == 'development':
    SQLALCHEMY_DATABASE_URI = (
        'mysql+pymysql://tomisin:tomisin10@localhost/disruptiv_development'
    )

elif os.environ['RUNNING_MODE'] == 'production':
    SQLALCHEMY_DATABASE_URI = (
        'postgres://lhomjoicjegfhc:3bc8a77b28d2e066b0703aee580a2af237ffa5f736de2f4cca314ce371c2a41d@ec2-107-20-211-10.compute-1.amazonaws.com:5432/djddknqmqrjr4'
    )

EVENTS_CRAWL_INTERVAL = int(os.environ['EVENTS_CRAWL_INTERVAL'])
MOBILE_PAYMENT_PUSH_INTERVAL = int(os.environ['MOBILE_PAYMENT_PUSH_INTERVAL'])
NEWS_CRAWL_INTERVAL = int(os.environ['NEWS_CRAWL_INTERVAL'])
LATEST_HEADLINES_PUSH_INTERVAL = int(
    os.environ['LATEST_HEADLINES_PUSH_INTERVAL'])

NGBULKSMS_BASE_URL = "http://portal.nigerianbulksms.com/api/"
NGBULKSMS_USERNAME = 'decave.12357@gmail.com'
NGBULKSMS_PASSWORD = 'password'
NGBULKSMS_SENDER = 'Disruptive'
NGBULKSMS_TIMEOUT = 60
SMS_ENABLED = True
