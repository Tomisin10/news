import calendar
from datetime import date, datetime
import time


class DateTimeWrapper(object):
    calendar = calendar

    def __init__(self):
        self.refresh()

    def refresh(self):
        days = [
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
            'Friday', 'Saturday'
        ]

        months = [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November',
            'December'
        ]

        self.days = days
        self.months = months
        self.months_short = [month[:3] for month in months]

        self._week_day_index = datetime.now().weekday()
        self.month_index = datetime.now().month

        self.month = months[(self.month_index - 1) % 12]
        self.month_short = self.month[:3]
        self.year = datetime.now().year
        self.day_no = datetime.now().day
        self.day_no_suffix = ''
        self.day_name = days[(self._week_day_index + 1) % 7]

        self.year_day_no = self.get_year_day(self.year, self.month, self.day_no)
        self.year_week_no = date(int(self.year), int(self.month_index),
                                 int(self.day_no)).isocalendar()[1]

        self.hour = datetime.now().hour if datetime.now().hour else 12
        self.hour_short = self.hour % 12 if datetime.now().hour else 12
        self.hour_short = 12 if not self.hour_short else self.hour_short
        self.min = datetime.now().minute
        self.min = '0%s' if len(str(self.min)) == 1 else self.min

        self.am_pm = 'am' if self.hour < 13 else 'pm'

        if self.day_no > 3 or (self.day_no > 10 and self.day_no < 21) or \
                self.day_no > 23:
            self.day_no_suffix = 'th'
        else:
            self.day_no_suffix = ('th', 'st', 'nd', 'rd')[
                int(str(self.day_no)[-1])]

    def get_month_start(self, year, month):
        return date(year, month, 1).isoweekday()

    def get_year_day(self, year, month, day):
        year = int(year)
        month_no = (self.months.index(month) + 1)
        total_days = int(day)

        if month_no > 1:
            for month in range(1, month_no):
                total_days += calendar.monthrange(year, month)[1]

        return total_days

    def parse(self, datetime_obj, timezone):
        datetime_str_splitted = str(datetime_obj).split(' ')
        date_str_splitted = datetime_str_splitted[0].split('-')
        time_str_splitted = datetime_str_splitted[1].split(':')

        wrapped_date = dict(
            month_no=date_str_splitted[1],
            day_no='11',
            year_no=date_str_splitted[0],
            hour=time_str_splitted[0],
            minute=time_str_splitted[1],
            second=time_str_splitted[2]
        )

        weekday_index = date(
            int(wrapped_date['year_no']),
            int(wrapped_date['month_no']),
            int(wrapped_date['day_no'])
        ).isoweekday() % 7

        wrapped_date['month_name'] = self.months[
            int(wrapped_date['month_no']) - 1]
        wrapped_date['day_name'] = self.days[weekday_index]

        wrapped_date['am_pm'] = 'am' if int(wrapped_date['hour']) < 13 else 'pm'

        wrapped_date['day_no'] = int(wrapped_date['day_no'])

        if ((wrapped_date['day_no'] >= 4 and
            wrapped_date['day_no'] <= 20) or wrapped_date['day_no'] >= 24):
            wrapped_date['day_no_suffix'] = 'th'
        else:
            wrapped_date['day_no_suffix'] = ('th', 'st', 'nd', 'rd')[
                int(str(wrapped_date['day_no'])[-1])]

        return wrapped_date


if __name__ == '__main__':
    print(DateTimeWrapper().get_year_day(2018, 'February', 2))
