import calendar
from functools import wraps
from json import loads
import random

from pymysql.err import IntegrityError as PyMySQLIntegrityError
from requests.exceptions import MissingSchema
from sqlalchemy.exc import IntegrityError as SQLAlchemyIntegrityError

from application.core import ngn_bulk_sms
from application.core.constants import (
    DEFAULT_IMAGE_URL,
    MAX_SMS_LENGTH,
    SUBSCRIPTION_ARTICLE_LIMIT)
from application.core.models import Article, Event, PushedArticle
from application.wrappers import instagram, twitter, medium
from application.core.utils import format_minute, ordinal


networks_and_modules = {
    'instagram': instagram,
    'twitter': twitter,
    # 'medium': medium
}


def _fetch_brief_for_reader(reader):
    subscription_field_articles = {}

    for subscription in reader.subscriptions:
        if subscription.section:
            subscription_field_articles[subscription.section.name] = (
                subscription.section.articles[-SUBSCRIPTION_ARTICLE_LIMIT:]
            )

        elif subscription.news_source:
            subscription_field_articles[subscription.news_source.title] = (
                subscription.news_source.articles[
                -SUBSCRIPTION_ARTICLE_LIMIT:]
            )

        elif subscription.event_location:
            subscription_field_articles[subscription.event_location.name] = (
                subscription.event_location.events[
                    -SUBSCRIPTION_ARTICLE_LIMIT:]
            )

    return subscription_field_articles


def create_brief_for_reader(reader):
    brief = None
    subscription_field_articles = _fetch_brief_for_reader(reader)

    if reader.disruptiv_platform.name == 'USSD':
        brief = ''

        collection_of_articles = subscription_field_articles.values()

        random.shuffle(collection_of_articles)

        for items in collection_of_articles:
            random.shuffle(items)

            for item in items:
                if type(item) is Article:
                    if (len(brief) + len(item.filtered_title)) < MAX_SMS_LENGTH:
                        brief += '{}.\n'.format(item.filtered_title)

                elif type(item) is Event:
                    venue = loads(item.venue)

                    this_msg = '{} @ {} -- {}:{} {} {}'.format(
                        item.name,
                        venue['name'],
                        item.date.hour,
                        format_minute(item.date.minute),
                        ordinal(item.date.day),
                        calendar.month_name[item.date.month])

                    if (len(brief) + len(this_msg)) < MAX_SMS_LENGTH:
                        brief += '{}.\n'.format(this_msg)

    return brief


def brief_sender(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        func(*args, **kwargs)

    return decorated_function


@brief_sender
def send_brief_to_sms_reader(brief, reader):
    ngn_bulk_sms.send_sms(brief, reader.phone)


@brief_sender
def send_brief_to_email_reader(brief, reader):
    return


@brief_sender
def send_brief_to_bot_user(brief, reader):
    return


def push_to_social_media(article):

    for app_name, module in networks_and_modules.items():
        print("\n=================")
        print(
            'Title: {}\nBody: {}\nURL: {}'.format(
                article.article_title, article.article_body,
                article.article_image_url)
        )
        print('About to push to {}'.format(module))

        section_name_splitted = article.section.name.split(' ')
        section_names = [name.title() for name in section_name_splitted]
        section_names.append(''.join(section_names))

        try:
           PushedArticle(app_name=app_name, article_id=article.id).save()

           module.update_status(
               article.article_title, article.article_body,
               article.article_image_url or DEFAULT_IMAGE_URL,
               source_url=article.article_image_url,
               tags=[*section_names, article.news_source.title.title()])

        except (PyMySQLIntegrityError, SQLAlchemyIntegrityError, MissingSchema):
            continue
