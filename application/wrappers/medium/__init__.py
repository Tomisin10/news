import requests

from application.wrappers.libs.medium import Client
from application.wrappers import (get_auth_data,
    create_entry_from_parsed, save_entry_if_new)


APPLICATION_ID = 'b3e773e46e92'
APPLICATION_SECRET = '4ed17ad171463cce575146119467f05bbccb580e'
DEFAULT_OAUTH_TOKEN = ''

DEFAULT_ACCOUNTS = []


def create_client(username=None, access_token=None, default=False):
    client = Client(
        application_id=APPLICATION_ID,
        application_secret=APPLICATION_SECRET)

    if default:
        oauth_token = DEFAULT_OAUTH_TOKEN

    elif access_token:
        oauth_token = access_token['oauth_token']

    else:
        oauth_token = get_auth_data(app_name='medium', username=username)

    client.access_token = oauth_token

    return client


def update_status(username, title, message, image=None):
    client = create_client(username)

    client.create_post(
        user_id=username, title=title, content="<h2>%s</h2><p>%s</p>" % (
            title, message),
        content_format="html", publish_status="draft")


def refresh_expired_token(username):
    client = create_client(username)
    refresh_token = get_auth_data(username)
    client.exchange_refresh_token(refresh_token)


def parse_post(post):
    tweet_time = post['created_at']
    tweet_time_splitted = tweet_time.split(' ')
    tweet_time_splitted.remove(tweet_time_splitted[4])

    parsed_tweet = dict(
        content=str({'text': post['text']}),
        retweeted=post['retweeted'],
        liked=post['favorited'],
        favourites_count=post['favorite_count'],
        retweet_count=post['retweet_count'],
        profile_photo_url=post['user']['profile_image_url_https'],
        author=post['user']['screen_name'].lower(),
        tweet_time=' '.join(tweet_time_splitted),
        app='twitter'
    )

    return parsed_tweet


def get_timeline(twitter_username, linked_trails_account=None):
    m = create_client(twitter_username)

    new_entries = []

    result = None
    while not result:
        try:
            result = the_statuses = m.statuses.user_timeline(
                screen_name=twitter_username)

            for status in the_statuses:
                try:
                    # status['id'] = str(status['id'])
                    extracted_ = parse_post(status)
                    # extracted_['id'] = str(status['id'])

                    entry = create_entry_from_parsed('twitter', extracted_)
                    save_entry_if_new(entry)

                except ImportError:
                    pass

            # cache new entries
            return new_entries

        except (ImportError):
            pass


def update_status(username, message, img=None):
    t = create_client(username)

    if img:
        try:
            url_ = img
            imagedata = requests.get(url_).content

            t_upload = create_upload_client(username)
            id_img1 = t_upload.media.upload(media=imagedata)["media_id_string"]

            return t.statuses.update(
                status=message, media_ids=",".join([id_img1]))

        except:
            print('Error in Twitter.updateStatus')

    else:
        try:
            return t.statuses.update(status=message)
        except:
            return 'Error in Twitter.updateStatus'


def follow_user(username):
    'we should follow users on their sign-in'


def send_dm(message, recipient):
    'do send dm'


def get_avatar(username, save_as):
    t = create_client(username)

    the_statuses = t.statuses.user_timeline(screen_name=username)

    full_url = the_statuses[0]['user']['profile_image_url_https']
    the_page = requests.get(full_url).content

    a = open(save_as, 'wb')
    a.write(the_page)
