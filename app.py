#!/usr/bin/env python3

from app_setup import application, celery  # , configuration_mode


if __name__ == '__main__':
    application.run()
