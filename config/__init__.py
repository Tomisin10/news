class DevelopmentConfig():
    DEBUG = True


class ProductionConfig():
    DEBUG = False


config_modes = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}


def load_configuration(app, mode):
    configuration = config_modes[mode]
    app.config.from_object(configuration)
    app.config.from_pyfile('config.py', silent=True)
