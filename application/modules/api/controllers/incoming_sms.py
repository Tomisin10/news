from flask import request
from flask.views import MethodView

from application.briefs.helpers import (
    create_brief_for_reader,
    send_brief_to_sms_reader)
from application.core.constants import SUBSCRIPTION_FEE
from application.core.models import (
    DisruptivPlatform, PendingSubscription, Section, Subscription)
from application.gateways.africas_talking_wrapper import initiate_mobile_payment
from application.modules.api.controllers.readers import Readers
from application.modules.api.controllers.payments import Payments


class IncomingSMS(MethodView):
    def unsubscribe_reader(self):
        return

    def post(self):
        request_data = request.form

        text = request_data['text']
        self.phone_number = request_data['from']
        self.service_code = request_data['to']

        if text.upper() == 'SUBSCRIBE':
            Payments.create_pending_mobile_payment(
                username='sandbox',
                product_name='My Online Store',
                phone=self.phone_number,
                currency_code='NGN',
                amount=SUBSCRIPTION_FEE,
                meta={}, environment='sandbox'
            )

            # query for the section related with that service code
            section = Section.get(name='NIGERIAN')

            PendingSubscription(
                section=section, phone=self.phone_number).save()

        elif text.upper() == 'UNSUBSCRIBE':
            self.unsubscribe_reader()

        return 'SUCCESS', 200
