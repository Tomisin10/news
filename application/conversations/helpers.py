from flask import g

from .dialogue import Dialogue
from .monologue import Monologue
from .collections import Collections
from application.core import db
from application.core.models import (Subscription, Conversation, Message,
                                     NewsSource)
from application.core.models.helpers import orm_get_section_by_id
from application.wrappers.facebook.helpers import get_user_profile
from application.readers.helpers import (create_new_reader,
                                         add_source_to_reader_briefs,
                                         drop_subscription)


def get_response(sender_id, platform, text=None, attachments=None, nlp=None,
                 sticker_id=None, payload=None, title=None, is_postback=True):

    response = list()

    if text:
        text = text.lower()

    if sticker_id == 369239263222822:  # if thumbs-up
        response.append(('text', Monologue.blush()))

    elif payload:
        if payload == 'GET_STARTED_PAYLOAD':
            user_profile = get_user_profile(sender_id)
            create_new_reader(
                first_name=user_profile.pop('first_name'),
                last_name=user_profile.pop('last_name'),
                uid=user_profile.pop('id'),
                disruptiv_platform=platform)

            welcome = Monologue.welcome()
            for statement in welcome:
                response.append(('text', statement))

            response.append(('generic', Collections.all_news_sources(page=1)))
            response.append(('generic', Collections.all_news_sources(page=2)))
            response.append(('generic', Collections.all_news_sources(page=3)))

        elif payload == 'DISPLAY_ALL_SECTIONS':
            response.append(('text', Monologue.take_to_all_sections()))
            response.append(('generic', Collections.all_sections(page=1)))
            response.append(('generic', Collections.all_sections(page=2)))

        elif payload == 'DISPLAY_ALL_NEWS_SOURCES':
            response.append(('text', Monologue.take_to_all_news_sources()))
            response.append(('generic', Collections.all_news_sources(page=1)))
            response.append(('generic', Collections.all_news_sources(page=2)))
            response.append(('generic', Collections.all_news_sources(page=3)))

        elif payload.startswith('DISPLAY_NEWS_SOURCE'):
            source_id = int(payload.split('__')[1])

            news_source = NewsSource.get(id=source_id)

            order_now_quick_reply = Dialogue.quick_reply(
                title=Monologue.enquire_add_to_brief(news_source.title),
                texts_and_payloads=[
                    ('YES', 'ADD_NEWS_SOURCE_TO_BRIEF__%s' % source_id),
                    ('No', 'NOTHING')
                ]
            )

            response.append(('text', Monologue.take_to_news_source(news_source)))
            response.append(('generic', Collections.news_source(news_source)))
            response.append(('quick_reply', order_now_quick_reply))

        elif payload.startswith('DISPLAY_SECTION'):
            section_id = int(payload.split('__')[1])

            response.append(('text', Monologue.take_to_section()))
            response.append(('generic', Collections.section(section_id)))

        elif payload.startswith('DISPLAY_BRIEF_SOURCES'):
            response.append(('text', Monologue.take_to_all_brief_sources()))
            response.append(('generic', Collections.all_brief_sources()))

        elif payload.startswith('ADD_NEWS_SOURCE_TO_BRIEF'):
            source_id = int(payload.split('__')[1])

            news_source = NewsSource.query.get(source_id)
            add_source_to_reader_briefs(news_source)

            response.append(('text', Monologue.process_completion()))
            response.append(
                ('text', Monologue.direct_to_all_brief_sources())
            )

        elif payload.startswith('DROP_BRIEF_SOURCE'):
            brief_source_id = int(payload.split('__')[1])

            brief_source = Subscription.query.get(brief_source_id)
            drop_subscription(brief_source)
            response.append(
                ('text', Monologue.support_brief_source_drop_decision()))
            response.append(('text', Monologue.process_completion()))

    elif nlp and 'greetings' in list(nlp.keys()):
        response.append(('text', Monologue.greet()))
        response.append(('text', Monologue.take_to_all_sections()))
        response.append(('generic', Collections.all_sections()))

    return response


def save_message(response):
    try:
        # TODO make this get_user_active_conversation
        conversation = g.reader.conversations[0]
    except:
        conversation = Conversation(reader_id=g.reader.id)

    Message(
        content=str(response), conversation_id=conversation.id,
        origin='SENT').save()


def get_last_message():
    try:
        return Message.query.filter(
            Message.conversation_id == g.reader.conversations[0].id
        ).order_by(
            Message.id.desc()
        ).first()

    except IndexError:
        return None


def to_send_response(response):
    last_message = get_last_message()

    if last_message is not None:
        return response[1] != eval(last_message.content)[1]

    return True
