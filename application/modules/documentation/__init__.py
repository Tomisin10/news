from flask import Blueprint
from flask_restplus import Api, Resource

from application.articles.helpers import get_compilation
from application.core import errors, models
from application.core.utils.contexts import (
    current_request_args,
    current_request_data)
from application.core.utils.request_response_helpers import (
    api_failure_response,
    api_success_response)
from application.modules.api import controllers

documentation_blueprint = Blueprint(
    'documentation', __name__, url_prefix='/docs/v1.0')

authorizations = {
    'api-key': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'api-key'
    }
}

api = Api(
    documentation_blueprint, version='1.0', title='Disruptivity API',
    authorizations=authorizations,
    description="""**A News Aggregator API.**
    
_________________________________________________

# INTRODUCTION    
This API documentation contains specifications for public endpoints available on the API.

News content in the responses of this API are not owned by Disruptivity, as their sources are cited within.

Some resources support multiple operations (e.g. creation, update) as may be indicated in each case. Standard HTTP methods are used.

All request data (where applicable) should be sent using the JSON format. All response payloads are equally delivered to the client in the JSON format.

The base URL for accessing the API (and to which all endpoints, as specified in this document, should be appended) is at: http://disruptiv.herokuapp.com/api/v1.0).

When making calls to an endpoint, avoid adding a trailing slash to the URL.
 
___________________________________________________

# AUTHORIZATION
**API Keys** and **IP addresses** restrict third-party access to **Disruptivity API**.

The API Key is required to be sent in the headers, as the value of "api-key".

#### JAVASCRIPT
```javascript
fetch(`${apiUrl}/readers`, {
method: 'POST',
headers: {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'api-key': apiKey
},
body: {
  ...
});
```

#### PYTHON
```python
import requests

requests.post(
    '{}/readers'.format(API_URL),
    headers={'api-key': API_KEY}
).json()
``` 

[Get access credentials.](%s)
 
_____________________________________________________

# CONTENT TYPE
All data posted to the API must be `application/json` encoded.


""" % ('https://disruptiv.herokuapp.com'), default='Test', default_label='')

url_prefix = 'api/v1-0'

# namespace
# articles_namespace = api.namespace(
#     '%s/articles' % url_prefix, description='Articles')
articles_namespace = api.namespace(
    path='%s/articles' % url_prefix,
    name='Articles')

event_namespace = api.namespace(
    path='%s/events' % url_prefix,
    name='Events')

readers_namespace = api.namespace(
    path='%s/readers' % url_prefix, name='Readers')

sections_namespace = api.namespace(
    path='%s/sections' % url_prefix, name='Sections')

sources_namespace = api.namespace(
    path='%s/news-sources' % url_prefix, name='News Sources')

subscriptions_namespace = api.namespace(
    path='%s/readers/<reader_uid>/subscriptions' % url_prefix,
    name='Subscriptions',
    mediatype='application/json')

# parsers
get_article_args_parser = api.parser()

get_events_args_parser = api.parser()
get_events_args_parser.add_argument(
    'city', type=str, help='City',
    location='query', required=True)
get_events_args_parser.add_argument(
    'country', type=str, help='Country',
    location='query', required=True)
get_events_args_parser.add_argument(
    'page', type=int, help='Page',
    location='query')
get_events_args_parser.add_argument(
    'per_page', type=int, help='Per page',
    location='query')

get_subscriptions_args_parser = api.parser()
get_subscriptions_args_parser.add_argument(
    'page', type=int, help='Page',
    location='query')
get_subscriptions_args_parser.add_argument(
    'per_page', type=int, help='Per page',
    location='query')

get_headlines_args_parser = api.parser()
get_headlines_args_parser.add_argument(
    'page', type=int, help='Page',
    location='query')
get_headlines_args_parser.add_argument(
    'per_page', type=int, help='Per page',
    location='query')

create_reader_args_parser = api.parser()
create_reader_args_parser.add_argument(
    'disruptiv_platform_id', type=int,
    help='ID of disruptiv platform being accessed.',
    location='form', required=True
)
create_reader_args_parser.add_argument(
    'first_name', type=str,
    help='First name of the reader.',
    location='form'
)
create_reader_args_parser.add_argument(
    'last_name', type=str,
    help='Last name of the reader',
    location='form'
)
create_reader_args_parser.add_argument(
    'phone', type=str,
    help='Phone number of the reader',
    location='form'
)

create_subscriber_args_parser = api.parser()
create_subscriber_args_parser.add_argument(
    'section_uid', type=str, help='UID of the section to be subscriber for.',
    location='form'
)
create_subscriber_args_parser.add_argument(
    'source_uid', type=str, help='UID of the news source to be subscriber for.',
    location='form'
)

get_sections_args_parser = api.parser()

get_sources_args_parser = api.parser()

get_section_args_parser = api.parser()
get_section_args_parser.add_argument(
    'page', type=int, help='Page',
    location='query')
get_section_args_parser.add_argument(
    'per_page', type=int, help='Per page',
    location='query')

get_source_args_parser = api.parser()
get_source_args_parser.add_argument(
    'page', type=int, help='Page',
    location='query')
get_source_args_parser.add_argument(
    'per_page', type=int, help='Per page',
    location='query')


@api.default_namespace.route(url_prefix)
class Test(Resource):
    def get(self):
        return api_success_response("Welcome to Disruptivity API")


@articles_namespace.route('')
class AllArticles(Resource):
    @api.doc(
        parser=get_article_args_parser
    )
    @api.response(200, 'OK')
    def get(self):
        """Get headlines across all sections"""
        return api_success_response({
            "api_ref": "G6nCJVwruiLWK7w5NEHFLB",
            "code": 200,
            "message": "Request completed successfully.",
            "status": "SUCCESS",
            "your_request": None,
            "your_response": [
                {
                    "body": "But there appeared to be no ballistic missiles at the country's 70th anniversary celebrations.",
                    "created_at": "2018-09-09T09:48:33",
                    "id": 3,
                    "image_url": "https://ichef.bbci.co.uk/news/1024/branded_news/10B5D/production/_103354486_p06kq5wt.jpg",
                    "section": {
                        "id": 1,
                        "name": "WORLD",
                        "uid": "ZAtKKwHXQy3aDT2zEZFL3f"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 32,
                        "section": "WORLD",
                        "title": "bbc news",
                        "uid": "2g2nrNYpc3V7A4jsvXMgPG",
                        "url": "https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey={api_key}"
                    },
                    "title": "North Korea parade shows off military strength",
                    "uid": "uAPjnnKBCgsYTUV6J4dCLe",
                    "url": "http://www.bbc.co.uk/news/world-asia-45463950",
                    "video_url": None
                },
                {
                    "body": "Arsenal will need time to adjust to new manager Unai Emery due to the wholesale changes that have been made at the club, according to former captain Tony Adams.",
                    "created_at": "2018-09-06T16:09:19",
                    "id": 14,
                    "image_url": "https://scripts.24.co.za/img/sites/sport24.png",
                    "section": {
                        "id": 2,
                        "name": "ENGLISH PREMIERSHIP",
                        "uid": "xoGzrXdcCgJmK2Jh7LrLyC"
                    },
                    "source": {
                        "category": "RSS",
                        "id": 16,
                        "section": "ENGLISH PREMIERSHIP",
                        "title": "sport24",
                        "uid": "bmbpUKL7CN6po4Nyq8pbpF",
                        "url": "http://feeds.sport24.co.za/articles/Sport/Soccer/EnglishPremiership/rss"
                    },
                    "title": " Adams: Emery will need time to improve Arsenal",
                    "uid": "grtNec9g5cF4SheAb73ZnB",
                    "url": "https://www.sport24.co.za/Soccer/EnglishPremiership/adams-emery-will-need-time-to-improve-arsenal-20180906",
                    "video_url": None
                },
                {
                    "body": "Eric Cantona believes only Pep Guardiola or himself can restore the \"magic\" missing from Manchester United.",
                    "created_at": "2018-09-09T10:01:01",
                    "id": 34,
                    "image_url": "https://images.cdn.fourfourtwo.com/sites/fourfourtwo.com/files/guardiolamourinho-cropped_1fp2wvprep3zo10ozrsiukf3rf.jpg",
                    "section": {
                        "id": 3,
                        "name": "FOOTBALL",
                        "uid": "XXALNSVEXXtD8qpQJM75HV"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 34,
                        "section": "FOOTBALL",
                        "title": "four four two",
                        "uid": "46zcPuE7pEJSkmXxHMyCkB",
                        "url": "https://newsapi.org/v2/top-headlines?sources=four-four-two&apiKey={api_key}"
                    },
                    "title": "Guardiola or me better for Man United than Mourinho \u2013 Cantona",
                    "uid": "GmfJBb85RCQeNvSESYNcfG",
                    "url": "http://www.fourfourtwo.com/news/guardiola-or-me-better-man-united-mourinho-cantona",
                    "video_url": None
                },
                {
                    "body": "Get the latest Serena Explodes at Ump news, photos, rankings, lists and more on Bleacher Report",
                    "created_at": "2018-09-08T21:52:53",
                    "id": 76,
                    "image_url": "https://static-assets.bleacherreport.com/img/br_630_height.png",
                    "section": {
                        "id": 4,
                        "name": "SPORTS",
                        "uid": "tZJtjfo2oP5kGRLp8UoJ4i"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 35,
                        "section": "SPORTS",
                        "title": "bleacher report",
                        "uid": "bKpPLHRhF9LkSMV9faWGVU",
                        "url": "https://newsapi.org/v2/top-headlines?sources=bleacher-report&apiKey={api_key}"
                    },
                    "title": "Serena Explodes at Ump",
                    "uid": "w3MPf8DKgcKHrAc2jzstUY",
                    "url": "https://bleacherreport.com/serena-explodes-at-ump",
                    "video_url": None
                },
                {
                    "body": "When Peter Ouko was sentenced to death for murder 20 years ago, he was sent to Kenya's Kamiti maximum security prison, notorious for being one of the worst jails in the world.",
                    "created_at": "2018-08-10T11:10:57",
                    "id": 94,
                    "image_url": "https://cdn.cnn.com/cnnnext/dam/assets/160108121625-pete-ouko-1-super-169.jpg",
                    "section": {
                        "id": 5,
                        "name": "AFRICAN",
                        "uid": "xMWt42QHVR8tebd2Xk29Co"
                    },
                    "source": {
                        "category": "RSS",
                        "id": 19,
                        "section": "AFRICAN",
                        "title": "cnn",
                        "uid": "HtSfaSUwDWF4SxtRVKQFJX",
                        "url": "http://rss.cnn.com/rss/edition_africa.rss"
                    },
                    "title": "Peter Ouko is transforming prisoners' lives in Kenya",
                    "uid": "BVmKSS7SuaYmDg4VcyVZZj",
                    "url": "http://rss.cnn.com/~r/rss/edition_africa/~3/KappXeKuvps/index.html",
                    "video_url": None
                },
                {
                    "body": "Cheats and Secrets - Spider-Man PS4: IGN's Spider-Man PS4 cheats and secrets guide gives you the inside scoop into every cheat, hidden code, helpful glitch, exploit, and secret in Spider...",
                    "created_at": "2018-09-09T02:07:37",
                    "id": 111,
                    "image_url": "https://assets1.ignimgs.com/2015/05/27/contentplaceholderpng-967b4c.png",
                    "section": {
                        "id": 6,
                        "name": "GAMING",
                        "uid": "in3Gwt832h8FQeRRNxZKkS"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 43,
                        "section": "GAMING",
                        "title": "ign",
                        "uid": "g6Ra4VLHJmiXZwBncNkpg7",
                        "url": "https://newsapi.org/v2/top-headlines?sources=ign&apiKey={api_key}"
                    },
                    "title": "Cheats and Secrets - Spider-Man PS4 Wiki Guide - IGN",
                    "uid": "7gX9omgyYNaWdABmonJCGD",
                    "url": "https://www.ign.com/wikis/spider-man-ps4/Cheats_and_Secrets",
                    "video_url": None
                },
                {
                    "body": "Mfon Udoh and four other home-based stars have been included in the list of 18-man Super Eagles who will play Liberia in an international friendly in Monrovia on Tuesday. Gernot Rohr has also listed goalkeeper Ikechukwu Ezenwa.",
                    "created_at": "2018-09-09T11:39:12",
                    "id": 273,
                    "image_url": None,
                    "section": {
                        "id": 7,
                        "name": "NIGERIAN",
                        "uid": "43gtFu53HAbvCqYorFSnu6"
                    },
                    "source": {
                        "category": "RSS",
                        "id": 18,
                        "section": "NIGERIAN",
                        "title": "naija",
                        "uid": "2NvfYScHoM6yWckSo8VMPE",
                        "url": "https://www.naija.ng/rss/all.rss"
                    },
                    "title": "5 home-based professionals set for Super Eagles clash with Liberia",
                    "uid": "z5jwxWZsnMn8Fj3yXKfno8",
                    "url": "https://www.naija.ng/1191141-5-home-based-professional-set-super-eagles-clash-liberia.html",
                    "video_url": None
                },
                {
                    "body": "Outfit Details:\nTank: Try here or similar styles here or these Forever 21 options here | Pants: FKSP here (wearing size xs) | Shoes: Prada. Enjoy and have a blessed weekend. xo\n\u00a0\n",
                    "created_at": "2018-08-24T12:03:09",
                    "id": 287,
                    "image_url": "http://stylepantry.com/wp-content/uploads/2018/08/SP4719.jpg",
                    "section": {
                        "id": 8,
                        "name": "NIGERIAN FASHION",
                        "uid": "dqZrAt9UimJtoH79Nt7mJb"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 9,
                        "section": "NIGERIAN FASHION",
                        "title": "stylepantry",
                        "uid": "izdTExyFyL9s5PoJN57eKj",
                        "url": "http://stylepantry.com/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": "One Shoulder Tank + Belted High Waist Windowpane Pants",
                    "uid": "7iQcpNKCRjDY2iaC6LQixG",
                    "url": "http://stylepantry.com/2018/08/24/one-shoulder-tank-belted-high-waist-windowpane-pants/",
                    "video_url": ""
                },
                {
                    "body": "Reinhard Tega \u2013 Fake Friends ft. Victoria Kimani, Nosa, Jesse Jagz | \u201cReverb & Depth\u201d EP OUT NOW!\nThe depth of the groove\u2026\nReinhard\u00a0Tega\u2018s debut producer EP will drive home the point that music from the continent is more than\u00a0Afro beats.\nKicking off \u2018Reverb & Depth\u2018\u00a0is Arewa\u00a0ft.\u00a0Funbi\u00a0which easily is the best feel good song you\u2019ll hear today,\u00a0Reinhard\u00a0calls on\u00a0Victoria Kimani,\u00a0Nosa\u00a0and\u00a0Jesse Jagz\u00a0to help tell the all too familiar tale story of\u00a0two faced\u00a0supporters in\u00a0Fake Friends. For some Afro-house vibes,\u00a0Ruby Gyang\u00a0is on the vocals\u00a0for Happiness Is Free\u00a0and\u00a0TMXO\u00a0delivers all sauce and groove in\u00a0Mama Teresa. Finally, East Africas melodies seals this 5 tracked deal withAnto Neosoul\u00a0&\u00a0Krissy\u00a0on\u00a0My Lover.\nReinhard\u00a0Tega is without doubt one of the continent\u2019s most versatile and talented producers and has worked on various projects with a host of talents including\u00a0Olamide,\u00a0M.I Abaga,\u00a0Koker,\u00a0Ice prince,\u00a0Wizkid,\u00a0Khuli Chana\u00a0and more\u2026\n\nCop the Reverb & Depth EP\n",
                    "created_at": "2018-09-07T19:56:08",
                    "id": 296,
                    "image_url": "https://notjustok.com/wp-content/uploads/2018/09/Reinhard-Tega-Reverb-Depth-Art.png",
                    "section": {
                        "id": 9,
                        "name": "NIGERIAN ENTERTAINMENT",
                        "uid": "B9BwjhYrmpDY3UPu8Ptifa"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 5,
                        "section": "NIGERIAN ENTERTAINMENT",
                        "title": "notjustok",
                        "uid": "MvNNhwUqnoZWEvFY2i9GSB",
                        "url": "http://www.notjustok.com/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": " \u201cReverb & Depth\u201d EP OUT NOW!",
                    "uid": "ALa7SdcCgxAmRNGhKFMe9J",
                    "url": "https://notjustok.com/ep/reinhard-tega-fake-friends-ft-victoria-kimani-nosa-jesse-jagz-reverb-depth-ep-out-now/",
                    "video_url": ""
                },
                {
                    "body": "So do you remember when I said \u201csee you next week\u201d in the last video? well that didn\u2019t happen, since I\u2019m writing this 8 months later! But it\u2019s never too late right?\nLast time we hung on here, I was about to open a pop-up shop for Big Hair No Care! I had filmed it all, but the second the shop opened, oh my God I can\u2019t even begin to tell you: I\u2019ve never felt so busy and overwhelmed!\u00a0\nThe good thing about editing this 8 months later, is to see how far I\u2019ve come ever since. Last summer was a thick fog, watching this I want to hi-5 myself for the hard work, because I tend to forget and like to think that I\u2019m lazy, but I\u2019ve relived it all with this video, and I remember how it felt. It\u2019s also very cringe to watch, I mean this whole shop thing, it was totally crazy!\n\u00a0\nMost people research a project for months, I had never thought about having a shop, I had no idea what it involved or when you\u2019d even start. We relaunched Big Hair No Care last March, I was still in Geneva then and I moved back in June. When we first relaunched, we had our stock in a warehouse in Derbyshire, and they were a disaster. I worked for weeks on the relaunch of Big Hair No Care, at the time I was working alone on it, in Geneva, and heavily pregnant. I took a 2-week break after delivering Hugo, but I had to hurry to get the website up and running because I had stock sitting in a warehouse in England. It took me 4 more months after Hugo was born, to create new assets, rework the packaging, and the supply chain. When we launched in March, I had worked so much on the packaging and it meant a lot to me, yet the warehouse was packing our products in the wrong boxes, messing up orders, we had to replace products for people who didn\u2019t receive their orders.\n\u00a0\nSo on a trip to London, I decided to get an office where I\u2019d ship the orders myself once I\u2019d move back. And that\u2019s when it unravelled ahah: I went to Pop Brixton to visit an office, they had a retail space that I asked to look, out of curiosity. And after that I basically had to have a shop, where I could also fulfil online orders. I applied and I got the keys two weeks before I had to open it. Totally crazy I said!\n\u00a0\nThere\u2019s a bit of me a bit ashamed for rushing things like this and not having a perfect execution as a result, but not only do I owe respect to my own journey so I can\u2019t judge myself harshly, I also know that we had so much fun last summer with this shop. People visited from Africa, the USA, the Netherlands, Portugal, Switzerland, Germany etc. So many of you on a trip in London who came to pay us a visit and with who it was so nice to hang, to get into fascinating conversations about how exciting times were getting for us, about this new wind in sisterhood and women entrepreneurship, about our culture and our dreams to be credited when it\u2019s due, and to be recognized and represented in our respective countries and around the world. It was beautiful.\nI\u2019m also proud of what we\u2019ve managed to accomplish in such little time. I\u2019m going to be honest, when the pop-up opened I was totally clueless about the staff I would need (initially thought I could do it all myself \u2013 JOKES), all the admin it would mean, the stress, and all the fun you\u2019re done having because all you\u2019re doing is managing the logistic (as in trying to keep everything running smoothly). It was tough because I was feeling like I was doing a lot of things at once, but not a single one of them was done properly. And when you\u2019re on the anal side like me, it quickly becomes crippling.\n\u00a0\n8 months later we have a completely different operation that I\u2019ll write on later (took a lot of learning, a lot of disappointment and proper heartache\u2026), we\u2019re opening a 3-day pop-up in Paris in 10 days (copying the same layout of this shop, but this time done better, I really like \u00a0our in-store step-by-step purchase process and want to recreate it wherever we have pop-ups!) and I absolutely can\u2019t wait to bring Big Hair No Care to the city where I was born. I initially thought of having the pop-up for a year in London, but learned whilst having it that it would be much more fun to just have them for short period of times, but in more places.\nSo Paris in ten days, and we\u2019re also opening a dispatch centre in the USA this month, and will probably have a pop-up in Atlanta sometime in April but it\u2019s all TBC for now. And this is what I meant with these videos, to take you on my business journey as I\u2019m trying to learn what it takes to be a businesswoman, to build a successful personal brand, whilst trying to be a good mother (and not killing myself with guilt for loving my career so much) a good wife, friend, and family member. Phew.\n\u00a0\nSo this is not going to be a weekly thing (but we got that by now ahah) as mentioned in the previous video, it wouldn\u2019t be interesting. I have this episode where we opened the shop, and had our launch party (!!). I also took you on my personal brand side with the Marie Claire shoot I was so proud to be part of (Power 30 of 2017 \u2013 say whaat) and my quick 30th lunch (I meant to have a big party for my 30th later in November, along with Hugo\u2019s first birthday. Instead we held my dad\u2019s wake on that weekend \u2013 2017 you\u2019ve been such a mind-blowing one!). I have a bit more footage from last summer, mostly on my personal brand side of things. I\u2019ll release it to take you through what I get up to, but after that I\u2019ll only have episodes for big occasions, like our Paris pop-up for example J ! Wish us luck, because I really want it to set the tone for the rest of our events this year!\n\u00a0\nAgain, THANK YOU so much for all your support, you have NO IDEA how much it means to me, all the comments, messages and emails that you guys send me on a daily basis. I wouldn\u2019t be able to dream the way I do without such support.\n\u00a0\nIf you do have questions, or things you\u2019d love me to share more about, let me know!\n\u00a0\nLots of love,\nFreddie x\n",
                    "created_at": "2018-03-04T14:38:20",
                    "id": 324,
                    "image_url": "",
                    "section": {
                        "id": 10,
                        "name": "FASHION",
                        "uid": "fRSoJpjUMAJmJZByVetvEB"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 12,
                        "section": "FASHION",
                        "title": "freddieharrel",
                        "uid": "7UbmVgaeZDrqtJYzZpCAzY",
                        "url": "http://freddieharrel.com/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": "BIG HAIR, BIGGER DREAMS & BUSINESS UPDATE",
                    "uid": "bzSJHzcFPtNj7wp5xSgZxm",
                    "url": "http://freddieharrel.com/big-hair-bigger-dreams-business-update/",
                    "video_url": ""
                },
                {
                    "body": "British Airways owns the largest flight simulator facility in the UK, which houses 16 of the $13-million machines. Pilots train around 50 hours inside before ever getting into a real plane. We spent only an hour inside to learn how to take off, fly and land a\u2026",
                    "created_at": "2018-09-01T00:00:00",
                    "id": 362,
                    "image_url": "https://amp.businessinsider.com/images/5b893a5f5c5e523a1e8b4bed-1920-960.jpg",
                    "section": {
                        "id": 11,
                        "name": "FINANCE",
                        "uid": "fxkcwwZUrcAxW983cE4ii6"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 20,
                        "section": "FINANCE",
                        "title": "business insider",
                        "uid": "GwztvBG3vfW8Fv6rDsnvFi",
                        "url": "https://newsapi.org/v2/top-headlines?sources=business-insider&apiKey={api_key}"
                    },
                    "title": "British Airways has a $13 million flight simulator that taught us how to take off, fly, and land an airplane",
                    "uid": "L9GxvjC8huM4Vx76KsL5x3",
                    "url": "https://www.businessinsider.com/video-british-airways-13-million-flight-simulator-2018-8",
                    "video_url": None
                },
                {
                    "body": "The Ocean Cleanup\u2019s nearly 2,000-foot boom will collect ocean plastics from the gigantic garbage gyre over the next year.",
                    "created_at": "2018-09-09T15:00:26",
                    "id": 397,
                    "image_url": "https://www.nationalgeographic.com/content/dam/environment/2018/09/the-ocean-cleanup/ocean-cleanup-garbage-patch-01.jpg",
                    "section": {
                        "id": 12,
                        "name": "NATURE & SCIENCE",
                        "uid": "2LCsZc4qtfmRH5NWnZdW75"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 24,
                        "section": "NATURE & SCIENCE",
                        "title": "national geographic",
                        "uid": "bF9rNVSuMwdnML8R9uXhY2",
                        "url": "https://newsapi.org/v2/top-headlines?sources=national-geographic&apiKey={api_key}"
                    },
                    "title": "Floating Trash Collector Set to Tackle Pacific Garbage Patch",
                    "uid": "c5h4rjMMDBLanXuaWdy6HQ",
                    "url": "https://www.nationalgeographic.com/environment/2018/09/ocean-cleanup-plastic-pacific-garbage-patch-news.html",
                    "video_url": None
                },
                {
                    "body": "Ariana Grande is mourning the sudden death of Mac Miller.\r\nThe \"No Tears Left to Cry\" singer took to Instagram on Saturday to share a new picture of her late ex-boyfriend. The...",
                    "created_at": "2018-09-08T21:49:00",
                    "id": 415,
                    "image_url": None,
                    "section": {
                        "id": 13,
                        "name": "ENTERTAINMENT",
                        "uid": "5efQqBHmFvRJbX2VwdvSkc"
                    },
                    "source": {
                        "category": "RSS",
                        "id": 15,
                        "section": "ENTERTAINMENT",
                        "title": "eonline",
                        "uid": "pabeeXhTzKgvEWmtE9TZuB",
                        "url": "http://syndication.eonline.com/syndication/feeds/rssfeeds/topstories.xml"
                    },
                    "title": "Ariana Grande Mourns the Death of Ex-Boyfriend Mac Miller With a Touching Photo",
                    "uid": "rFbu32WaxFRyQjCZDhBErf",
                    "url": "https://www.eonline.com/news/966516/ariana-grande-mourns-the-death-of-ex-boyfriend-mac-miller-with-a-touching-photo?cmpid=rss-000000-rssfeed-365-topstories&utm_source=eonline&utm_medium=rssfeeds&utm_campaign=rss_topstories",
                    "video_url": None
                },
                {
                    "body": "Another domino has fallen in Alex Jones\u2019 media empire. Apple confirmed with TechCrunch this week that it\u2019s pulled the controversial conspiracy theorist/provocateur from the App Store this week, banning the Infowars app over violations to its \u201cobjectionable content\u201d rules. \nSlightly more specifically, the host was determined to have violated the TOS around \u201cdefamatory, discriminatory, or mean-spirited content, including references or commentary about religion, race, sexual orientation, gender, national/ethnic origin, or other targeted groups, particularly if the app is likely to humiliate, intimidate, or place a targeted individual or group in harm\u2019s way.\u201d\nThere\u2019s been a cascade effect with many of the major platforms Jones has used to distribute his video content. Facebook, Google and Spotify have all pulled Infowars content from their respective platforms. This week, Twitter and Periscope banned him after widespread criticism. \nAmong other controversial comments, Jones has come under fire for suggesting that the Sandy Hook shooting, which resulted in the deaths of 20 elementary school students, was a hoax. \n\u00a0\n",
                    "created_at": "2018-09-08T18:20:03",
                    "id": 479,
                    "image_url": "https://techcrunch.com/wp-content/uploads/2018/08/alex-jones-press-2.jpg",
                    "section": {
                        "id": 14,
                        "name": "TECHNOLOGY",
                        "uid": "WkeDHw5V7qYZAzJHnRqZLX"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 1,
                        "section": "TECHNOLOGY",
                        "title": "techcrunch",
                        "uid": "MHvgTA7nAaQfqE54GtvRfL",
                        "url": "https://www.techcrunch.com/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": "Alex Jones\u2019 Infowars gets banned from Apple\u2019s App Store",
                    "uid": "vWZpL36p4oYooKi8FhE9dB",
                    "url": "https://techcrunch.com/2018/09/08/alex-jones-infowars-gets-banned-from-apples-app-store/",
                    "video_url": ""
                },
                {
                    "body": "If you're looking for inspiration for your fall wardrobe, look no further.\r\nNow that New York Fashion Week is off to a good start, celebrities are pulling out the best of their...",
                    "created_at": "2018-09-08T00:39:57",
                    "id": 486,
                    "image_url": None,
                    "section": {
                        "id": 15,
                        "name": "LIFESTYLE",
                        "uid": "AQvapoMZpTLf6eUEbNS4fW"
                    },
                    "source": {
                        "category": "RSS",
                        "id": 14,
                        "section": "LIFESTYLE",
                        "title": "eonline",
                        "uid": "anNS5BXd2CHStvjsLU9DLi",
                        "url": "http://syndication.eonline.com/syndication/feeds/rssfeeds/style.xml"
                    },
                    "title": "7 Celebrity Fashion Trends You Need to Copy Now",
                    "uid": "mpYb8ykjvmzKKfVhnbVnSo",
                    "url": "https://www.eonline.com/news/966615/7-celebrity-fashion-trends-you-need-to-copy-now?cmpid=rss-000000-rssfeed-365-lifestyle&utm_source=eonline&utm_medium=rssfeeds&utm_campaign=rss_lifestyle",
                    "video_url": None
                },
                {
                    "body": "The shooter has been identified as 29-year-old Faisal Hussain.",
                    "created_at": "2018-07-24T22:42:02",
                    "id": 544,
                    "image_url": "https://img.buzzfeed.com/buzzfeed-static/static/2018-07/25/11/enhanced/buzzfeed-prod-web-06/original-10900-1532532412-2.jpg?crop=800:419;0,88",
                    "section": {
                        "id": 16,
                        "name": "COMPILATIONS",
                        "uid": "ix5nzhuKiP6BuLT7uFofxc"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 28,
                        "section": "COMPILATIONS",
                        "title": "buzzfeed",
                        "uid": "eDd37GxsvD3h5bfhTRQSun",
                        "url": "https://newsapi.org/v2/top-headlines?sources=buzzfeed&apiKey={api_key}"
                    },
                    "title": "We Are Tracking All The Rumors About The Toronto Shooting",
                    "uid": "aWnixP3qgge2DjqiHxYQuL",
                    "url": "https://www.buzzfeed.com/ishmaeldaro/rumors-toronto-shooting-true-false-unverified",
                    "video_url": None
                },
                {
                    "body": "The 70th Annual Primetime Creative Arts Emmy Awards ceremony kicked off last night on September 8 with the second event to hold today.\nThe nominations were announced on July 12, 2018.\nThe ceremony is in conjunction with the annual Primetime Emmy Awards and is presented in recognition of technical and other similar achievements in American television programming, including guest acting roles.\nSeveral celebrities attended the event at The Microsoft Theater in Los Angeles.\nSterling K. Brown, Neil Patrick Harris, Jane Lynch, Samira Wiley, Bill Nye, John Stamos,\u00a0Cristin Milioti \u00a0Tichina Arnold and more were spotted.\nSee the photos below.\nJane Lynch\nBill Nye\nSarah Drew\nRon Cephas Jones\nNeil Patrick Harris\nCristin Milioti\nMolly Shannon\nTichina Arnold\nAne Crabtree\nDiarra Kilpatrick\nMadchen Amick\nCicely Tyson\nSamira Wiley\nSonequa Martin-Geen\nJames Corden\nSterling K. Brown\nRyan Michelle Bathe and Sterling K. Brown\nCaitlin McHugh (L) and John Stamos\nPhoto Credit: Getty Images/Alberto E. Rodriguez\n",
                    "created_at": "2018-09-09T11:06:33",
                    "id": 552,
                    "image_url": "https://www.bellanaija.com/wp-content/uploads/2018/09/GettyImages-1029688376.jpg",
                    "section": {
                        "id": 17,
                        "name": "NIGERIAN LIFESTYLE",
                        "uid": "WPYpVRRZUqvy4QL6eMkksR"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 6,
                        "section": "NIGERIAN LIFESTYLE",
                        "title": "bellanaija",
                        "uid": "Awspt9rfBysWuNFxXWHCNJ",
                        "url": "https://www.bellanaija.com/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": "Red Carpet Photos! Samira Wiley, Sterling K. Brown, Neil Patrick Harris attend Day 1 of 2018 Creative Arts Emmy Awards",
                    "uid": "cU3egYzq353p2z3bTeTrFV",
                    "url": "https://www.bellanaija.com/2018/09/red-carpet-photos-samira-wiley-sterling-k-brown-neil-patrick-harris-attend-day-1-2018-creative-arts-emmy-awards/",
                    "video_url": ""
                },
                {
                    "body": "Dash on Saturday continued to trend upwards amid a strong buying sentiment phase that started mid-week.",
                    "created_at": "2018-09-08T16:09:00",
                    "id": 562,
                    "image_url": "https://www.ccn.com/wp-content/uploads/2016/02/Running-climb.jpg",
                    "section": {
                        "id": 18,
                        "name": "CRYPTO CURRENCY",
                        "uid": "jZch2XuMaGDqhJ3A2hfSzc"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 40,
                        "section": "CRYPTO CURRENCY",
                        "title": "crypto coins news",
                        "uid": "Co2k5m2TYCQunck2po2MXN",
                        "url": "https://newsapi.org/v2/top-headlines?sources=crypto-coins-news&apiKey={api_key}"
                    },
                    "title": "Dash Breaks Bear Paralysis as Strong Fundamentals Add 25% Gains",
                    "uid": "seUCogP94P7Z9DVj8tfWC2",
                    "url": "https://www.ccn.com/dash-breaks-bear-paralysis-as-strong-fundamentals-add-25-gains/",
                    "video_url": None
                },
                {
                    "body": "Sometime late last week, news began circulating around social media that Nigeria has moved from 125th to 45th place up the Ease of Doing Business Index.\nThe claim was cumulatively reposted and shared thousands of times on Facebook.\nA sample collection of Facebook users that posted, reposted and shared the fake news.\nSome users wondered why this \u201cgood news\u201d was not trending at the time.\n\u201cIt is not trending,\u201d a user echoes a general sentiment.\nWhy is this a big deal? \u00a0\nThe Ease of Doing Business Index is an academic research by the World Bank that shows the degree of ease or difficulty of starting and running businesses in 190 countries across the world.\nIt\u2019s official description reads;\nThe Doing Business project provides objective measures of business regulations and their enforcement across 190 economies and selected cities at the subnational and regional level.\nIn Nigeria, setting up and running a business can be a near herculean task.\nSo Nigeria moving 80 places up this list deserves serious accolades and worthy mentions. But is the move even true?\nFacts\nAccording to the \u201cDoing Business 2018\u201d report, Nigeria is still 145th out of 190 counties.\nThe only recorded move was a leap in 2017, 24 places up from 169th to its current 145th place.\nAccording to the World Bank, the move also placed Nigeria among top 10 improved countries worldwide.\nThere has not been any upward move on the index this year. And no revisions can be made to the list until the last quarter of the year.\nWhere then is this story from? \u00a0\nThe origin of this fake news is unclear.\nIn August, the Special Assistant to the President on Industry, Trade and Investment, Dr. \u00a0Jumoke Oduwole reportedly made a statement.\nShe said Nigeria will move 45 places up the index in 2 years to reach 100th place by 2020.\nDr. Jumoke who also doubles as secretary of the Presidential Enabling Business Environment Council (PEBEC) said this at a meeting of the council.\nAt first glance, it is unclear and confusing on what grounds she is making such a bold prediction.\nBut as the council was inaugurated to make doing business easier in Nigeria, her statement can be seen as a declaration of purpose.\nAlso considering Nigeria\u2019s 24 place jump on the index from 2017 to 2018, the projected 45-place move does not seem so far-fetched.\nConclusion\nNigeria did not move up the Ease of Doing Business Index 2018.\nThe fake news spreading in this regard may be a misconstruing \u2014 deliberate or not \u2014 of the statement by the PEBEC secretary. Or plain mischievous attempt by undesirable elements to misinform.\n",
                    "created_at": "2018-09-04T14:00:11",
                    "id": 572,
                    "image_url": "https://techpoint.africa/wp-content/uploads/2016/01/nigerian-market.jpg",
                    "section": {
                        "id": 19,
                        "name": "NIGERIAN TECHNOLOGY",
                        "uid": "xSAhwnGneXm5d3zqBhgquH"
                    },
                    "source": {
                        "category": "WORDPRESS",
                        "id": 3,
                        "section": "NIGERIAN TECHNOLOGY",
                        "title": "techpoint",
                        "uid": "sG2DPo498uKMNzNbzttK2e",
                        "url": "https://techpoint.africa/wp-json/wp/v2/posts?per_page={per_page}"
                    },
                    "title": "Fact Check: Nigeria did not move up the Ease of Doing Business Index",
                    "uid": "deWE53LM3vYArU3P6eZDnL",
                    "url": "https://techpoint.africa/2018/09/04/nigeria-ease-of-doing-business-index/",
                    "video_url": ""
                },
                {
                    "body": "It is still unknown what causes multiple sclerosis, but new research identifies another potential culprit in the immune system: memory B cells.",
                    "created_at": "2018-09-08T00:00:00",
                    "id": 1504,
                    "image_url": "https://cdn1.medicalnewstoday.com/content/images/hero/323/323006/323006_1100.jpg",
                    "section": {
                        "id": 20,
                        "name": "MEDICAL",
                        "uid": "8SeGb73QJyU7yTkTTEUNGJ"
                    },
                    "source": {
                        "category": "NEWS_API",
                        "id": 42,
                        "section": "MEDICAL",
                        "title": "medical news today",
                        "uid": "wouxEuVYEgyDpUvnKuJZ5S",
                        "url": "https://newsapi.org/v2/top-headlines?sources=medical-news-today&apiKey={api_key}"
                    },
                    "title": "New multiple sclerosis culprit identified",
                    "uid": "HqGwmYbYaQCGnLnSeBXfUA",
                    "url": "http://www.medicalnewstoday.com/articles/323006.php",
                    "video_url": None
                }
            ]
        }

        )


@articles_namespace.route('/<article_uid>')
class Article(Resource):
    @api.doc(
        parser=get_article_args_parser
    )
    @api.response(200, 'OK')
    @api.response(404, 'Article not found')
    def get(self, article_uid):
        """Fetch an article"""
        return api_success_response({
            "body": "\"We do want to see Papadopoulos. We also want to see Michael Cohen, who has indicated he would come back without any immunity and testify\"",
            "created_at": "2018-09-08T20:24:21",
            "id": 1269,
            "image_url": "https://cbsnews1.cbsistatic.com/hub/i/r/2018/09/09/2104203c-ceff-4b64-9ca8-b9abbbfd32ec/thumbnail/1200x630/97bd401817dfdfbccfd5646864eeac66/screen-shot-2018-09-09-at-10-22-43-am.png",
            "section": {
                "id": 11,
                "name": "FINANCE",
                "uid": "fxkcwwZUrcAxW983cE4ii6"
            },
            "source": {
                "category": "NEWS_API",
                "id": 23,
                "section": "FINANCE",
                "title": "cbs news",
                "uid": "j9srCFR8bt4CAWAXyMLFFG",
                "url": "https://newsapi.org/v2/top-headlines?sources=cbs-news&apiKey={api_key}"
            },
            "title": "Mark Warner on \"Face the Nation\": Senate Intel likely won't release Russia report before midterms",
            "uid": "q5N9kkyiTaMfSHzg2PTWUH",
            "url": "https://www.cbsnews.com/news/mark-warner-face-the-nation-intelligence-committee-likely-wont-release-russia-report-before-midterms/"
        }
        )


@event_namespace.route('')
class Events(Resource):
    @api.doc(
        parser=get_events_args_parser
    )
    @api.response(200, 'OK')
    def get(self):
        """Retrieve upcoming events in a defined location"""
        return api_success_response(
            {
                'name': self.name,
                'description': self.description,
                'date': self.date.isoformat()
            }
        )


@readers_namespace.route('')
class AllReaders(Resource):
    @api.doc(
        parser=create_reader_args_parser
    )
    @api.response(201, 'Created')
    @api.response(400, 'Bad request')
    def post(self):
        """Create a reader"""
        request_data = current_request_data()

        # try:
        #     controllers.Readers.validate_reader_creation_fields(**request_data)
        # except errors.BadRequest as e:
        #     return api_failure_response(error_msg=e.message, code=403)

        return api_success_response(response_data={
            'first_name': 'Jason',
            'last_name': 'Ajanlekoko',
            'email': 'mail@example.com',
            'phone': '08107684553',
            'disruptiv_platform': {
                'id': 1,
                'name': 'USSD'
            },
            'uid': 'Xc1920bpvdpuerdfA'
        }, code=201)


@readers_namespace.route('/<reader_uid>')
class Reader(Resource):
    @api.doc(
        parser=create_reader_args_parser
    )
    @api.response(200, 'Ok')
    @api.response(404, 'Reader not found')
    def get(self, reader_uid):
        """Retrieve a reader"""
        request_data = current_request_data()

        # try:
        #     controllers.Readers.validate_reader_creation_fields(**request_data)
        # except errors.BadRequest as e:
        #     return api_failure_response(error_msg=e.message, code=403)

        return api_success_response(
            response_data={
                'first_name': 'Jason',
                'last_name': 'Ajanlekoko',
                'email': 'mail@example.com',
                'phone': '08107684553',
                'disruptiv_platform': {
                    'id': 1,
                    'name': 'USSD'
                },
                'uid': 'Xc1920bpvdpuerdfA'
            },
            code=200
        )

    @api.doc(
        parser=create_reader_args_parser
    )
    @api.response(200, 'Ok')
    @api.response(404, 'Reader not found')
    def patch(self, reader_uid):
        """Edit a reader"""
        request_data = current_request_data()

        # try:
        #     controllers.Readers.validate_reader_creation_fields(**request_data)
        # except errors.BadRequest as e:
        #     return api_failure_response(error_msg=e.message, code=403)

        return api_success_response(
            response_data={
                'first_name': 'Jason',
                'last_name': 'Ajanlekoko',
                'email': 'mail@example.com',
                'phone': '08107684553',
                'disruptiv_platform': {
                    'id': 1,
                    'name': 'USSD'
                },
                'uid': 'Xc1920bpvdpuerdfA'
            }, code=200
        )

    @api.doc(
        parser=create_reader_args_parser
    )
    @api.response(204, 'Deleted')
    @api.response(404, 'Reader not found')
    def delete(self, reader_uid):
        """Delete a reader"""
        request_data = current_request_data()

        # try:
        #     controllers.Readers.validate_reader_creation_fields(**request_data)
        # except errors.BadRequest as e:
        #     return api_failure_response(error_msg=e.message, code=403)

        return api_success_response(response_data={
            'first_name': 'Jason',
            'last_name': 'Ajanlekoko',
            'email': 'mail@example.com',
            'phone': '08107684553',
            'disruptiv_platform': {
                'id': 1,
                'name': 'USSD'
            },
            'uid': 'Xc1920bpvdpuerdfA'
        }, code=204)


@sections_namespace.route('')
class AllSections(Resource):
    @api.doc(
        parser=get_sections_args_parser
    )
    @api.response(200, 'OK')
    def get(self):
        """Retrieve all sections."""
        sections = models.Section.prepare_query_for_active().all()

        return api_success_response([section.as_json() for section in sections])


@sections_namespace.route('/<section_uid>/headlines')
class Section(Resource):
    @api.doc(
        parser=get_section_args_parser
    )
    @api.response(200, 'OK')
    @api.response(404, 'Section not found')
    def get(self, section_uid):
        """Retrieve the latest headlines under a section."""
        request_args = current_request_args()
        page = int(request_args.get('page') or 1)
        per_page = int(request_args.get('per_page') or 10)

        section = models.Section.get(uid=section_uid)
        if section is None:
            return api_failure_response(
                code=404, error_msg='Section not found!')

        section_articles = models.Article.prepare_query_for_active(
            section_id=section.id).all()

        return api_success_response(
            [article.as_json() for article in section_articles[:per_page]])


@sources_namespace.route('')
class AllSources(Resource):
    @api.doc(
        parser=get_sources_args_parser
    )
    @api.response(200, 'OK')
    def get(self):
        """Retrieve a list of all news sources."""
        sources = models.NewsSource.prepare_query_for_active().all()

        return api_success_response([source.as_json() for source in sources])


@sources_namespace.route('/<source_uid>/headlines')
class Source(Resource):
    @api.doc(
        parser=get_source_args_parser
    )
    @api.response(200, 'OK')
    @api.response(404, 'News source not found')
    def get(self, source_uid):
        """Retrieve the latest headlines from a news source."""
        request_args = current_request_args()
        page = int(request_args.get('page') or 1)
        per_page = int(request_args.get('per_page') or 10)

        source = models.NewsSource.get(uid=source_uid)
        if source is None:
            return api_failure_response(
                code=404, error_msg='Source not found!')

        source_articles = models.Article.prepare_query_for_active(
            news_source_id=source.id).all()

        return api_success_response(
            [article.as_json() for article in source_articles[:per_page]])


@subscriptions_namespace.route('')
class AllSubscriptions(Resource):
    @api.doc(
        parser=get_subscriptions_args_parser
    )
    @api.response(200, 'OK')
    @api.response(404, 'Reader not found')
    def get(self, reader_uid):
        """Get a list of a reader's subscriptions"""
        request_data = current_request_data()

        section_uid = request_data.get('section_uid')
        news_source_uid = request_data.get('news_source_uid')

        section = models.Section.get(uid=section_uid)
        source = models.NewsSource.get(uid=news_source_uid)
        if bool(source) == bool(section) == False:
            return api_failure_response(
                code=404, error_msg='Send a valid `section_uid` or '
                                    '`news_source_uid`')

        return api_success_response(
            response_data=[
                {
                    "news_source": None,
                    "reader": "SJmwVrTzaEie77NAJnYMFb",
                    "section": "2LCsZc4qtfmRH5NWnZdW75"
                },
                {
                    "news_source": "fmRH5NWnZdW752LCsZc4qt",
                    "reader": "SJmwVrTzaEie77NAJnYMFb",
                    "section": None
                }
            ]
        )

    @api.doc(
        parser=create_subscriber_args_parser
    )
    @api.response(201, 'Created')
    @api.response(400, 'Bad request')
    @api.response(404, 'Reader not found')
    def post(self, reader_uid):
        """Subscribe a reader to a section or a news source."""
        request_data = current_request_data()

        section_uid = request_data.get('section_uid')
        news_source_uid = request_data.get('news_source_uid')

        section = models.Section.get(uid=section_uid)
        source = models.NewsSource.get(uid=news_source_uid)
        if bool(source) == bool(section) == False:
            return api_failure_response(
                code=404, error_msg='Send a valid `section_uid` or '
                                    '`news_source_uid`')

        return api_success_response(
            response_data={
                "news_source": None,
                "reader": "SJmwVrTzaEie77NAJnYMFb",
                "section": "2LCsZc4qtfmRH5NWnZdW75"
            },
            code=201
        )


@subscriptions_namespace.route('/<subscription_uid>')
class Subscriptions(Resource):
    @api.doc(
        parser=get_subscriptions_args_parser
    )
    @api.response(200, 'OK')
    @api.response(404, 'Reader not found')
    def get(self, reader_uid):
        """Get the details of a reader's subscription"""
        request_data = current_request_data()

        section_uid = request_data.get('section_uid')
        news_source_uid = request_data.get('news_source_uid')

        section = models.Section.get(uid=section_uid)
        source = models.NewsSource.get(uid=news_source_uid)
        if bool(source) == bool(section) == False:
            return api_failure_response(
                code=404, error_msg='Send a valid `section_uid` or '
                                    '`news_source_uid`')

        return api_success_response(
            response_data=[
                {
                    "news_source": None,
                    "reader": "SJmwVrTzaEie77NAJnYMFb",
                    "section": "2LCsZc4qtfmRH5NWnZdW75"
                },
                {
                    "news_source": "fmRH5NWnZdW752LCsZc4qt",
                    "reader": "SJmwVrTzaEie77NAJnYMFb",
                    "section": None
                }
            ]
        )

    @api.response(204, 'Deleted')
    @api.response(404, 'Reader not found')
    def delete(self, reader_uid, subscription_uid):
        """Unsubscribe a reader to a section or a news source."""
        request_data = current_request_data()

        section_uid = request_data.get('section_uid')
        news_source_uid = request_data.get('news_source_uid')

        section = models.Section.get(uid=section_uid)
        source = models.NewsSource.get(uid=news_source_uid)
        if bool(source) == bool(section) == False:
            return api_failure_response(
                code=404, error_msg='Send a valid `section_uid` or '
                                    '`news_source_uid`')

        return api_success_response(
            response_data={
                "news_source": None,
                "reader": "SJmwVrTzaEie77NAJnYMFb",
                "section": "2LCsZc4qtfmRH5NWnZdW75"
            },
            code=204
        )
