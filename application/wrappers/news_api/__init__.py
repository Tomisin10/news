from dateutil import parser as date_parser
import requests


API_KEY = '183cef0a6d09498e8aa485f0ef130c09'


FINANCE_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=business-insider&apiKey=%s' %
    API_KEY,
)
ENTERTAINMENT_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=mashable&apiKey=%s' % API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=entertainment-weekly'
    '&apiKey=%s' % API_KEY,
)
WORLD_NEWS_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=%s' % API_KEY,
)
SPORTS_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=bbc-sport&apiKey=%s' %
    API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=four-four-two&apiKey=%s'
    % API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=bleacher-report&apiKey=%s'
    % API_KEY
)
TECHNOLOGY_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=hacker-news&apiKey=%s' % API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=engadget&apiKey=%s' % API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=recode&apiKey=%s' % API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=crypto-coins-news&apiKey=%s' %
    API_KEY,
    'https://newsapi.org/v2/top-headlines?sources=wired&apiKey=%s' % API_KEY,
)
GAMING_LINK = (
    'https://newsapi.org/v2/top-headlines?sources=ign&apiKey=%s' % API_KEY,)


def _get_datetime_from_timestamp(timestamp):
    return date_parser.parse(timestamp)


def save_post(json, news_source_id, section_id):
    from app import application
    from application.articles.helpers import create_new_article

    title = json['title']
    body = json['description']
    link = json['url']
    image_url = json['urlToImage']
    published_at = json['publishedAt']

    created_at = _get_datetime_from_timestamp(published_at)

    # with application.app_context():
    create_new_article(
        title, body, link, news_source_id, section_id, image_url=image_url,
        created_at=created_at
    )


def fetch_from_url(url, news_source_id, section_id):
    url = url.format(api_key=API_KEY)

    response = requests.get(url).json()

    try:
        return response['articles']
    except:
        print(response)

    for article in response['articles']:
        save_post(article,news_source_id, section_id)
