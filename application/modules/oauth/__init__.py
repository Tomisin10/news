import json
import logging
from urllib import parse as urlparse

from flask import Blueprint
from flask import make_response, redirect, request
from flask.views import MethodView
import requests

from application.wrappers.libs import oauth2 as oauth
from application.core.constants import (FACEBOOK_APP_ID, FACEBOOK_APP_SECRET)
from application.core.models.helpers import (
    orm_get_social_media_account_by_username)
from application.wrappers import save_social_media_account
from application.wrappers import facebook, twitter, instagram


oauth_blueprint = Blueprint('oauth', __name__, url_prefix='/oauth')


class FacebookPage(MethodView):
    def get(self, action):
        if action == 'login':
            code_callback_url = '{}oauth/facebook/callback'.format(request.host_url)

            facebook_oauth_url = (
                "https://www.facebook.com/v3.0/dialog/oauth?client_id={}&"
                "redirect_uri={}&state={}".format(
                FACEBOOK_APP_ID, code_callback_url, 'callback')
            )

            # graph = facebook.GraphAPI(
            #     access_token=facebook.get_auth_token(), version="3.0")
            #
            # perms = ["manage_pages", "publish_pages"]
            # fb_login_url = graph.get_auth_url(
            #     FACEBOOK_APP_ID, callback_url, perms)
            #
            # print(fb_login_url)

            return redirect(facebook_oauth_url)

        elif action == 'callback':
            code = request.args.get('code')

            if code:
                access_token_callback_url = '{}oauth/facebook/callback'.format(
                    request.host_url
                )

                access_token_url = (
                    "https://graph.facebook.com/v3.0/oauth/access_token?"
                    "client_id={}&redirect_uri={}&client_secret={}&"
                    "code={}".format(
                        FACEBOOK_APP_ID, access_token_callback_url,
                        FACEBOOK_APP_SECRET, code
                    )
                )

                return redirect(access_token_url)

            else:
                access_token = request.args.get('token')

                save_social_media_account(
                    app_name='Facebook', username=None, meta=None,
                    oauth_token=access_token, oauth_token_secret=None,
                    client_token=FACEBOOK_APP_ID,
                    client_token_secret=FACEBOOK_APP_SECRET)

                graph = facebook.GraphAPI(access_token)

                return 'All done'


class InstagramPage(MethodView):

    def get(self, action):
        from application.wrappers.instagram import (
            CLIENT_ID, DEFAULT_CLIENT_SECRET)

        SERVERS_ADDRESS = request.host_url

        redirect_uri = (
                '%sexternal-apps/instagram/callback' % (
            SERVERS_ADDRESS)
        )

        request_code_url = (
            'https://api.instagram.com/oauth/authorize/?'
            'client_id={client_id}&redirect_uri={redirect_uri}&'
            'response_type=code&scope={scope}'.format(
                client_id=CLIENT_ID,
                redirect_uri=redirect_uri,
                scope='public_content'
            )
        )

        if action == 'login':
            trails_user_uid = request.args.get('trails_user_uid')
            referrer = request.referrer

            response = make_response(redirect(request_code_url))

            response.set_cookie('trails_user_uid', trails_user_uid)
            response.set_cookie(
                'ext_app_login_referrer',
                referrer or SERVERS_ADDRESS)

            return response

        elif action == 'callback':
            ext_app_login_referrer = request.cookies.get(
                'ext_app_login_referrer')
            code = request.args.get('code')

            request_token_url = 'https://api.instagram.com/oauth/access_token'

            token_response = json.loads(
                requests.post(
                    request_token_url,
                    data={
                        'client_id': CLIENT_ID,
                        'client_secret': DEFAULT_CLIENT_SECRET,
                        'grant_type': 'authorization_code',
                        'code': code,
                        'redirect_uri': '%soauth/instagram/callback' % (
                            SERVERS_ADDRESS)
                    }
                ).content.decode()
            )

            oauth_token = token_response['access_token']

            instagram_client = instagram.create_client(
                access_token={
                    'oauth_token': oauth_token
                }
            )

            social_media_account = instagram_client.user()

            social_media_username = social_media_account.username

            social_media_orm = orm_get_social_media_account_by_username(
                'Instagram', social_media_username)

            if not social_media_orm:
                save_social_media_account(
                    app_name='instagram',
                    oauth_token=oauth_token,
                    username=social_media_username,
                    meta=social_media_account
                )

            else:
                social_media_orm.update(oauth_token=oauth_token)

            return redirect(ext_app_login_referrer)


class TwitterPage(MethodView):
    def get(self, action='a'):
        from application.wrappers.twitter import (
            CONSUMER_KEY, CONSUMER_SECRET)

        SERVERS_ADDRESS = request.host_url

        if action == 'login':
            # trails_user_uid = request.args.get('trails_user_uid')
            referrer = request.referrer

            request_token_url = 'https://api.twitter.com/oauth/request_token'
            authorize_url = 'https://api.twitter.com/oauth/authorize'

            consumer = oauth.Consumer(CONSUMER_KEY, CONSUMER_SECRET)
            client = oauth.Client(consumer)
            resp, content = client.request(request_token_url, "GET")
            request_token = dict(urlparse.parse_qsl(content))

            oauth_token = request_token[b'oauth_token'].decode()

            response = make_response(redirect(
                "%s?oauth_token=%s" % (
                    authorize_url, oauth_token)))

            # response.set_cookie('trails_user_uid', trails_user_uid)
            response.set_cookie('twitter_request_token',
                                str(request_token))
            response.set_cookie('ext_app_login_referrer', referrer or
                                SERVERS_ADDRESS)

            return response

        elif action == 'callback':
            access_token_url = 'https://api.twitter.com/oauth/access_token'

            oauth_verifier = request.args.get('oauth_verifier')

            ext_app_login_referrer = request.cookies.get(
                'ext_app_login_referrer')

            # trails_user_uid = request.cookies.get('trails_user_uid')
            request_token = eval(request.cookies.get(
                'twitter_request_token'))

            token = oauth.Token(
                request_token[b'oauth_token'].decode(),
                request_token[b'oauth_token_secret'].decode()
            )
            token.set_verifier(oauth_verifier)

            consumer = oauth.Consumer(CONSUMER_KEY, CONSUMER_SECRET)

            client = oauth.Client(consumer, token)

            resp, content = client.request(access_token_url, "POST")
            access_token = dict(urlparse.parse_qsl(content))

            print('ACCESS TOKEN IS: ', access_token)

            token = access_token[b'oauth_token'].decode()
            secret = access_token[b'oauth_token_secret'].decode()

            twitter_client = twitter.create_client(
                username=None,
                access_token={'oauth_token': token,
                              'oauth_token_secret': secret})

            social_media_account = twitter_client.statuses.user_timeline()[
                0]['user']

            social_media_username = social_media_account['screen_name']

            social_media_orm = orm_get_social_media_account_by_username(
                'Twitter', social_media_username)

            if not social_media_orm:
                save_social_media_account(
                    app_name='Twitter',
                    oauth_token=token,
                    oauth_token_secret=secret,
                    username=social_media_username,
                    meta=str(social_media_account),
                    client_token=twitter.CONSUMER_KEY,
                    client_token_secret=twitter.CONSUMER_SECRET
                )

            else:
                social_media_orm.update(oauth_token=token,
                                        oauth_token_secret=secret)

            return 'Done!'
            # return redirect(ext_app_login_referrer)


class MediumPage(MethodView):
    def get(self, action):
        from application.wrappers.medium import create_client

        SERVERS_ADDRESS = request.host_url

        client = create_client(default=True)

        if action == 'login':
            trails_user_uid = request.args.get('trails_user_uid')
            referrer = request.referrer

            auth_url = client.get_authorization_url(
                "secretstate",
                "%soauth/medium/callback" % SERVERS_ADDRESS,
                ["basicProfile", "uploadImage"]
            )

            logging.info(
                'callback_url: %soauth/medium/callback' %
                SERVERS_ADDRESS)
            logging.info('auth_url: %s' % auth_url)

            # request_token_url = 'https://api.twitter.com/oauth/request_token'
            # authorize_url = 'https://api.twitter.com/oauth/authorize'
            #
            # consumer = oauth.Consumer(CONSUMER_KEY, CONSUMER_SECRET)
            # client = oauth.Client(consumer)
            # resp, content = client.request(request_token_url, "GET")
            # request_token = dict(urlparse.parse_qsl(content))
            #
            response = make_response(redirect(auth_url))
            response.set_cookie('trails_user_uid', trails_user_uid)
            # self.response.set_cookie('twitter_request_token',
            #                          str(request_token))
            response.set_cookie('ext_app_login_referrer',
                                referrer or SERVERS_ADDRESS)

            return response

        elif action == 'callback':
            logging.info(request.body)

            auth = client.exchange_authorization_code(
                "YOUR_AUTHORIZATION_CODE",
                "http://%soauth/medium/callback" % SERVERS_ADDRESS)

            client.access_token = auth["access_token"]

            access_token_url = 'https://api.twitter.com/oauth/access_token'

            oauth_verifier = request.args.get('oauth_verifier')

            ext_app_login_referrer = request.cookies.get(
                'ext_app_login_referrer')
            trails_user_uid = request.cookies.get('trails_user_uid')
            request_token = eval(request.cookies.get(
                'twitter_request_token'))

            token = oauth.Token(request_token['oauth_token'],
                                request_token['oauth_token_secret'])
            token.set_verifier(oauth_verifier)

            consumer = oauth.Consumer(self.CONSUMER_KEY, self.CONSUMER_SECRET)

            client = oauth.Client(consumer, token)

            resp, content = client.request(access_token_url, "POST")
            access_token = dict(urlparse.parse_qsl(content))

            token = access_token['oauth_token']
            secret = access_token['oauth_token_secret']

            twitter_client = twitter.create_client(
                username=None,
                access_token={
                    'oauth_token': token,
                    'oauth_token_secret': secret
                }
            )

            social_media_account = twitter_client.statuses.user_timeline()[0]['user']

            social_media_username = social_media_account['screen_name']

            social_media_orm = orm_get_social_media_account_by_username(
                'Medium', social_media_username)

            if not social_media_orm:
                save_social_media_account(
                    app_name='medium',
                    oauth_token=token,
                    oauth_token_secret=secret,
                    username=social_media_username,
                    meta=social_media_account
                )

            else:
                social_media_orm.update(oauth_token=token,
                                        oauth_token_secret=secret)

            # if account is being created with ext app login

            return redirect(ext_app_login_referrer)


class LinkedInPage(MethodView):
    def get(self, action):
        from application.wrappers.libs.linkedin import linkedin
        from application.wrappers.linkedin import (
            create_client, CONSUMER_SECRET, CONSUMER_KEY)

        SERVERS_ADDRESS = request.host_url
        return_url = '%sexternal-apps/linked-in/callback' % request.host_url

        trails_user_uid = request.args.get('trails_user_uid')

        if action == 'login':
            referrer = request.referrer

            authentication = linkedin.LinkedInAuthentication(
                CONSUMER_KEY, CONSUMER_SECRET,
                return_url,
                linkedin.PERMISSIONS.enums.values()
            )

            response = make_response(redirect(authentication.authorization_url))
            response.set_cookie('trails_user_uid', trails_user_uid)
            response.set_cookie(
                'ext_app_login_referrer',
                referrer or SERVERS_ADDRESS)

            return response

        elif action == 'callback':
            ext_app_login_referrer = request.cookies.get(
                'ext_app_login_referrer')
            return_url = '%sexternal-apps/linked-in/callback' % request.host_url

            authentication = linkedin.LinkedInAuthentication(
                CONSUMER_KEY, CONSUMER_SECRET,
                return_url,
                linkedin.PERMISSIONS.enums.values()
            )

            authentication.authorization_code = request.args.get('code')
            token = authentication.get_access_token().access_token

            linkedin_client = create_client(
                access_token={
                    'oauth_token': token
                }
            )

            social_media_account = linkedin_client.get_profile(
                selectors=[
                    'id', 'first-name', 'last-name', 'location',
                    'distance', 'num-connections', 'skills', 'educations'
                ]
            )

            social_media_username = social_media_account['id']

            social_media_orm = orm_get_social_media_account_by_username(
                social_media_username)

            if not social_media_orm:
                save_social_media_account(
                    app_name='linked-in',
                    oauth_token=token,
                    username=social_media_username,
                    meta=social_media_account
                )

            else:
                'update existing records'

            # if account is being created with ext app login
            return redirect(ext_app_login_referrer)


class LandingPage(MethodView):
    def get(self):
        return 'Hello'


mappings = [
    ('/landing', LandingPage, 'landing'),
    ('/facebook/<action>', FacebookPage, 'facebook'),
    ('/instagram/<action>', InstagramPage, 'instagram'),
    ('/linked-in/<action>', LinkedInPage, 'linked-in'),
    ('/medium/<action>', MediumPage, 'medium'),
    ('/twitter/<action>', TwitterPage, 'twitter'),
]

for url in mappings:
    path, view, name = url

    oauth_blueprint.add_url_rule(path, view_func=view.as_view(name))
