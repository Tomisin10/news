#!/usr/bin/env python

import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from sqlalchemy.exc import IntegrityError as SQLAlchemyIntegrityError
from pymysql.err import IntegrityError as PyMySQLIntegrityError

from application.core.constants import *
from application.core.models import *
from application.wrappers import news_api, rss, wordpress
from app_setup import application, db


manager = Manager(application)

if os.environ['RUNNING_MODE'] == 'production':
    migrate = Migrate(application, db, directory='remote_migrations')
else:
    migrate = Migrate(application, db, directory='local_migrations')

manager.add_command('db', MigrateCommand)


@manager.command
def pump_status_table():
    status_modes_and_ids = (
        'Active', 'Pending', 'Disabled', 'Failed', 'Deleted'
    )

    for status in status_modes_and_ids:
        if Status.get(name=status) is None:
            status_orm = Status(name=status)
            status_orm.save()

    # change the last one, which is 'Deleted'
    try:
        status_orm.update(id=99)
    except:
        pass


@manager.command
def pump_sections_table():
    for section in ['WORLD', 'ENGLISH PREMIERSHIP', 'FOOTBALL', 'SPORTS',
            'AFRICAN', 'GAMING', 'GOSSIPS', 'NIGERIAN', 'NIGERIAN FASHION',
            'NIGERIAN ENTERTAINMENT', 'FASHION', 'FINANCE', 'NATURE & SCIENCE',
            'ENTERTAINMENT', 'TECHNOLOGY', 'LIFESTYLE', 'COMPILATIONS',
            'NIGERIAN LIFESTYLE', 'CRYPTO CURRENCY', 'FINANCE',
            'NIGERIAN TECHNOLOGY', 'MEDICAL']:

        if Section.get(name=section) is None:
            Section(name=section).save()


@manager.command
def pump_news_sources_table_():
    var_category = {
        news_api: 'NEWS_API',
        rss: 'RSS',
        wordpress: 'WORDPRESS'
    }

    for module in [news_api, rss, wordpress]:
        category = var_category[module]

        for var in dir(module):

            if var.endswith('_LINK'):
                section = var.split('_LINK')[0]
                section_name = section.replace('_', ' ')

                section_id = Section.get(name=section_name).id

                urls = getattr(module, var)

                for url in urls:
                    if NewsSource.get(url=url) is None:
                        NewsSource(
                            url=url, category=category, section_id=section_id
                        ).save()


@manager.command
def pump_news_sources_table():
    sections = dict(
        [(section.name, section.id) for section in Section.prepare_query_for_active()]
    )

    news_sources = [
        ('WORDPRESS',
         'https://www.techcrunch.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['TECHNOLOGY']),
        ('WORDPRESS',
         'https://techpoint.africa/wp-json/wp/v2/posts?per_page={per_page}',
         sections['TECHNOLOGY']),
        ('WORDPRESS',
         'https://techpoint.africa/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN TECHNOLOGY']),
        ('WORDPRESS',
         'http://www.tooxclusive.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN ENTERTAINMENT']),
        ('WORDPRESS',
         'http://www.notjustok.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN ENTERTAINMENT']),
        ('WORDPRESS',
         'https://www.bellanaija.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN LIFESTYLE']),
        ('WORDPRESS',
         'https://www.zikoko.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN LIFESTYLE']),
        ('WORDPRESS',
         'http://www.reachnaija.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN']),
        ('WORDPRESS',
         'http://stylepantry.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN FASHION']),
        ('WORDPRESS',
         'http://infashionitrust.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['NIGERIAN FASHION']),
        ('WORDPRESS',
         'http://www.thefashionspot.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['FASHION']),
        ('WORDPRESS',
         'http://freddieharrel.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['FASHION']),
        ('WORDPRESS',
         'https://www.bellanaija.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['GOSSIPS']),
        ('WORDPRESS',
         'https://www.reachnaija.com/wp-json/wp/v2/posts?per_page={per_page}',
         sections['GOSSIPS']),

        ('RSS',
         'http://saharareporters.com/feeds/latest/feed',
         sections['NIGERIAN']),
        ('RSS',
         'http://syndication.eonline.com/syndication/feeds/rssfeeds/style.xml',
         sections['LIFESTYLE']),
        ('RSS',
         'http://syndication.eonline.com/syndication/feeds/rssfeeds/topstories.xml',
         sections['ENTERTAINMENT']),
        ('RSS',
         'http://feeds.sport24.co.za/articles/Sport/Soccer/EnglishPremiership/rss',
         sections['ENGLISH PREMIERSHIP']),
        ('RSS',
         'http://feeds.sport24.co.za/articles/Sport/Featured/TopStories/rss',
         sections['SPORTS']),
        ('RSS',
         'https://www.naija.ng/rss/all.rss',
         sections['NIGERIAN']),
        ('RSS',
         'http://rss.cnn.com/rss/edition_africa.rss',
         sections['AFRICAN']),

        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=business-insider'
         '&apiKey={api_key}',
         sections['FINANCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=the-economist'
         '&apiKey={api_key}',
         sections['FINANCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=fortune'
         '&apiKey={api_key}',
         sections['FINANCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=cbs-news'
         '&apiKey={api_key}',
         sections['FINANCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=national-geographic'
         '&apiKey={api_key}',
         sections['NATURE & SCIENCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=next-big-future'
         '&apiKey={api_key}',
         sections['NATURE & SCIENCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=new-scientist'
         '&apiKey={api_key}',
         sections['NATURE & SCIENCE']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=mashable&apiKey={'
         'api_key}',
         sections['COMPILATIONS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=buzzfeed&apiKey={'
         'api_key}',
         sections['COMPILATIONS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=daily-mail&'
         'apiKey={api_key}',
         sections['COMPILATIONS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=entertainment-weekly'
         '&apiKey={api_key}',
         sections['ENTERTAINMENT']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=mtv-news'
         '&apiKey={api_key}',
         sections['ENTERTAINMENT']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=bbc-news'
         '&apiKey={api_key}',
         sections['WORLD']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=bbc-sport'
         '&apiKey={api_key}',
         sections['SPORTS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=four-four-two'
         '&apiKey={api_key}',
         sections['FOOTBALL']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=bleacher-report'
         '&apiKey={api_key}',
         sections['SPORTS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=hacker-news'
         '&apiKey={api_key}',
         sections['TECHNOLOGY']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=engadget'
         '&apiKey={api_key}',
         sections['TECHNOLOGY']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=recode'
         '&apiKey={api_key}',
         sections['TECHNOLOGY']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=wired&apiKey={api_key}',
         sections['TECHNOLOGY']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=crypto-coins-news'
         '&apiKey={api_key}',
         sections['CRYPTO CURRENCY']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=associated-press'
         '&apiKey={api_key}',
         sections['COMPILATIONS']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=medical-news-today'
         '&apiKey={api_key}',
         sections['MEDICAL']),
        ('NEWS_API',
         'https://newsapi.org/v2/top-headlines?sources=ign'
         '&apiKey={api_key}',
         sections['GAMING'])
    ]

    for source in news_sources:
        try:
            NewsSource(
                category=source[0], url=source[1], section_id=source[2]
            ).save()
        except (SQLAlchemyIntegrityError, PyMySQLIntegrityError):
            pass


@manager.command
def pump_disruptiv_platforms_categories_table():
    for category in PLATFORM_CATEGORIES:
        if DisruptivPlatformCategory.get(name=category) is None:
            DisruptivPlatformCategory(name=category).save()


@manager.command
def pump_disruptiv_platforms_table():
    for platform in SUPPORTED_PLATFORMS:
        if DisruptivPlatform.get(name=platform) is None:
            DisruptivPlatform(
                name=platform, disruptiv_platform_category_id=1).save()


@manager.command
def show_all_articles():
    articles = Article.query.all()

    print([article.as_json() for article in articles])


@manager.command
def crawl_all_news_sites():
    from jobs import crawl_all_sites
    crawl_all_sites()


@manager.command
def crawl_all_event_sites():
    from jobs import crawl_events
    crawl_events()


@manager.command
def crawl_all_youtube_channels():
    from jobs import crawl_youtube_channels
    crawl_youtube_channels()


@manager.command
def pump_event_locations_table():
    for city, country in [
        ('Abuja', 'Nigeria'),
        ('Benin', 'Nigeria'),
        ('Calabar', 'Nigeria'),
        ('Enugu', 'Nigeria'),
        ('Lagos', 'Nigeria'),
        ('Port Harcourt', 'Nigeria')
    ]:
        EventLocation(city=city, country=country).save()


@manager.command
def pump_video_sources_table():
    for source in ['YouTube']:
        if VideoSource.get(name=source) is None:
            VideoSource(name=source).save()


@manager.command
def pump_youtube_channels_table():
    youtube_channels = {
        'POPULAR': ['UCL8Nxsa1LB9DrMTHtt3IKiw'],
        'AFRICAN': ['UC1_E8NeF5QHY2dtdLRBCCLA'],
        'ENTERTAINMENT': ['UCtg5C-d_3rPUgMaxr285mQQ',
                          'UCjDsbbzHgTrGc4Ff26TJtsA',
                          'UC-yXuc1__OzjwpsJPlxYUCQ'],
        'FINANCE': ['UCe-4xQosMQGkIA8mT4sR98Q'],
        'TECHNOLOGY': ['UCCjyq_K1Xwfg8Lndy7lKMpA'],
        'LIFESTYLE': [],
        'GAMING': ['UCKy1dAqELo0zrOtPkf0eTMw'],
        'SPORTS': ['UClhp9g6TPiqCTOlcw0ROfNg', 'UCW6-BQWFA70Dyyc7ZpZ9Xlg'],
        'ENGLISH PREMIERSHIP': ['UC2qSO3_lA502PUlZLNKFPcQ'],
        'NIGERIAN TECHNOLOGY': ['UC9T6ECutY4EPwJnGlbw4BzA'],
        'NATURE & SCIENCE': ['UCpVm7bg6pXKo1Pr6k5kxG9A'],
        'NIGERIAN': ['UCEXGDNclvmg6RW0vipJYsTQ'],
        'NIGERIAN ENTERTAINMENT': ['UCA2sngt9ZRxrqAHtanBEUnA'],
        'WORLD': ['UCupvZG-5ko_eiXAupbDfxWw']
    }

    video_source_youtube_orm = VideoSource.get(name='YouTube')

    for section, channels in youtube_channels.items():
        try:
            section_orm_id = Section.get(name=section).id
        except AttributeError:
            section_orm_id = None

        for channel_id in channels:
            if VideoChannel.get(channel_uid=channel_id) is None:
                VideoChannel(
                    channel_uid=channel_id,
                    section_id=section_orm_id,
                    video_source_id=video_source_youtube_orm.id
                ).save()



@manager.command
def show_all_sources():
    from jobs import push_latest_headlines

    push_latest_headlines()
    # NewsSource.get(id=9).update(
    #     url='https://www.reachnaija.com/wp-json/wp/v2/posts?per_page={per_page}'
    # )

    # for source in NewsSource.prepare_query_for_active().all():
    #     print(source.as_json())


@manager.command
def run_all_commands():
    print('status')
    pump_status_table()
    print('sections')
    pump_sections_table()
    print('news_sources')
    pump_news_sources_table()
    print('disruptiv_platforms_categories_table')
    pump_disruptiv_platforms_categories_table()
    print('disruptiv_platforms_table')
    pump_disruptiv_platforms_table()
    print('pump_video_sources_table')
    pump_video_sources_table()
    print('pump_youtube_channels_table')
    pump_youtube_channels_table()
    print('pump_event_locations_table')
    pump_event_locations_table()


if __name__ == "__main__":
    manager.run()
