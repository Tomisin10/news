import json
import requests


def initiate_mobile_payment(username, product_name, phone,
        currency_code, amount, meta, environment, provider_channel=None,
        api_key='30899f88b18489fc54262ce2d55ae9ce884e43582a3c3aaf4bca7dc169884'
                '5de'):

    headers = {'Accept': 'application/json',
               'Content-Type': 'application/json',
               'apikey': api_key}

    parameters = {
        'username': username,
        'productName': product_name,
        'phoneNumber': phone,
        'currencyCode': currency_code,
        'amount': amount,
        'metadata': meta
    }

    if provider_channel is not None:
        parameters['providerChannel'] = provider_channel

    payments_base_url = 'https://payments.africastalking.com'
    if environment == 'sandbox':
        payments_base_url = 'https://payments.sandbox.africastalking.com'

    payments_base_url += "/mobile/checkout/request"

    response = requests.post(payments_base_url, json=parameters,
                             headers=headers)

    print(response.content, response.status_code)

    return json.loads(response.content.decode())
