from flask.views import MethodView

from application.core.models import Author, NewsSource, Section
from application.core import errors
from application.core.utils.contexts import (
    current_request_args,
    current_request_data)
from application.core.utils.request_response_helpers import api_success_response
from application.articles.helpers import (
    get_existing_section_headlines, get_existing_news_source_headlines,
    get_existing_author_headlines,
    get_existing_tag_headlines)


class Headlines(MethodView):
    pass


class SectionHeadlines(Headlines):
    def get(self, section_uid):
        request_args = current_request_args()

        page = request_args.get('page', 1)
        per_page = request_args.get('per_page', 9)

        section_orm = Section.get(uid=section_uid)
        if section_orm is None:
            raise errors.ResourceNotFound

        headlines = get_existing_section_headlines(section_orm, page, per_page)

        return api_success_response(headlines)


class NewsSourceHeadlines(Headlines):
    def get(self, source_uid):
        request_args = current_request_args()

        page = request_args.get('page', 1)
        per_page = request_args.get('per_page', 9)

        source_orm = NewsSource.get(uid=source_uid)
        if source_orm is None:
            raise errors.ResourceNotFound

        headlines = get_existing_news_source_headlines(
            source_orm, page, per_page)

        return api_success_response(headlines)


class AuthorHeadlines(Headlines):
    def get(self, author_uid):
        request_args = current_request_args()

        page = request_args.get('page')
        per_page = request_args.get('per_page')

        author = Author.get(uid=author_uid)
        if author is None:
            raise errors.ResourceNotFound

        headlines = get_existing_author_headlines(author, page, per_page)

        return api_success_response(headlines)


class TagHeadlines(Headlines):
    def get(self, tag):
        headlines = get_existing_tag_headlines(tag)

        return api_success_response(headlines)
