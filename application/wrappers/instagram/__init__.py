import os
import time
import random
import urllib

import requests

from application.wrappers.libs.InstagramAPI import InstagramAPI


from application.core.constants import EXT_APP_POSTS_PER_USER_LIMIT
from application.wrappers import (
    get_auth_data, create_entry_from_parsed, save_entry_if_new)


DEFAULT_ACCOUNTS = [
    'nowthisnews', 'chrisburkard', 'wwmax', 'buzzfeed', 'natgeo', 'mtvnews',
    'thenextweb', 'tomisin10']

ADMIN_USERNAME = 'tomisin10'
DEFAULT_ACCESS_TOKEN = '4275228862.0acbcee.966b190850fb4a8b95033e72187dcf5b'
DEFAULT_CLIENT_SECRET = '5098c463f1064e0fa34d9f16018c6b0b'
CLIENT_ID = '0acbcee60d9543f9a7a29660ac444c35'

mock = {u'logging_page_id': u'profilePage_4275228862',
        u'show_suggested_profiles': False, u'graphql': {u'user': {
        u'external_url_linkshimmed': u'https://l.instagram.com/?u=http%3A%2F%2Fmarine-album-170216.appspot.com%2F&e=ATM9x145XIAe5c20k3UMl1mDOYJGZLSuH_zXQKGOJK9ZE4-aLA6o848YIIbvfem2X71OuLzW',
        u'has_requested_viewer': False, u'full_name': u'Tomisin',
        u'blocked_by_viewer': False, u'id': u'4275228862',
        u'biography': u"I hate making apps that don't make me lazier! On the cards you want to be lazier too, checkout #Trails. marine-album-170216.appspot.com",
        u'edge_follow': {u'count': 406}, u'followed_by_viewer': False,
        u'follows_viewer': False,
        u'edge_saved_media': {u'count': 0, u'page_info': {
            u'end_cursor': None, u'has_next_page': False}, u'edges': []},
        u'edge_media_collections': {u'count': 0,
                                    u'page_info': {u'end_cursor': None,
                                                   u'has_next_page': False},
                                    u'edges': []}, u'is_verified': False,
        u'username': u'tomisin10',
        u'edge_owner_to_timeline_media': {
            u'count': 4,
            u'page_info': {
                u'end_cursor': u'AQBAgvA_CpuVTcu3UVbYfbUvUgCc9lp86sRHKqI-daPW8fYrhKapIqDdlXhmpugbR9k',
                u'has_next_page': False},
            u'edges': [{
                u'node': {
                      u'edge_media_preview_like': {
                          u'count': 49},
                    u'is_video': False,
                    u'edge_media_to_caption': {
                        u'edges': [
                        {
                            u'node': {
                            u'text': u'#NYSC'}}]},
                  u'dimensions': {
                      u'width': 1080,
                      u'height': 1080},
                  u'display_url': u'https://instagram.flos1-1.fna.fbcdn.net/vp/395d765f518fced2d0669c4b2830e99c/5B5A9FC9/t51.2885-15/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                  u'edge_media_to_comment': {
                      u'count': 0},
                  u'comments_disabled': False,
                  u'__typename': u'GraphImage',
                  u'owner': {
                      u'id': u'4275228862'},
                  u'media_preview': u'ACoqvxirQGKiQVDfFvLCLxuOCfQDr+dAxG1KJW2j5vcdKsxypMMoc/zrmREme5rVgi8kq8ecEgMp9D6fQ0AaDCk21MRRigBiiq9/ny8DrmrSim3Me+MgdQMigDm7eB5GI5GAT9fatCw8wf6zOAeh9c8YNUYHkVwFOPy/HrWpYRnzH67c5APagRq4oxSM4QZJximCZDzkUhkT3UcR2sTkdcDpTXvFKnZkk8Djpnv+FZ0/32+ppw46UwMplcOCwz3yP5+1bVuHVdzHG4cYPb+p/l0qG1A8xv8APeooSVmdRwuG47flQImuy3ylmH+e/p+oqMXSgY3D8v8A61Tz9D9F/wDQalRRtHHYUAf/2Q==',
                  u'edge_liked_by': {
                      u'count': 49},
                  u'thumbnail_resources': [
                      {
                          u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/ad2f6a1c5c6152abf49996854aed953f/5B562CAC/t51.2885-15/s150x150/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                          u'config_width': 150,
                          u'config_height': 150},
                      {
                          u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/7f69847308e85be8b75129091d7bea0a/5B52A793/t51.2885-15/s240x240/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                          u'config_width': 240,
                          u'config_height': 240},
                      {
                          u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/3605dc1a662224639a03fbfee60c28b0/5B5118EB/t51.2885-15/s320x320/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                          u'config_width': 320,
                          u'config_height': 320},
                      {
                          u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/2aeb76ad7a28df8719a742a32381de1f/5B39896D/t51.2885-15/s480x480/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                          u'config_width': 480,
                          u'config_height': 480},
                      {
                          u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/f752d2465eb7f272e72d8bd996914414/5B5C35A8/t51.2885-15/s640x640/sh0.08/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                          u'config_width': 640,
                          u'config_height': 640}],
                  u'taken_at_timestamp': 1517433651,
                  u'gating_info': None,
                  u'thumbnail_src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/f752d2465eb7f272e72d8bd996914414/5B5C35A8/t51.2885-15/s640x640/sh0.08/e35/26870242_375510812860517_5633485371237990400_n.jpg',
                  u'shortcode': u'BeoPPV3nVLk',
                  u'id': u'1704679483931185892'}},
              {
                  u'node': {
                      u'edge_media_preview_like': {
                          u'count': 52},
                      u'is_video': False,
                      u'edge_media_to_caption': {
                          u'edges': []},
                      u'dimensions': {
                          u'width': 1080,
                          u'height': 1080},
                      u'display_url': u'https://instagram.flos1-1.fna.fbcdn.net/vp/e2cf3a7f223e4d1acf03abc3c088a1d5/5B547F98/t51.2885-15/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                      u'edge_media_to_comment': {
                          u'count': 6},
                      u'comments_disabled': False,
                      u'__typename': u'GraphImage',
                      u'owner': {
                          u'id': u'4275228862'},
                      u'media_preview': u'ACoq0KpXkhjX5fvHge3vViGTzVDdM1VvhkL9T/KsizORXJ3bjn61t2yeamW6jg/l1rLj3cituwB2kH1/pWi1E1YHgABx/X0zVbFaxTI//X6YrK6UmJEdl/qh9TUlwu5D+n1rPiuTFHtUZbJ69KgNy7nLEgjken4Ucj36DTXclhIPXrXRWq7IxjnPJrnlnjbJlGG7MvGT7jp+NTC4xzHkDAxzzWiQmbspI6envVfywe1R29y0x2t1AzmrmKq1tGZ3OWFNIzThSV0oyYCMNx0BpYhn8KVOlJF3rCWj0O6nFSUb63vf5F62bZID0zx+db2K5wda6QdKJbJnLJWk0trn/9k=',
                      u'edge_liked_by': {
                          u'count': 52},
                      u'thumbnail_resources': [
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/bec0b02c6c506043a7662208fba7b34c/5B4FF3FD/t51.2885-15/s150x150/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                              u'config_width': 150,
                              u'config_height': 150},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/7bf42d7ebb3846d90d9540ae26ed8e09/5B7227C2/t51.2885-15/s240x240/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                              u'config_width': 240,
                              u'config_height': 240},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/65f637ce8248423e7098b6dc86a4f56c/5B5BAEBA/t51.2885-15/s320x320/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                              u'config_width': 320,
                              u'config_height': 320},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/41136c7000ea147aedb9ed58c688561b/5B74133C/t51.2885-15/s480x480/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                              u'config_width': 480,
                              u'config_height': 480},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/0a87864b5962b54b2d02e60c32029e23/5B39E3F9/t51.2885-15/s640x640/sh0.08/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                              u'config_width': 640,
                              u'config_height': 640}],
                      u'taken_at_timestamp': 1508752328,
                      u'gating_info': None,
                      u'thumbnail_src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/0a87864b5962b54b2d02e60c32029e23/5B39E3F9/t51.2885-15/s640x640/sh0.08/e35/22709089_752058838333579_7571815673974751232_n.jpg',
                      u'shortcode': u'Balg7cpnmLI',
                      u'id': u'1631855262692172488'}},
              {
                  u'node': {
                      u'edge_media_preview_like': {
                          u'count': 46},
                      u'is_video': False,
                      u'edge_media_to_caption': {
                          u'edges': [
                              {
                                  u'node': {
                                      u'text': u"Love the STM shirts ?\U0001f918 Em' beauties rocking em'\n@zitastones\n@minika.xo\n@ally3oooo \U0001f468\u200d\U0001f467\u200d\U0001f466\U0001f45a\U0001f455\U0001f45a\nDm @stuffs_that_matter to get yours at N3000 only\nCc: @hilaysanmie\n@xzaminer\n@jemilarh\n@harbiolah\n@official_flopey\n@kiishi_x \n@itz_lamilekan \n@t_e_d_d_i_e ...delivery across all campuses\n#stuffsTM #eksu #crawford #jabu #covenant #unilag #uinben #futa #abuad"}}]},
                      u'dimensions': {
                          u'width': 750,
                          u'height': 750},
                      u'display_url': u'https://instagram.flos1-1.fna.fbcdn.net/vp/e8deea4870a251b385a3793aa45592ed/5B54B0BE/t51.2885-15/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                      u'edge_media_to_comment': {
                          u'count': 2},
                      u'comments_disabled': False,
                      u'__typename': u'GraphImage',
                      u'owner': {
                          u'id': u'4275228862'},
                      u'media_preview': u'ACoq1UvomOM4+tWd6g4yMjtXJMdq+/8AWrS3JwB+dDfYNjeefA3AfL/n/PFVJpVJwy5DDjJ71lGXzXIb5gvQHj/P1p0dwcYxv2k47g4/wqQa6kzPsK46r6A/lz1/lS/aM87GP4VYS6DMN4UA8Ngf48fl/wDWpzWUmTtI25457dqLXEiBYbd24wWBzjJ7enqPzqoIC90VAwobJ/3ev69PxqNZlEqnJfYB9M4x39+a1JmCnK8O3GR1xVSklZAlchazj80sc884A6ZpiwJFC5U5+bKnoR7A9c/1rQ2KTnHPrVa4UKBgfKCc1bskJXbs2ZaPtlX3YDmldiGI3gcn1qa6jBBMYyOAAOu44x/PP4Yq19jB5KDPf/OaiLuhtPoZsYRVyBlv72Tz+HSrsdypk3PycY47e4FZa/cNPg+5nvUPuzay28jXe7AHy9c45qYkOoHBz1FYqk4/GnCRh0JH4mm3zK3oJpW07sttmMHsV45/iB5H4jtUPnn3p9oS8nzfN068/wA60GUAngdaajcFUdPbqf/Z',
                      u'edge_liked_by': {
                          u'count': 46},
                      u'thumbnail_resources': [
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/2976727910863c571fcd92ca57669cd7/5B5C74DB/t51.2885-15/s150x150/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                              u'config_width': 150,
                              u'config_height': 150},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/590d7b84b2a4dc929b9a0cfd4739dccf/5B6876E4/t51.2885-15/s240x240/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                              u'config_width': 240,
                              u'config_height': 240},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/4f3b8cc992fa8503f6458c2bd12d7e72/5B58129C/t51.2885-15/s320x320/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                              u'config_width': 320,
                              u'config_height': 320},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/ca5dc4ae867a60153b141dcb43795b58/5B3E021A/t51.2885-15/s480x480/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                              u'config_width': 480,
                              u'config_height': 480},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/ab3815fe8f3b4e0ac36e15730f4e3426/5B6B21DF/t51.2885-15/s640x640/sh0.08/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                              u'config_width': 640,
                              u'config_height': 640}],
                      u'taken_at_timestamp': 1504816292,
                      u'gating_info': None,
                      u'thumbnail_src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/ab3815fe8f3b4e0ac36e15730f4e3426/5B6B21DF/t51.2885-15/s640x640/sh0.08/e35/21435658_257652491409522_2144221654698426368_n.jpg',
                      u'shortcode': u'BYwNiYxn50w',
                      u'id': u'1598837405385727280'}},
              {
                  u'node': {
                      u'edge_media_preview_like': {
                          u'count': 42},
                      u'is_video': False,
                      u'edge_media_to_caption': {
                          u'edges': [
                              {
                                  u'node': {
                                      u'text': u"Love the STM shirts ?\U0001f918\nThe main geez had to rock em'\n@zitastones\n@olajiwon_re\n@minika.xo\n@ally3oooo\nrepping #ABUAD\n\U0001f468\u200d\U0001f467\u200d\U0001f466\U0001f45a\U0001f455\U0001f45a\nDm @stuffs_that_matter to get yours at N3000 only\nCc: @hilaysanmie\n@xzaminer\n@jemilarh\n@harbiolah\n@official_flopey\n@kiishi_x \n@itz_lamilekan\n@t_e_d_d_i_e ...delivery across all campuses\n#stuffsTM #eksu #crawford #jabu #covenant #unilag #uinben #futa #allcampuses"}}]},
                      u'dimensions': {
                          u'width': 640,
                          u'height': 640},
                      u'display_url': u'https://instagram.flos1-1.fna.fbcdn.net/vp/6c761cd32af3312c7f46f9ef33781bd7/5B6BA250/t51.2885-15/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                      u'edge_media_to_comment': {
                          u'count': 0},
                      u'comments_disabled': False,
                      u'__typename': u'GraphImage',
                      u'owner': {
                          u'id': u'4275228862'},
                      u'media_preview': u'ACoq0g6t0p1OAOKCpNa3MxKWjbS4oAbRiq8tx5Z2jr7/AM6gF2/oTU8yCw6eYw7ZVBJlxnr0AOOBnAOf05xSpcSM5jzhgg5HTdnPAPYr19D1wKakmJc7vlCMNv0xwP1471RVVWDzejb8dedpPKnHY9/TpWaelzQuy3sqbshfUAc4z8oB9wQSeeRU5Ltk7tyjacL8p4AYnJ4I65Az6VkzP5jN/D8uCAeo5I/HJ/TnNWbo7YwsbMABjgnIGzJX6E4p3CxDqReIqjvvLAnp0GcgdSeSPbFIkcu0fd6Dr/8AqpJYRIOMseMEnJAz0/AdqtoqbRx2FZuRVh+0GLecFsdapLMxHr+Aq0f9XVNBwab9BIVZmPQ/oP8ACr0D7lJbrVBBVmP7tDt2AZO2DgcfgKYJTipZBUGKkaP/2Q==',
                      u'edge_liked_by': {
                          u'count': 42},
                      u'thumbnail_resources': [
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/e4bb81b0097657aa5c8b265047b9abf0/5B5ADF35/t51.2885-15/s150x150/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                              u'config_width': 150,
                              u'config_height': 150},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/157d48130a6466a63682104b0dc31e1d/5B39E00A/t51.2885-15/s240x240/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                              u'config_width': 240,
                              u'config_height': 240},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/610714840b10eeb5d7c51f6cd204d9ea/5B597172/t51.2885-15/s320x320/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                              u'config_width': 320,
                              u'config_height': 320},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/c4526bf42fc9bf1bc73dfa3d29da1158/5B544FF4/t51.2885-15/s480x480/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                              u'config_width': 480,
                              u'config_height': 480},
                          {
                              u'src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/6c761cd32af3312c7f46f9ef33781bd7/5B6BA250/t51.2885-15/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                              u'config_width': 640,
                              u'config_height': 640}],
                      u'taken_at_timestamp': 1504816273,
                      u'gating_info': None,
                      u'thumbnail_src': u'https://instagram.flos1-1.fna.fbcdn.net/vp/6c761cd32af3312c7f46f9ef33781bd7/5B6BA250/t51.2885-15/e35/21373443_101251123952068_1095010931546521600_n.jpg',
                      u'shortcode': u'BYwNgBdn2t_',
                      u'id': u'1598837242915154815'}}]},
        u'country_block': False, u'edge_followed_by': {u'count': 328},
        u'mutual_followers': None,
        u'profile_pic_url': u'https://instagram.flos1-1.fna.fbcdn.net/vp/c6093d553d8ad22a836b71a777aa73cb/5B54CB8B/t51.2885-19/s150x150/27573533_2026171430939166_3017211295624593408_n.jpg',
        u'is_private': False, u'connected_fb_page': None,
        u'requested_by_viewer': False,
        u'profile_pic_url_hd': u'https://instagram.flos1-1.fna.fbcdn.net/vp/8059867f2274d1de15049dd4222e63c3/5B51C7F3/t51.2885-19/s320x320/27573533_2026171430939166_3017211295624593408_n.jpg',
        u'has_blocked_viewer': False,
        u'external_url': u'http://marine-album-170216.appspot.com/'}}}


general_accounts = []
interests_accounts = {
    'Politics': ['nytimes', 'theeconomist'],
    'Art': [],
    'Startups': ['500Startups'],
    'Technology': ['techcrunch', 'thenextweb', 'theverge', 'recode', 'ign', 'ProgrammingWrld'],
    'Literature': ['nybooks', 'time'],
    'Oil & Gas': [],
    'Finance': ['bloomberg', 'theeconomist', 'businessinsider', 'cnbc'],
    'Academics': ['newscientist', 'natgeo', ],
    'Lifestyle': ['telegraphluxury', ],
    'Health & Medicine': ['natgeo', 'newscientist'],
    'Tourism': ['buzzfeed', 'dailymail'], 'Leadership': ['cnbc', 'tedx'],
    'Fashion': ['ew', 'timesfashion', 'mashable', 'voguemagazine'],
    'Entertainment': ['mtvnews', 'buzzfeed', 'dailymail', 'mashable'],
    'Law & Order': ['thewallstreetjournal'],
    'Sports': ['bbcsport', 'espn', 'skysports'],
    'ScIEntific Research': ['newscientist', 'natgeo', ],
    'Gaming': ['polygon', 'recode', 'ign', 't3n']}


def _gen_image_filename():
    random_number = random.randint(1, 10000)
    if os.path.exists('cache'):
        return 'cache/{}.jpg'.format(random_number)

    return '{}.jpg'.format(random_number)


def parse_post(post, *fullResult):
    # print dir(post)
    # print dir(post.user)
    # print dir(post.caption)
    # print dir(post.images['standard_resolution'])

    text = ''
    likes = post.like_count
    comments = post.comment_count
    img_source = post.images
    username = post.user.username
    profile_pic = post.user.profile_picture
    post_date = post.created_time
    post_type = 'photo'
    
    try:
        text = post.caption.text
    except:
        pass

    if isinstance(img_source, list):
        img_source = [img['standard_resolution'].url for img in img_source]
    else:
        img_source = img_source['standard_resolution'].url

    # if post['is_video']:
    #     post_type = 'video'

    content = {'type': post_type, 'text': str(text.encode('utf-8').strip()),
               'source': img_source}

    trail_post_form = {
        'author': username,
        'app': 'instagram',
        'profile_photo_url': profile_pic,
        # 'source':img_source,
        'content': str(content),
        'created_at': post_date,
        'external_engagements': str({
            'likes': {
                'including_user': None,
                'total': likes
            },
            'comments': {
                'including_user': None,
                'total': comments
            }
        })
    }

    return trail_post_form


def create_client(username=None, access_token=None, default=False):
    client_secret = DEFAULT_CLIENT_SECRET

    if default:
        access_token = DEFAULT_ACCESS_TOKEN

    elif access_token:
        access_token = access_token['oauth_token']

    else:
        access_token = get_auth_data(app_name='instagram', username=username)[0]

    from application.wrappers.libs.instagram.client import InstagramAPI

    # access_token = access_token
    client_secret = client_secret
    
    api = InstagramAPI(client_id=CLIENT_ID, client_secret=client_secret,
                       access_token=access_token)
    # print 'API >>> %s' % api.user_recent_media('cristiano', 10)
    return api


def create_login_client(username, password):
    instagram_api = InstagramAPI(username, password)
    instagram_api.login()  # login

    return instagram_api


def cache_new_entries(new_entries):
    app = 'instagram'
    return


def save_posts_by_user(linked_trails_account, posts):
    return


def get_timeline(ext_app_trails_user_username):
    api = create_client(ext_app_trails_user_username)

    result, next = api.user_recent_media()

    posts = []
    for status in result:
        parsed_post = parse_post(status)

        entry = create_entry_from_parsed('instagram', parsed_post)
        save_entry_if_new(entry)

        posts.append(entry.header)

    return posts


def get_user_photos_from_timeline(instagram_timeline):
    for payload in instagram_timeline['media']['nodes']:
        print(payload['display_src'])


def get_user_posts(instagram_username):
    api = create_client(default=True)

    result, next = api.user_recent_media(user_id=21212)

    posts = []
    for status in result:
        parsed_post = parse_post(status)
        posts.append(parsed_post)

        entry = create_entry_from_parsed('instagram', parsed_post)
        save_entry_if_new(entry)

    return posts

    # for post in result:['media']['nodes']:
    #     post['id'] = str(post['id'])
    #     extracted_ = parse_post(post, result)
    #
    #     all_feeds.append(extracted_)
    #
    # return all_feeds


def get_popular():
    popular_media = api.media_popular(count=20)
    for media in popular_media:
        thisUrl = media.images['standard_resolution'].url


def follow_user(username):
    'We should follow users on their registration.'


def login(username):
    'follow users from our own account, save username in users db for birthday, observation shout-outs.'


def update_status(title, body, file_url, source_url=None, tags=None, type=None):
    from flask import current_app

    username = current_app.config.get('INSTAGRAM_USERNAME')
    password = current_app.config.get('INSTAGRAM_PASSWORD')

    InstagramAPI = create_login_client(username, password)
    caption = title
    # caption = '{} -- {}'.format(title, body)

    if tags:
        for tag in tags:
            if tag:
                caption += ' #{}'.format(tag)

    if source_url:
        caption += '\n\n{}'.format(source_url)

    if not file_url:
        return

    image_bytes = requests.get(file_url).content

    image_fn = _gen_image_filename()
    open(image_fn, 'wb').write(image_bytes)

    if type is None or type == 'image':
        try:
            InstagramAPI.uploadPhoto(image_fn, caption=caption)
        except RuntimeError:
            print('Unsuccessful upload. Image has an unsupported format.')
            pass

    elif type == 'video':
        video_url = 'https://instagram.fmad3-2.fna.fbcdn.net/t50.2886-16/17157217_1660580944235536_866261046376005632_n.mp4'  # a valid instagram video
        video_local_path = video_url.split("/")[-1]
        thumbnail_url = "https://instagram.fmad3-2.fna.fbcdn.net/t51.2885-15/e15/17075853_1759410394387536_3927726791665385472_n.jpg"
        thumbnail_local_path = thumbnail_url.split("/")[-1]

        urllib.urlretrieve(video_url, video_local_path)
        urllib.urlretrieve(thumbnail_url, thumbnail_local_path)

        InstagramAPI.uploadVideo(
            video_local_path, thumbnail_local_path, caption="Tortuguero")


def fetch_user_timeline(username):
    'first get the posts on DB. get_timeline(), get the new DB().'
    old_cached_app_data_by_username = {}

    # from trail_cloud_handler import DB
    # appData = DB().get3PData('instagram')

    try:
        users_posts = get_timeline(username)
        users_posts.reverse()     #now older to newer
    except:
        users_posts = old_cached_app_data_by_username[username]['posts']

    needed_posts = users_posts[len(users_posts) - 10:]
    return needed_posts


def get_avatar(username, save_as):
    result = IE.user(username).data
    full_url = result['profile_pic_url']

    the_page = requests.get(full_url)
    thisDownloadedImageData = the_page.read()

    a = open(save_as, 'wb')
    a.write(thisDownloadedImageData)
