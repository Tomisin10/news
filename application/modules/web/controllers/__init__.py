from collections import OrderedDict

from flask import g, redirect, render_template

from application.articles.helpers import (
    get_compilation,
    get_existing_section_headlines,
    get_world_headlines,
    get_existing_news_source_headlines)
from application.readers.helpers import (
    get_current_reader_events,
    get_current_reader_location,
    get_current_reader_weather)
from application.videos.helpers import (
    get_video_headlines, get_popular_youtube_videos)
from application.core.models import Article, NewsSource, Section
from application.modules.web import web_blueprint


SITE_META = {
    'site_name': 'Disruptivity',
    'site_description': 'A news aggregator platform.'
}

url_pages_map = OrderedDict()
url_pages_map['/'] = 'Home'
url_pages_map['/advertise'] = 'Advertise'

NAVIGATION_DATA = {
    'active_page': 'Home',
    'navigation': url_pages_map
}


def _add_index(list__):
    for item in list__:
        item['index'] = list__.index(item)

    return list__


def _add_section_articles(sections):
    for section in sections:
        section_orm = Section.get(id=section['id'])

        section_articles = get_existing_section_headlines(section_orm)

        section['articles'] = {
            'headlines': section_articles[:5],
            'regular': section_articles[5:10]
        }

    return sections


def _add_source_articles(sources):
    for source in sources:

        source_orm = NewsSource.get(id=source['id'])

        source_articles = get_existing_news_source_headlines(source_orm)

        source['articles'] = {
            'headlines': source_articles[:5],
            'regular': source_articles[5:10]
        }

    return sources


def _remove_recurring_items(sources):
    source_titles = []
    sources_ = []

    for source in sources:
        if source['title'] in source_titles:
            continue

        source_titles.append(source['title'])
        sources_.append(source)

    return sources_


@web_blueprint.route('/')
def render_home_page():
    sections = Section.query.all()
    sources = NewsSource.query.all()

    sections = [section.as_json() for section in sections]
    sections = _add_section_articles(sections)

    sources = [source.as_json() for source in sources]
    sources = _remove_recurring_items(sources)
    sources = _add_source_articles(sources)

    g.reader_location = get_current_reader_location()

    context = {
        'weather_data': get_current_reader_weather(),
        'compilation': get_compilation(),
        'headlines': get_world_headlines(page=1, per_page=3),
        'sections': _add_index(sections),
        'top_stories': get_world_headlines(page=1, per_page=12),
        'weekly_roundup': get_world_headlines(page=1, per_page=5),
        'todays_pick': get_world_headlines(page=1, per_page=1)[0],
        'sources': _add_index(sources),
        'popular_videos': get_popular_youtube_videos(page=1, per_page=12),
        'video_feeds': get_video_headlines(section=None, source=None),
        'events': get_current_reader_events(),
        'reader_location': g.reader_location,
    }

    context.update(SITE_META)
    context.update(NAVIGATION_DATA)

    return render_template('index.html', **context)


@web_blueprint.route('/sections/<section_uid>')
def render_section_page(section_uid):
    section = Section.get(uid=section_uid) or Section.get(id=section_uid)

    g.reader_location = get_current_reader_location()

    context = {
        'articles': [article.as_json() for article in section.articles[:25]],
        'top_stories': get_world_headlines(page=1, per_page=12),
        'title': section.name.title(),
        'popular_videos': get_popular_youtube_videos(page=1, per_page=12),
        'weather_data': get_current_reader_weather(),
        'events': get_current_reader_events(),
        'reader_location': g.reader_location,
    }

    context.update(SITE_META)
    context.update(NAVIGATION_DATA)

    return render_template('category.html', **context)


@web_blueprint.route('/sources/<source_uid>')
def render_source_page(source_uid):
    news_source = (
        NewsSource.get(uid=source_uid) or NewsSource.get(id=source_uid)
    )

    g.reader_location = get_current_reader_location()

    context = {
        'articles': [article.as_json() for article in news_source.articles[:25]],
        'top_stories': get_world_headlines(page=1, per_page=12),
        'title': news_source.title.title(),
        'popular_videos': get_popular_youtube_videos(page=1, per_page=12),
        'weather_data': get_current_reader_weather(),
        'events': get_current_reader_events(),
        'reader_location': g.reader_location,
    }

    context.update(SITE_META)
    context.update(NAVIGATION_DATA)

    return render_template('category.html', **context)


@web_blueprint.route('/articles/<article_uid>')
def render_article_page(article_uid):
    article = Article.get(uid=article_uid) or Article.get(id=article_uid)

    similar_articles = get_existing_section_headlines(article.section)

    if article.news_source.category == 'NEWS_API':
        return redirect(article.article_url)

    g.reader_location = get_current_reader_location()

    context = {
        'article': article.as_json(),
        'similar_articles': similar_articles[:5],
        'top_stories': get_world_headlines(page=1, per_page=12),
        'weather_data': get_current_reader_weather(),
        'events': get_current_reader_events(),
        'reader_location': g.reader_location,
        'popular_videos': get_popular_youtube_videos(page=1, per_page=12)
    }

    context.update(SITE_META)
    context.update(NAVIGATION_DATA)

    return render_template('post.html', **context)
