import random

from flask import make_response, request

from application.articles.helpers import (
    get_existing_trends)
from application.core.utils.request_response_helpers import (
    api_success_response)
from application.modules.api import api_blueprint
from .articles import Articles
from .headlines import (
    AuthorHeadlines,
    SectionHeadlines,
    NewsSourceHeadlines,
    TagHeadlines)
from .incoming_sms import IncomingSMS
from .lookups import (
    NewsSourcesLookUp,
    SectionsLookUp)
from .payments import Payments
from .readers import Readers
from .subscriptions import Subscriptions


@api_blueprint.route('')
def say_hi():
    # task_say_hello()

    return api_success_response('Welcome to Disruptivity API')


@api_blueprint.route('/static/thumbnails/section', methods=['GET'])
def show_section_assets():
    section = request.args.get('name')

    colors = ['blue', 'brown', 'green', 'grey', 'lemon', 'pink']
    filedata = (
        open('static/thumbnails/sections/{}.png'.format(
            random.choice(colors)), 'rb').read()
    )

    response = make_response(filedata)
    response.mimetype = 'image'

    return response


@api_blueprint.route('/static/<resource>', methods=['GET'])
def show_static_assets(resource):
    filedata = (
        open('static/{}.png'.format(resource), 'rb').read()
    )

    response = make_response(filedata)
    response.mimetype = 'image'

    return response


@api_blueprint.route('/trends')
def show_trends():
    return get_existing_trends()


@api_blueprint.route('/calendars/<year>/<month>')
def get_calendar(year, month):
    """Get headlines, on this days, etc., for the month"""
    return


@api_blueprint.route('/legal/privacy-policy', methods=['GET'])
def get_privacy_policy():
    return 'Your data is private. Honestly!'


mappings = [
    ('/articles', Articles, 'articles'),
    ('/articles/<article_uid>', Articles, 'article'),

    ('/authors/<author_uid>/headlines', AuthorHeadlines, 'author_headlines'),
    ('/sections/<section_uid>/headlines', SectionHeadlines,
     'section_headlines'),
    ('/sources/<source_uid>/headlines', NewsSourceHeadlines,
     'source_headlines'),
    ('/tags/<tag_uid>/headlines', TagHeadlines, 'tag_headlines'),

    ('/readers', Readers, 'readers'),
    ('/readers/<reader_uid>', Readers, 'reader'),

    ('/readers/<reader_uid>/subscriptions', Subscriptions, 'subscriptions'),
    ('/readers/<reader_uid>/subscriptions/<subscription_uid>', Subscriptions,
     'subscription'),

    ('/news-sources', NewsSourcesLookUp, 'news_sources'),
    ('/sections', SectionsLookUp, 'sections'),

    ('/incoming-sms', IncomingSMS, 'incoming_sms'),
    ('/payments', Payments, 'payments'),
]


for url in mappings:
    path, view, name = url

    api_blueprint.add_url_rule(path, view_func=view.as_view(name))
