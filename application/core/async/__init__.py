from inspect import getmembers, isfunction

# from celery import Celery
from rq import Queue

from worker import conn
import tasks
# import jobs


q = Queue(connection=conn)


def background_task(func, *args, **kwargs):
    q.enqueue(func, *args, **kwargs)


def is_task(object_):
    return isfunction(object_) and object_.__name__.startswith('task_')


def is_job(object_):
    return isfunction(object_) and object_.__name__.startswith('cron_')


def get_tasks():
    # nv_pair -> name-value pair
    return [nv_pair[1] for nv_pair in getmembers(tasks, is_task)]


def get_jobs():
    # nv_pair -> name-value pair
    return [nv_pair[1] for nv_pair in getmembers(jobs, is_job)]


def make_tasks(_celery):
    for task in get_tasks():
        setattr(tasks, task.__name__, _celery.task()(task))


def make_jobs(_celery):
    for job in get_jobs():
        setattr(jobs, job.__name__, _celery.task()(job))


# TODO implement make background tasks
def make_background_tasks():
    for task in get_tasks():
        setattr(tasks, task.__name__, lambda: background_task(task))


def make_celery(app):
    # celery = Celery(app.import_name, include=app.config.get('CELERY_INCLUDE'))
    # celery.conf.update(app.config)
    # task_base = celery.Task
    #
    # class ContextTask(task_base):
    #     abstract = True
    #
    #     def __call__(self, *args, **kwargs):
    #         with app.app_context():
    #             return task_base.__call__(self, *args, **kwargs)
    #
    # celery.Task = ContextTask
    # make_tasks(celery)
    # make_jobs(celery)
    # return celery

    make_background_tasks()
