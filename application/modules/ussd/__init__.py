"""API blueprint"""
# third-party library imports
from flask import Blueprint

ussd_blueprint = Blueprint('ussd', __name__, url_prefix='/ussd/v1.0')

from .controllers import *
