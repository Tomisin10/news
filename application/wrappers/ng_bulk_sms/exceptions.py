class NGBulkSMSException(Exception):

    def __init__(self, message=None):
        message = message or 'NGBulkSMS Exception!'

        super(NGBulkSMSException, self).__init__(message)


class NGBulkSMSGatewayError(NGBulkSMSException):
    default_message = 'NGBulkSMS Gateway Error!'

    def __init__(self, message=None):
        message = ('NGBulkSMS Gateway Error!'
                   + ' {}'.format(message or '')).rstrip()

        super(NGBulkSMSGatewayError, self).__init__(message
                                                    or self.default_message)


class NGBulkSMSAPIError(NGBulkSMSException):
    default_message = 'NGBulkSMS API Error!'

    def __init__(self, message=None, api_msg=None, api_code=None):
        message = message or ''

        if api_msg is not None:
            message += ' [Msg.: "{}"]'.format(api_msg)

        if api_code is not None:
            message += ' [Code: ({})]'.format(api_code)

        super(NGBulkSMSAPIError, self).__init__(message
                                                or self.default_message)


class NGBulkSMSUnhandledError(NGBulkSMSException):
    default_message = 'Unhandled NGBulkSMS Error!'

    def __init__(self, message=None, http_code=None):
        message = message or ''

        if http_code is not None:
            message = (message +
                       ' [HTTP Response Code: {}]'.format(http_code)).strip()

        super(NGBulkSMSUnhandledError, self).__init__(message
                                                      or self.default_message)
