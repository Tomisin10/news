from flask.views import MethodView

from application.core.models import NewsSource, Section
from application.core.utils.request_response_helpers import api_success_response


class LookUp(MethodView):
    pass


class NewsSourcesLookUp(LookUp):
    def get(self):
        return api_success_response(
            [news_source.as_json() for news_source in NewsSource.query.all()]
        )


class SectionsLookUp(LookUp):
    def get(self):
        return api_success_response(
            [section.as_json() for section in Section.query.all()]
        )
