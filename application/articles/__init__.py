from collections import namedtuple


Article = namedtuple('Article', 'title content tags images videos')
