from flask import g, request

from .datetime_wrapper import DateTimeWrapper


def current_request_data():
    request_data = request.get_json() or dict(request.args)

    if request_data is None:
        request_data = {}

    return request_data


def format_minute(minute):
    return '0{}'.format(minute) if int(minute) < 10 else str(minute)


def get_section_thumbnail(section):
    # TODO implement get_section_thumbnail

    return '%sapi/v1.0/static/thumbnails/section?name=%s' % (
        request.host_url, section)


def get_app_icon_url():
    return '%sapi/v1.0/static/app_icon' % request.host_url


SUFFIXES = {1: 'st', 2: 'nd', 3: 'rd'}
def ordinal(num):
    # I'm checking for 10-20 because those are the digits that
    # don't follow the normal counting scheme.
    if 10 <= num % 100 <= 20:
        suffix = 'th'
    else:
        # the second parameter is a default.
        suffix = SUFFIXES.get(num % 10, 'th')
    return str(num) + suffix