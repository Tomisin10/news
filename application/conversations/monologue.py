import random

from flask import g


class Monologue(object):
    @classmethod
    def welcome(cls):
        welcome_statements = [
            "Hi %s, I'm Disruptiv! B)" % g.reader.first_name,
            # "If you had ever wished you had total control of the news that "
            # "got to you, you are in luck!",
            "I go around the top news networks, filtering in only what people "
            "are interested in.",
            "So... What's your interest?"
        ]

        return welcome_statements

    @classmethod
    def apologize(cls):
        apologies = [
            "I'm 'berry' sorry. But I really don't understand what you mean.",
            "Come tomorrow, I'll probably understand that.",
            "I may not know what you mean now, but I'll find out"
        ]
        return random.choice(apologies)

    @classmethod
    def blush(cls):
        blushes = [
            "You're too kind!",
            "Awwwn... Thanks!",
            "It's what I was made to do. O:)",
            ':">'
        ]
        return random.choice(blushes)

    @classmethod
    def compliment(cls):
        random_statements = [
            '%s, I must say... You have a great taste. ;)' % g.reader.first_name,
            'Awesome! Get for two, please? :) :)',
            'Sweet!',
            'Terrific!',
            'Way to turn on the envy.',
            'Your friends are gonna be so damn jealous! B)',
            'Great choice, %s.' % g.reader.first_name,
            'Fabulous!',
            'Excellent.',
            'This is such a fine product.',
            'Hmmn... Rad!'
        ]
        return random.choice(random_statements)

    @classmethod
    def greet(cls):
        random_statements = [
            "I'm glad you came back, %s!" % g.reader.first_name,
            'Oh, hello %s!' % g.reader.first_name,
            '%s! Welcome back. :)' % g.reader.first_name,
        ]
        return random.choice(random_statements)

    @classmethod
    def random(cls):
        random_statements = [
            'You are the best.',
            'For you, I am here always.',
            'Forever at your service'
        ]
        return random.choice(random_statements)

    @classmethod
    def thank(cls):
        return

    @classmethod
    def thank_for_patronage(cls):
        return random.choice(
            ['Thanks for your patronage.', "We're grateful to know you :)"])

    @classmethod
    def take_to_all_sections(cls):
        return random.choice(
            ['What do you really care about?',
             "What's on your mind, %s?" % g.reader.first_name,
             "Here are what we currently cover:",
             "Where would you like to start? ",
             "Have a look around..."]
        )

    @classmethod
    def direct_to_checkout(cls):
        return

    @classmethod
    def take_to_all_news_sources(cls):
        return random.choice([
            'Which would you like to read from?',
            'We get content from these:',
            'Lots of channels to choose from...',
        ])

    @classmethod
    def take_to_news_source(cls, news_source):
        return random.choice([
            'The latest stories from %s:' % news_source.title.upper(),
            "Here are %s's headlines:" % news_source.title.upper(),
            'Headlines from %s:' % news_source.title.upper()
        ])

    @classmethod
    def process_completion(cls):
        return random.choice([
            "Done!", 'Task complete.', "It's done.", 'Done. Anything else?',
            'That was easy.', "Give me a minu... I'm done and back! B)",
            'All done! O:)'
        ])

    @classmethod
    def take_to_section(cls):
        all_statements = ['Here are the latest headlines:']
        return random.choice(all_statements)

    @classmethod
    def show_cart(cls):
        return random.choice(
            ['You added these to your cart recently:', 'Here it is:']
        )

    @classmethod
    def support_item_drop_decision(cls):
        return random.choice([
            "I'll take it back the shelf :)", "Just as well...",
            "You have an eye on your wallet it seems..."
        ])

    @classmethod
    def on_arrival_at_aisle(cls):
        return

    @classmethod
    def take_to_all_brief_sources(cls):
        return random.choice([
            "Here are your brief's sources:",
            "Your brief's sources, %s:" % g.reader.first_name
        ])

    @classmethod
    def support_brief_source_drop_decision(cls):
        return random.choice([
            'Shedding some boring sections, I see :)',
            "It's best to have a slim pack, in my opinion."
        ])

    @classmethod
    def direct_to_all_brief_sources(cls):
        return random.choice([
            'You can access your Brief in the Menu below',
            'Tap "Brief" in the Menu below, to access them.',
        ])

    @classmethod
    def enquire_add_to_brief(cls, news_source_title):
        news_source_title = news_source_title.upper()

        return random.choice([
            "Should I add %s's stories to your daily briefs?" %
            news_source_title,
            "Would you like stories from %s in your daily briefs?" %
            news_source_title,

        ])

if __name__ == '__main__':
    print(Monologue.direct_to_checkout())
