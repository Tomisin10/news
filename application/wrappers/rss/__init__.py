import logging
from datetime import datetime
import feedparser


class time(object):
    @staticmethod
    def struct_time(**kwargs):
        return


WORLD_NEWS_LINK = ('http://rss.cnn.com/rss/edition_africa.rss',)
ENGLISH_PREMIERSHIP_LINK = (
    'http://feeds.sport24.co.za/articles/Sport/Soccer/EnglishPremiership/rss',)
SOCCER_LINK = ('http://feeds.sport24.co.za/articles/Sport/Soccer/rss',)
SPORTS_LINK = (
    'http://feeds.sport24.co.za/articles/Sport/Featured/TopStories/rss',)
AFRICAN_NEWS_LINK = (
    'http://saharareporters.com/feeds/latest/feed',)
NIGERIAN_NEWS_LINK = ('https://www.naija.ng/rss/all.rss',)
ENTERTAINMENT_LINK = (
    'http://syndication.eonline.com/syndication/feeds/rssfeeds/topstories.xml',)
FASHION_LINK = (
    'http://syndication.eonline.com/syndication/feeds/rssfeeds/style.xml',)


def _get_datetime_from_timestamp(timestamp):
    'Thu, 31 May 2018 16:54:43 +0200'
    if all(chr in timestamp for chr in ['-', 'T', ':']) or all(
            chr in timestamp for chr in ['-', ' ', ':']):

        timestamp = timestamp.replace('T', ' ')
        timestamp = timestamp.split('+')[0]

        date, time = timestamp.split(' ')
        date_units = date.split('-')
        if len(date_units[0]) != 4:
            date_units.reverse()

        date_units_int = [int(unit) for unit in date_units]
        time_units_int = [int(unit) for unit in time.split(':')]

        return datetime(*date_units_int, *time_units_int)

    else:
        day_name, day_no, month_name, year_no, time, timezone = timestamp.split(' ')
        time_units = [int(unit) for unit in time.split(':')]

        months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct',
            'Nov', 'Dec'
        ]

        return datetime(
            int(year_no), months.index(month_name)+1, int(day_no), *time_units)


def save_post(json, news_source_id, section_id):
    from app import application
    from application.core.models import Article
    from application.articles.helpers import create_new_article

    image_url = None

    title = json['title']
    body = json['summary']
    links = json['links']

    try:
        created_at = _get_datetime_from_timestamp(json['published'])
    except:
        print('ERROR PARSING DATETIME FOR %s' % json)
        created_at = datetime.now()

    link_to_article = links[0]

    for link in links:
        if link['type'].startswith('image'):
            image_url = link['href']

    if image_url is None:
        try:
            image_url = json['media_content'][0]['url']
        except:
            pass

    # with application.app_context():
    create_new_article(
        title=title, body=str(body), image_url=image_url,
        link=link_to_article['href'], created_at=created_at,
        news_source_id=news_source_id, section_id=section_id
    )


def fetch_from_url(url, news_source_id, section_id):
    entries = feedparser.parse(url)['entries']

    # logging.info('ABOUT TO VISIT: {}'.format(url))
    # try:
    #     feeds = feedparser.parse(url)
    # except AttributeError:
    #     return

    return entries

    for article_json in entries:
        save_post(article_json, news_source_id, section_id)


if __name__ == '__main__':
    feeds = feedparser.parse(WORLD_ENTERTAINMENT_LINK[0])
    for entry in feeds['entries']:
        print(entry)
        print('\n\n\n')

        E_ONLINE = {'id': 'b940525',
                    'summary': '<img src="http://images.eonline.com/resize/66/66//eol_images/Entire_Site/2018431//rs_600x600-180531145108-600--john-mayer-andy-cohen053118.jpg"  height="66" width="66" border="0" alt="John Mayer, Andy Cohen " align="left" hspace="5" />John Mayer and Andy Cohen are not your average bromance.\r\nOn one end of the spectrum, you have a Grammy-winning rock star who sells out arenas across the world. The other side includes a...<br clear="all" />',
                    'links': [{'type': 'text/html',
                               'href': 'http://www.eonline.com/news/940525/inside-john-mayer-s-unlikely-hollywood-friendship-with-andy-cohen?cmpid=rss-000000-rssfeed-365-topstories&utm_source=eonline&utm_medium=rssfeeds&utm_campaign=rss_topstories',
                               'rel': 'alternate'}],
                    'title': "Inside John Mayer's Unlikely Hollywood Friendship With Andy Cohen",
                    'published': 'Fri, 01 Jun 2018 10:00:00 GMT',
                    'summary_detail': {
                        'value': '<img src="http://images.eonline.com/resize/66/66//eol_images/Entire_Site/2018431//rs_600x600-180531145108-600--john-mayer-andy-cohen053118.jpg"  height="66" width="66" border="0" alt="John Mayer, Andy Cohen " align="left" hspace="5" />John Mayer and Andy Cohen are not your average bromance.\r\nOn one end of the spectrum, you have a Grammy-winning rock star who sells out arenas across the world. The other side includes a...<br clear="all" />',
                        'type': 'text/html', 'language': None,
                        'base': 'http://syndication.eonline.com/syndication/feeds/rssfeeds/topstories.xml'},
                    'published_parsed': time.struct_time(tm_year=2018, tm_mon=6,
                                                         tm_mday=1, tm_hour=10,
                                                         tm_min=0, tm_sec=0,
                                                         tm_wday=4, tm_yday=152,
                                                         tm_isdst=0),
                    'guidislink': False, 'title_detail': {
                'value': "Inside John Mayer's Unlikely Hollywood Friendship With Andy Cohen",
                'type': 'text/plain', 'language': None,
                'base': 'http://syndication.eonline.com/syndication/feeds/rssfeeds/topstories.xml'},
                    'link': 'http://www.eonline.com/news/940525/inside-john-mayer-s-unlikely-hollywood-friendship-with-andy-cohen?cmpid=rss-000000-rssfeed-365-topstories&utm_source=eonline&utm_medium=rssfeeds&utm_campaign=rss_topstories'}


        EPL = {
            'link': 'https://www.sport24.co.za/Soccer/EnglishPremiership/everton-appoint-marco-silva-as-new-manager-20180531',
            'summary': 'Everton have appointed Marco Silva as the new manager, the Premier League club announced on their website.',
            'published_parsed': time.struct_time(tm_year=2018, tm_mon=5, tm_mday=31,
                                                 tm_hour=14, tm_min=54, tm_sec=43,
                                                 tm_wday=3, tm_yday=151,
                                                 tm_isdst=0),
            'links': [
                {
                    'href': 'https://www.sport24.co.za/Soccer/EnglishPremiership/everton-appoint-marco-silva-as-new-manager-20180531',
                    'rel': 'alternate',
                    'type': 'text/html'
                },
                {
                    'href': 'https://scripts.24.co.za/img/sites/sport24.png',
                    'rel': 'enclosure',
                    'length': '1',
                    'type': 'image/png'
                }],
            'title_detail': {
                'value': 'Sport24.co.za | Everton appoint Marco Silva as new manager',
                'base': 'http://feeds.sport24.co.za/articles/Sport/Soccer/EnglishPremiership/rss',
                'language': None, 'type': 'text/plain'},
            'published': 'Thu, 31 May 2018 16:54:43 +0200',
            'summary_detail': {
                'value': 'Everton have appointed Marco Silva as the new manager, the Premier League club announced on their website.',
                'base': 'http://feeds.sport24.co.za/articles/Sport/Soccer/EnglishPremiership/rss',
                'language': None, 'type': 'text/html'},
            'title': 'Sport24.co.za | Everton appoint Marco Silva as new manager'}

        CNN = {
            'guidislink': False,
            'link': 'http://rss.cnn.com/~r/rss/edition_africa/~3/qntagNTbkUo/index.html',
            'summary_detail': {
                'value': 'Zimbabwe will hold its national elections on July 30, President Emmerson Mnangagwa announced on Wednesday.<img src="http://feeds.feedburner.com/~r/rss/edition_africa/~4/qntagNTbkUo" height="1" width="1" alt=""/>',
                'type': 'text/html', 'language': None,
                'base': 'http://rss.cnn.com/rss/edition_africa.rss'},
            'summary': 'Zimbabwe will hold its national elections on July 30, President Emmerson Mnangagwa announced on Wednesday.<img src="http://feeds.feedburner.com/~r/rss/edition_africa/~4/qntagNTbkUo" height="1" width="1" alt=""/>',
            'links': [{'type': 'text/html', 'rel': 'alternate',
                       'href': 'http://rss.cnn.com/~r/rss/edition_africa/~3/qntagNTbkUo/index.html'}],
            'published_parsed': time.struct_time(tm_year=2018, tm_mon=5, tm_mday=30,
                                                 tm_hour=14, tm_min=45, tm_sec=9,
                                                 tm_wday=2, tm_yday=150,
                                                 tm_isdst=0),
            'feedburner_origlink': 'https://www.cnn.com/2018/05/30/africa/zimbabwe-elections-july-intl/index.html',
            'title_detail': {
                'value': 'First post-Mugabe vote announced for July 30',
                'type': 'text/plain', 'language': None,
                'base': 'http://rss.cnn.com/rss/edition_africa.rss'},
            'id': 'https://www.cnn.com/2018/05/30/africa/zimbabwe-elections-july-intl/index.html',
            'title': 'First post-Mugabe vote announced for July 30',
            'media_content': [
                {'medium': 'image', 'height': '619', 'width': '1100',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-super-169.jpg'},
                {'medium': 'image', 'height': '300', 'width': '300',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-large-11.jpg'},
                {'medium': 'image', 'height': '552', 'width': '414',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-vertical-large-gallery.jpg'},
                {'medium': 'image', 'height': '480', 'width': '640',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-video-synd-2.jpg'},
                {'medium': 'image', 'height': '324', 'width': '576',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-live-video.jpg'},
                {'medium': 'image', 'height': '250', 'width': '250',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-t1-main.jpg'},
                {'medium': 'image', 'height': '360', 'width': '270',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-vertical-gallery.jpg'},
                {'medium': 'image', 'height': '169', 'width': '300',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-story-body.jpg'},
                {'medium': 'image', 'height': '250', 'width': '250',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-t1-main.jpg'},
                {'medium': 'image', 'height': '186', 'width': '248',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-assign.jpg'},
                {'medium': 'image', 'height': '144', 'width': '256',
                 'url': 'https://cdn.cnn.com/cnnnext/dam/assets/171121193242-emmerson-mnangagwa-robert-mugabe-zimbabwe-pkg-curnow-cnni-00003505-hp-video.jpg'}],
            'published': 'Wed, 30 May 2018 14:45:09 GMT'}
