import requests


def get_location(remote_addr):
    url = (
        'http://api.ipstack.com/{}?'
        'access_key=925c5d0ce1a152f4efa575d5c86f7d8c'.format(remote_addr)
    )

    return requests.get(url).json()


if __name__ == '__main__':
    print(get_location('105.112.38.26'))
