from lepl.apps.rfc3696 import Email

from application.core import errors



def validate_field_length(value, greater=None, lesser=None):
    if value is None:
        return

    if greater and len(value) < greater:
        raise errors.BadRequest(
            '{} must be more than {} characters.'.format(value, greater)
        )

    if lesser and len(value) > lesser:
        raise errors.BadRequest(
            '{} must be less than {} characters.'.format(value, lesser)
        )


def validate_email_field(email):
    validator = Email()
    if email is not None and not validator(email):
            raise errors.BadRequest('`email` {} is invalid.'.format(email))


def validate_phone_field(phone_field):
    if phone_field is not None:
        phone_num = phone_field.strip().strip('+').replace(' ', '').replace(
            '-', '')

        try:
            if not phone_num.isdigit():
                raise errors.APIError()

            if len(phone_num) == 10 and not phone_num.startswith('0'):
                phone_num = '234' + phone_num
            elif len(phone_num) == 11 and phone_num.startswith('0'):
                phone_num = '234' + phone_num[1:]
            elif phone_num.startswith('234') and len(phone_num) == 13:
                pass

            return phone_num

        except:
            raise errors.InvalidPhoneNumber

