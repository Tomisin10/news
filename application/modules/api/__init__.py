"""API blueprint"""
# third-party library imports
from flask import Blueprint

api_blueprint = Blueprint('api', __name__, url_prefix='/api/v1.0')

from application.modules.api.controllers import *
