import json

from flask.views import MethodView

from application.briefs.helpers import (
    create_brief_for_reader,
    send_brief_to_sms_reader)
from application.core import ngn_bulk_sms
from application.core.constants import (
    ACTIVE_STATUS_ID,
    DEFAULT_EVENTS_SUBSCRIBER_WELCOME_MESSAGE,
    DEFAULT_NEWS_SUBSCRIBER_WELCOME_MESSAGE)
from application.core.models import (
    DisruptivPlatform, Payment, PendingSubscription, Section, Subscription)
from application.core.constants import PENDING_STATUS_ID
from application.core.utils.contexts import current_request_data
from application.modules.api.controllers.readers import Readers


class Payments(MethodView):
    def subscribe_ussd_reader(self, section=None, news_source=None,
                              event_location=None):
        USSD_PLATFORM_ID = DisruptivPlatform.get(name='USSD').id

        reader = Readers.create_reader(
            phone=self.phone_number,
            disruptiv_platform_id=USSD_PLATFORM_ID
        )

        Subscription(
            reader_id=reader.id,
            event_location=event_location,
            section=section,
            news_source=news_source
        ).save()

        brief = create_brief_for_reader(reader)
        if brief:
            send_brief_to_sms_reader(brief, reader)

        welcome_message = (
            DEFAULT_NEWS_SUBSCRIBER_WELCOME_MESSAGE if event_location is None
            else DEFAULT_EVENTS_SUBSCRIBER_WELCOME_MESSAGE
        )

        ngn_bulk_sms.send_sms(welcome_message, self.phone_number)

    def unsubscribe_reader(self):
        return

    @staticmethod
    def create_pending_mobile_payment(username, product_name, phone,
            currency_code, amount, meta, environment, provider_channel=None):

        parameters = {
            'username': username,
            'productName': product_name,
            'phoneNumber': phone,
            'currencyCode': currency_code,
            'amount': amount,
            'metadata': meta
        }

        if provider_channel is not None:
            parameters['providerChannel'] = provider_channel

        payment = Payment(username=username, product_name=product_name,
                          phone=phone, currency_code=currency_code,
                          amount=amount, meta=json.dumps(meta),
                          provider_channel=provider_channel,
                          status_id=PENDING_STATUS_ID, environment=environment)
        payment.save()

        return payment


    def post(self):
        request_data = current_request_data()

        self.phone_number = request_data['source']
        value = request_data['value']
        status = request_data['status']

        # if status == 'Success':
        #     pending_subscription = PendingSubscription.get(
        #         phone=self.phone_number)
        #
        #     self.subscribe_reader(
        #         section=pending_subscription.section,
        #         news_source=pending_subscription.news_source)
        #
        #     pending_subscription.delete()

        # print('DESCRIPTION', request_data.get('description'))

        if (status == 'Success' and
            request_data.get('description', '').startswith(
                'Received Mobile Checkout funds from ')
        ):
            pending_subscription = PendingSubscription.get(
                phone=self.phone_number)

            if pending_subscription is None:
                return 'SUCCESS', 201

            self.subscribe_ussd_reader(
                section=pending_subscription.section,
                news_source=pending_subscription.news_source,
                event_location=pending_subscription.event_location)

            pending_subscription.delete()

            payment = Payment.get(phone=self.phone_number, _desc=True)
            payment.update(status_id=ACTIVE_STATUS_ID)

        return 'SUCCESS', 201
