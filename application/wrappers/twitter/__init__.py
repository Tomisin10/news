import requests
from datetime import datetime

from sqlalchemy.exc import InternalError

from application.wrappers.libs.twitter import Twitter, OAuth
from application.wrappers import (
    get_auth_data, create_entry_from_parsed, save_entry_if_new)
from application.core.constants import EXT_APP_POSTS_PER_USER_LIMIT

from application.core.utils import DateTimeWrapper

DEFAULT_TOKEN = '1014030603971330048-MG6voQ0DWIOVhOR1warzilk3CZ49oW'
DEFAULT_TOKEN_SECRET = 'pFGWOc9gtrGj07ZGxw6TvygtCmxyrbUS1BYrp9b8cVIgk'
CONSUMER_KEY = 'o1vnE0zQz6mGH6vGiq8hTPaGo'
CONSUMER_SECRET = 'jKYwYfZ3UulK3fP8BEgznuW5fEofD7HKrt2tURnqAcczdJNQi1'

datetimewrapper = DateTimeWrapper()

# a regular tweet
mock_post = {}

# a media tweet
mock_post_2 = {u'contributors': None, u'truncated': False,
               u'text': u'Neymar! https://t.co/hddEthYQYW',
               u'is_quote_status': False, u'in_reply_to_status_id': None,
               u'id': 986169283783938048, u'favorite_count': 0,
               u'source': u'<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>',
               u'retweeted': False, u'coordinates': None,
               u'entities': {u'symbols': [], u'user_mentions': [],
                             u'hashtags': [], u'urls': [], u'media': [{
                                                                          u'expanded_url': u'https://twitter.com/Decave_/status/986169283783938048/photo/1',
                                                                          u'display_url': u'pic.twitter.com/hddEthYQYW',
                                                                          u'url': u'https://t.co/hddEthYQYW',
                                                                          u'media_url_https': u'https://pbs.twimg.com/media/Da-TultXUAA1MsR.jpg',
                                                                          u'id_str': u'986169272023076864',
                                                                          u'sizes': {
                                                                              u'small': {
                                                                                  u'h': 680,
                                                                                  u'resize': u'fit',
                                                                                  u'w': 680},
                                                                              u'large': {
                                                                                  u'h': 928,
                                                                                  u'resize': u'fit',
                                                                                  u'w': 928},
                                                                              u'medium': {
                                                                                  u'h': 928,
                                                                                  u'resize': u'fit',
                                                                                  u'w': 928},
                                                                              u'thumb': {
                                                                                  u'h': 150,
                                                                                  u'resize': u'crop',
                                                                                  u'w': 150}},
                                                                          u'indices': [
                                                                              8,
                                                                              31],
                                                                          u'type': u'photo',
                                                                          u'id': 986169272023076864,
                                                                          u'media_url': u'http://pbs.twimg.com/media/Da-TultXUAA1MsR.jpg'}]},
               u'in_reply_to_screen_name': None, u'in_reply_to_user_id': None,
               u'retweet_count': 0, u'id_str': u'986169283783938048',
               u'favorited': False, u'user': {u'follow_request_sent': False,
                                              u'has_extended_profile': False,
                                              u'profile_use_background_image': False,
                                              u'default_profile_image': False,
                                              u'id': 566190404,
                                              u'profile_background_image_url_https': u'https://abs.twimg.com/images/themes/theme1/bg.png',
                                              u'verified': False,
                                              u'translator_type': u'none',
                                              u'profile_text_color': u'000000',
                                              u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                                              u'profile_sidebar_fill_color': u'000000',
                                              u'entities': {u'url': {u'urls': [{
                                                                                   u'url': u'https://t.co/SNe2wVbwpr',
                                                                                   u'indices': [
                                                                                       0,
                                                                                       23],
                                                                                   u'expanded_url': u'http://play.google.com/store/apps/details?id=org.codelexis.svcs.socialeapp',
                                                                                   u'display_url': u'play.google.com/store/apps/det\u2026'}]},
                                                            u'description': {
                                                                u'urls': []}},
                                              u'followers_count': 947,
                                              u'profile_sidebar_border_color': u'000000',
                                              u'id_str': u'566190404',
                                              u'profile_background_color': u'000000',
                                              u'listed_count': 30,
                                              u'is_translation_enabled': False,
                                              u'utc_offset': None,
                                              u'statuses_count': 4067,
                                              u'description': u"I hate writing apps that don't make me a million times lazier! You probably want to be lazy too, so check-out the #Trails app.\n\n#TeamSTM #Kivy #GCP",
                                              u'friends_count': 2186,
                                              u'location': u'Lagos, Nigeria.',
                                              u'profile_link_color': u'3B94D9',
                                              u'profile_image_url': u'http://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                                              u'following': True,
                                              u'geo_enabled': True,
                                              u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/566190404/1504185356',
                                              u'profile_background_image_url': u'http://abs.twimg.com/images/themes/theme1/bg.png',
                                              u'screen_name': u'Decave_',
                                              u'lang': u'en',
                                              u'profile_background_tile': False,
                                              u'favourites_count': 4201,
                                              u'name': u'Tomisin',
                                              u'notifications': False,
                                              u'url': u'https://t.co/SNe2wVbwpr',
                                              u'created_at': u'Sun Apr 29 10:38:34 +0000 2012',
                                              u'contributors_enabled': False,
                                              u'time_zone': None,
                                              u'protected': False,
                                              u'default_profile': False,
                                              u'is_translator': False},
               u'geo': None, u'in_reply_to_user_id_str': None,
               u'possibly_sensitive': False, u'lang': u'pt',
               u'created_at': u'Tue Apr 17 09:07:34 +0000 2018',
               u'in_reply_to_status_id_str': None, u'place': None,
               u'extended_entities': {u'media': [{
                                                     u'expanded_url': u'https://twitter.com/Decave_/status/986169283783938048/photo/1',
                                                     u'display_url': u'pic.twitter.com/hddEthYQYW',
                                                     u'url': u'https://t.co/hddEthYQYW',
                                                     u'media_url_https': u'https://pbs.twimg.com/media/Da-TultXUAA1MsR.jpg',
                                                     u'id_str': u'986169272023076864',
                                                     u'sizes': {
                                                         u'small': {u'h': 680,
                                                                    u'resize': u'fit',
                                                                    u'w': 680},
                                                         u'large': {u'h': 928,
                                                                    u'resize': u'fit',
                                                                    u'w': 928},
                                                         u'medium': {u'h': 928,
                                                                     u'resize': u'fit',
                                                                     u'w': 928},
                                                         u'thumb': {u'h': 150,
                                                                    u'resize': u'crop',
                                                                    u'w': 150}},
                                                     u'indices': [8, 31],
                                                     u'type': u'photo',
                                                     u'id': 986169272023076864,
                                                     u'media_url': u'http://pbs.twimg.com/media/Da-TultXUAA1MsR.jpg'}]}}

# quoted a text
mock_post_3 = {u'contributors': None, u'truncated': False,
               u'text': u"RT @omoissy: Other people's success does not mean you're failing in life.",
               u'is_quote_status': False, u'in_reply_to_status_id': None,
               u'id': 986187753011499009, u'favorite_count': 0,
               u'source': u'<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>',
               u'retweeted': False, u'coordinates': None,
               u'entities': {u'symbols': [], u'user_mentions': [
                   {u'id': 2803522953, u'indices': [3, 11],
                    u'id_str': u'2803522953', u'screen_name': u'omoissy',
                    u'name': u'Omoniyi Israel'}], u'hashtags': [], u'urls': []},
               u'in_reply_to_screen_name': None, u'in_reply_to_user_id': None,
               u'retweet_count': 41, u'id_str': u'986187753011499009',
               u'favorited': False,
               u'retweeted_status': {u'contributors': None, u'truncated': False,
                                     u'text': u"Other people's success does not mean you're failing in life.",
                                     u'is_quote_status': False,
                                     u'in_reply_to_status_id': None,
                                     u'id': 986175382033522688,
                                     u'favorite_count': 72,
                                     u'source': u'<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>',
                                     u'retweeted': False, u'coordinates': None,
                                     u'entities': {u'symbols': [],
                                                   u'user_mentions': [],
                                                   u'hashtags': [],
                                                   u'urls': []},
                                     u'in_reply_to_screen_name': None,
                                     u'in_reply_to_user_id': None,
                                     u'retweet_count': 41,
                                     u'id_str': u'986175382033522688',
                                     u'favorited': False,
                                     u'user': {u'follow_request_sent': False,
                                               u'has_extended_profile': True,
                                               u'profile_use_background_image': False,
                                               u'default_profile_image': False,
                                               u'id': 2803522953,
                                               u'profile_background_image_url_https': u'https://abs.twimg.com/images/themes/theme1/bg.png',
                                               u'verified': False,
                                               u'translator_type': u'none',
                                               u'profile_text_color': u'000000',
                                               u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/884737463381569537/RlIL-WD-_normal.jpg',
                                               u'profile_sidebar_fill_color': u'000000',
                                               u'entities': {u'url': {u'urls': [
                                                   {
                                                       u'url': u'https://t.co/W9OUWlWWv2',
                                                       u'indices': [0, 23],
                                                       u'expanded_url': u'https://www.instagram.com/issy_jnr/',
                                                       u'display_url': u'instagram.com/issy_jnr/'}]},
                                                             u'description': {
                                                                 u'urls': []}},
                                               u'followers_count': 48146,
                                               u'profile_sidebar_border_color': u'000000',
                                               u'id_str': u'2803522953',
                                               u'profile_background_color': u'000000',
                                               u'listed_count': 479,
                                               u'is_translation_enabled': False,
                                               u'utc_offset': 3600,
                                               u'statuses_count': 21046,
                                               u'description': u"A quick witted young man|| DM open for Business||  Contact; +2348142810342 Email; omoissy@gmail.com  PLS DON'T CHECK MY LIKES\U0001f62d",
                                               u'friends_count': 9758,
                                               u'location': u'searching...',
                                               u'profile_link_color': u'3B94D9',
                                               u'profile_image_url': u'http://pbs.twimg.com/profile_images/884737463381569537/RlIL-WD-_normal.jpg',
                                               u'following': False,
                                               u'geo_enabled': True,
                                               u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/2803522953/1503615925',
                                               u'profile_background_image_url': u'http://abs.twimg.com/images/themes/theme1/bg.png',
                                               u'screen_name': u'omoissy',
                                               u'lang': u'en',
                                               u'profile_background_tile': False,
                                               u'favourites_count': 774,
                                               u'name': u'Omoniyi Israel',
                                               u'notifications': False,
                                               u'url': u'https://t.co/W9OUWlWWv2',
                                               u'created_at': u'Sat Oct 04 02:23:32 +0000 2014',
                                               u'contributors_enabled': False,
                                               u'time_zone': u'West Central Africa',
                                               u'protected': False,
                                               u'default_profile': False,
                                               u'is_translator': False},
                                     u'geo': None,
                                     u'in_reply_to_user_id_str': None,
                                     u'lang': u'en',
                                     u'created_at': u'Tue Apr 17 09:31:47 +0000 2018',
                                     u'in_reply_to_status_id_str': None,
                                     u'place': None},
               u'user': {u'follow_request_sent': False,
                         u'has_extended_profile': False,
                         u'profile_use_background_image': False,
                         u'default_profile_image': False, u'id': 566190404,
                         u'profile_background_image_url_https': u'https://abs.twimg.com/images/themes/theme1/bg.png',
                         u'verified': False, u'translator_type': u'none',
                         u'profile_text_color': u'000000',
                         u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                         u'profile_sidebar_fill_color': u'000000',
                         u'entities': {u'url': {u'urls': [
                             {u'url': u'https://t.co/SNe2wVbwpr',
                              u'indices': [0, 23],
                              u'expanded_url': u'http://play.google.com/store/apps/details?id=org.codelexis.svcs.socialeapp',
                              u'display_url': u'play.google.com/store/apps/det\u2026'}]},
                                       u'description': {u'urls': []}},
                         u'followers_count': 948,
                         u'profile_sidebar_border_color': u'000000',
                         u'id_str': u'566190404',
                         u'profile_background_color': u'000000',
                         u'listed_count': 30, u'is_translation_enabled': False,
                         u'utc_offset': None, u'statuses_count': 4070,
                         u'description': u"I hate writing apps that don't make me a million times lazier! You probably want to be lazy too, so check-out the #Trails app.\n\n#TeamSTM #Kivy #GCP",
                         u'friends_count': 2186,
                         u'location': u'Lagos, Nigeria.',
                         u'profile_link_color': u'3B94D9',
                         u'profile_image_url': u'http://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                         u'following': True, u'geo_enabled': True,
                         u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/566190404/1504185356',
                         u'profile_background_image_url': u'http://abs.twimg.com/images/themes/theme1/bg.png',
                         u'screen_name': u'Decave_', u'lang': u'en',
                         u'profile_background_tile': False,
                         u'favourites_count': 4208, u'name': u'Tomisin',
                         u'notifications': False,
                         u'url': u'https://t.co/SNe2wVbwpr',
                         u'created_at': u'Sun Apr 29 10:38:34 +0000 2012',
                         u'contributors_enabled': False, u'time_zone': None,
                         u'protected': False, u'default_profile': False,
                         u'is_translator': False}, u'geo': None,
               u'in_reply_to_user_id_str': None, u'lang': u'en',
               u'created_at': u'Tue Apr 17 10:20:57 +0000 2018',
               u'in_reply_to_status_id_str': None, u'place': None}

# quoted a text + media tweet
mock_post_4 = {u'contributors': None, u'truncated': False,
               u'text': u'RT @OdumadeEmmanuel: My name is Odumade Emmanuel. This are my artworks of @wizkidayo  I did with charcoal and graphite pencil. https://t.co\u2026',
               u'is_quote_status': False, u'in_reply_to_status_id': None,
               u'id': 984810557474828288, u'favorite_count': 0,
               u'source': u'<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>',
               u'retweeted': False, u'coordinates': None,
               u'entities': {u'symbols': [], u'user_mentions': [
                   {u'id': 944750052064530432, u'indices': [3, 19],
                    u'id_str': u'944750052064530432',
                    u'screen_name': u'OdumadeEmmanuel',
                    u'name': u'emmaodumadeng'},
                   {u'id': 32660559, u'indices': [74, 84],
                    u'id_str': u'32660559', u'screen_name': u'wizkidayo',
                    u'name': u'Wizkid'}], u'hashtags': [], u'urls': []},
               u'in_reply_to_screen_name': None, u'in_reply_to_user_id': None,
               u'retweet_count': 6108, u'id_str': u'984810557474828288',
               u'favorited': False,
               u'retweeted_status': {u'contributors': None, u'truncated': False,
                                     u'text': u'My name is Odumade Emmanuel. This are my artworks of @wizkidayo  I did with charcoal and graphite pencil. https://t.co/chmQIm9zXE',
                                     u'is_quote_status': False,
                                     u'in_reply_to_status_id': None,
                                     u'id': 984038053512728576,
                                     u'favorite_count': 8164,
                                     u'source': u'<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>',
                                     u'retweeted': False, u'coordinates': None,
                                     u'entities': {u'symbols': [],
                                                   u'user_mentions': [
                                                       {u'id': 32660559,
                                                        u'indices': [53, 63],
                                                        u'id_str': u'32660559',
                                                        u'screen_name': u'wizkidayo',
                                                        u'name': u'Wizkid'}],
                                                   u'hashtags': [], u'urls': [],
                                                   u'media': [{
                                                                  u'expanded_url': u'https://twitter.com/OdumadeEmmanuel/status/984038053512728576/photo/1',
                                                                  u'display_url': u'pic.twitter.com/chmQIm9zXE',
                                                                  u'url': u'https://t.co/chmQIm9zXE',
                                                                  u'media_url_https': u'https://pbs.twimg.com/media/DagBIJnXkAAiWrM.jpg',
                                                                  u'id_str': u'984037758112075776',
                                                                  u'sizes': {
                                                                      u'small': {
                                                                          u'h': 680,
                                                                          u'resize': u'fit',
                                                                          u'w': 453},
                                                                      u'large': {
                                                                          u'h': 2048,
                                                                          u'resize': u'fit',
                                                                          u'w': 1365},
                                                                      u'medium': {
                                                                          u'h': 1200,
                                                                          u'resize': u'fit',
                                                                          u'w': 800},
                                                                      u'thumb': {
                                                                          u'h': 150,
                                                                          u'resize': u'crop',
                                                                          u'w': 150}},
                                                                  u'indices': [
                                                                      106, 129],
                                                                  u'type': u'photo',
                                                                  u'id': 984037758112075776,
                                                                  u'media_url': u'http://pbs.twimg.com/media/DagBIJnXkAAiWrM.jpg'}]},
                                     u'in_reply_to_screen_name': None,
                                     u'in_reply_to_user_id': None,
                                     u'retweet_count': 6108,
                                     u'id_str': u'984038053512728576',
                                     u'favorited': False,
                                     u'user': {u'follow_request_sent': False,
                                               u'has_extended_profile': True,
                                               u'profile_use_background_image': True,
                                               u'default_profile_image': False,
                                               u'id': 944750052064530432,
                                               u'profile_background_image_url_https': None,
                                               u'verified': False,
                                               u'translator_type': u'none',
                                               u'profile_text_color': u'333333',
                                               u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/954394178179469313/_ahnM5As_normal.jpg',
                                               u'profile_sidebar_fill_color': u'DDEEF6',
                                               u'entities': {u'description': {
                                                   u'urls': []}},
                                               u'followers_count': 480,
                                               u'profile_sidebar_border_color': u'C0DEED',
                                               u'id_str': u'944750052064530432',
                                               u'profile_background_color': u'F5F8FA',
                                               u'listed_count': 8,
                                               u'is_translation_enabled': False,
                                               u'utc_offset': None,
                                               u'statuses_count': 68,
                                               u'description': u"I'm an artist",
                                               u'friends_count': 73,
                                               u'location': u'Lagos, Nigeria',
                                               u'profile_link_color': u'1DA1F2',
                                               u'profile_image_url': u'http://pbs.twimg.com/profile_images/954394178179469313/_ahnM5As_normal.jpg',
                                               u'following': False,
                                               u'geo_enabled': False,
                                               u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/944750052064530432/1516380276',
                                               u'profile_background_image_url': None,
                                               u'screen_name': u'OdumadeEmmanuel',
                                               u'lang': u'en',
                                               u'profile_background_tile': False,
                                               u'favourites_count': 184,
                                               u'name': u'emmaodumadeng',
                                               u'notifications': False,
                                               u'url': None,
                                               u'created_at': u'Sun Dec 24 02:02:19 +0000 2017',
                                               u'contributors_enabled': False,
                                               u'time_zone': None,
                                               u'protected': False,
                                               u'default_profile': True,
                                               u'is_translator': False},
                                     u'geo': None,
                                     u'in_reply_to_user_id_str': None,
                                     u'possibly_sensitive': False,
                                     u'lang': u'en',
                                     u'created_at': u'Wed Apr 11 11:58:49 +0000 2018',
                                     u'in_reply_to_status_id_str': None,
                                     u'place': None, u'extended_entities': {
                       u'media': [{
                                      u'expanded_url': u'https://twitter.com/OdumadeEmmanuel/status/984038053512728576/photo/1',
                                      u'display_url': u'pic.twitter.com/chmQIm9zXE',
                                      u'url': u'https://t.co/chmQIm9zXE',
                                      u'media_url_https': u'https://pbs.twimg.com/media/DagBIJnXkAAiWrM.jpg',
                                      u'id_str': u'984037758112075776',
                                      u'sizes': {u'small': {u'h': 680,
                                                            u'resize': u'fit',
                                                            u'w': 453},
                                                 u'large': {u'h': 2048,
                                                            u'resize': u'fit',
                                                            u'w': 1365},
                                                 u'medium': {u'h': 1200,
                                                             u'resize': u'fit',
                                                             u'w': 800},
                                                 u'thumb': {u'h': 150,
                                                            u'resize': u'crop',
                                                            u'w': 150}},
                                      u'indices': [106, 129], u'type': u'photo',
                                      u'id': 984037758112075776,
                                      u'media_url': u'http://pbs.twimg.com/media/DagBIJnXkAAiWrM.jpg'},
                                  {
                                      u'expanded_url': u'https://twitter.com/OdumadeEmmanuel/status/984038053512728576/photo/1',
                                      u'display_url': u'pic.twitter.com/chmQIm9zXE',
                                      u'url': u'https://t.co/chmQIm9zXE',
                                      u'media_url_https': u'https://pbs.twimg.com/media/DagBK8nX0AAs8Rv.jpg',
                                      u'id_str': u'984037806162038784',
                                      u'sizes': {u'large': {u'h': 1920,
                                                            u'resize': u'fit',
                                                            u'w': 1920},
                                                 u'small': {u'h': 680,
                                                            u'resize': u'fit',
                                                            u'w': 680},
                                                 u'medium': {u'h': 1200,
                                                             u'resize': u'fit',
                                                             u'w': 1200},
                                                 u'thumb': {u'h': 150,
                                                            u'resize': u'crop',
                                                            u'w': 150}},
                                      u'indices': [106, 129], u'type': u'photo',
                                      u'id': 984037806162038784,
                                      u'media_url': u'http://pbs.twimg.com/media/DagBK8nX0AAs8Rv.jpg'},
                                  {
                                      u'expanded_url': u'https://twitter.com/OdumadeEmmanuel/status/984038053512728576/photo/1',
                                      u'display_url': u'pic.twitter.com/chmQIm9zXE',
                                      u'url': u'https://t.co/chmQIm9zXE',
                                      u'media_url_https': u'https://pbs.twimg.com/media/DagBQ6vXUAAQny-.jpg',
                                      u'id_str': u'984037908737904640',
                                      u'sizes': {u'small': {u'h': 680,
                                                            u'resize': u'fit',
                                                            u'w': 398},
                                                 u'large': {u'h': 2048,
                                                            u'resize': u'fit',
                                                            u'w': 1199},
                                                 u'medium': {u'h': 1200,
                                                             u'resize': u'fit',
                                                             u'w': 703},
                                                 u'thumb': {u'h': 150,
                                                            u'resize': u'crop',
                                                            u'w': 150}},
                                      u'indices': [106, 129], u'type': u'photo',
                                      u'id': 984037908737904640,
                                      u'media_url': u'http://pbs.twimg.com/media/DagBQ6vXUAAQny-.jpg'},
                                  {
                                      u'expanded_url': u'https://twitter.com/OdumadeEmmanuel/status/984038053512728576/photo/1',
                                      u'display_url': u'pic.twitter.com/chmQIm9zXE',
                                      u'url': u'https://t.co/chmQIm9zXE',
                                      u'media_url_https': u'https://pbs.twimg.com/media/DagBVJTX0AErMHO.jpg',
                                      u'id_str': u'984037981366505473',
                                      u'sizes': {u'large': {u'h': 2021,
                                                            u'resize': u'fit',
                                                            u'w': 1920},
                                                 u'small': {u'h': 680,
                                                            u'resize': u'fit',
                                                            u'w': 646},
                                                 u'medium': {u'h': 1200,
                                                             u'resize': u'fit',
                                                             u'w': 1140},
                                                 u'thumb': {u'h': 150,
                                                            u'resize': u'crop',
                                                            u'w': 150}},
                                      u'indices': [106, 129], u'type': u'photo',
                                      u'id': 984037981366505473,
                                      u'media_url': u'http://pbs.twimg.com/media/DagBVJTX0AErMHO.jpg'}]}},
               u'user': {u'follow_request_sent': False,
                         u'has_extended_profile': False,
                         u'profile_use_background_image': False,
                         u'default_profile_image': False, u'id': 566190404,
                         u'profile_background_image_url_https': u'https://abs.twimg.com/images/themes/theme1/bg.png',
                         u'verified': False, u'translator_type': u'none',
                         u'profile_text_color': u'000000',
                         u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                         u'profile_sidebar_fill_color': u'000000',
                         u'entities': {u'url': {u'urls': [
                             {u'url': u'https://t.co/SNe2wVbwpr',
                              u'indices': [0, 23],
                              u'expanded_url': u'http://play.google.com/store/apps/details?id=org.codelexis.svcs.socialeapp',
                              u'display_url': u'play.google.com/store/apps/det\u2026'}]},
                                       u'description': {u'urls': []}},
                         u'followers_count': 947,
                         u'profile_sidebar_border_color': u'000000',
                         u'id_str': u'566190404',
                         u'profile_background_color': u'000000',
                         u'listed_count': 30, u'is_translation_enabled': False,
                         u'utc_offset': None, u'statuses_count': 4066,
                         u'description': u"I hate writing apps that don't make me a million times lazier! You probably want to be lazy too, so check-out the #Trails app.\n\n#TeamSTM #Kivy #GCP",
                         u'friends_count': 2186,
                         u'location': u'Lagos, Nigeria.',
                         u'profile_link_color': u'3B94D9',
                         u'profile_image_url': u'http://pbs.twimg.com/profile_images/958823925894537218/FM3iiLTt_normal.jpg',
                         u'following': True, u'geo_enabled': True,
                         u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/566190404/1504185356',
                         u'profile_background_image_url': u'http://abs.twimg.com/images/themes/theme1/bg.png',
                         u'screen_name': u'Decave_', u'lang': u'en',
                         u'profile_background_tile': False,
                         u'favourites_count': 4201, u'name': u'Tomisin',
                         u'notifications': False,
                         u'url': u'https://t.co/SNe2wVbwpr',
                         u'created_at': u'Sun Apr 29 10:38:34 +0000 2012',
                         u'contributors_enabled': False, u'time_zone': None,
                         u'protected': False, u'default_profile': False,
                         u'is_translator': False}, u'geo': None,
               u'in_reply_to_user_id_str': None, u'lang': u'en',
               u'created_at': u'Fri Apr 13 15:08:28 +0000 2018',
               u'in_reply_to_status_id_str': None, u'place': None}


DEFAULT_ACCOUNTS = [
    'nowthisnews', 'nytimes', 'theeconomist', 'theverge', 'buzzfeed',
    'dailymail', 'natgeo', 'espn', 'omoissy', 'adamjosephsport', 'mefeater',
    'africafactszone'
]

ADMIN_USERNAME = 'TrailAppTeam'


interests_accounts = {
    'Politics': ['nytimes', 'theeconomist'],
    'Art': [],
    'Startups': ['500Startups'],
    'Technology': [
        'techcrunch', 'thenextweb', 'theverge', 'recode', 'ign', 
        'ProgrammingWrld'],
    'Literature': ['nybooks', 'time'],
    'Oil & Gas': [],
    'Finance': [
        'bloomberg', 'theeconomist', 'businessinsider', 'cnbc'],
    'Academics': ['newscientist', 'natgeo', ],
    'Lifestyle': ['telegraphluxury', ],
    'Health & Medicine': ['natgeo', 'newscientist'],
    'Tourism': ['buzzfeed', 'dailymail'], 'Leadership': ['cnbc', 'tedx'],
    'Fashion': ['ew', 'timesfashion', 'mashable', 'voguemagazine'],
    'Entertainment': ['mtvnews', 'buzzfeed', 'dailymail', 'mashable'],
    'Law & Order': ['thewallstreetjournal'],
    'Sports': ['bbcsport', 'espn', 'skysports'],
    'Scientific Research': ['newscientist', 'natgeo', ],
    'Gaming': ['polygon', 'recode', 'ign', 't3n']}


def create_client(username=None, access_token=None, default=False):
    if default:
        token, token_secret = DEFAULT_TOKEN, DEFAULT_TOKEN_SECRET

    elif access_token:
        token = access_token['oauth_token']
        token_secret = access_token['oauth_token_secret']

    else:
        token, token_secret = get_auth_data(
            app_name='Twitter', username=username)

    t = Twitter(auth=OAuth(token, token_secret, CONSUMER_KEY, CONSUMER_SECRET))
    return t


def create_upload_client(username=None, default=False):
    if default:
        token, token_secret = DEFAULT_TOKEN, DEFAULT_TOKEN_SECRET

    else:
        token, token_secret = get_auth_data(app_name='Twitter',
                                            username=username)
    
    t_upload = Twitter(
        domain='upload.twitter.com',
        auth=OAuth(token, token_secret, CONSUMER_KEY, CONSUMER_SECRET))

    return t_upload


def save_posts_by_user(linked_trails_account, posts):
    return


def parse_text(text):
    return text.split('https://t.co')[0]


def parse_post(post):
    tweet_time = post['created_at']
    tweet_time_splitted = tweet_time.split(' ')
    tweet_time_splitted.remove(tweet_time_splitted[4])

    year = int(tweet_time_splitted[4])
    month = (
        datetimewrapper.months_short.index(tweet_time_splitted[1]) + 1)
    day = int(tweet_time_splitted[2])

    hour = int(tweet_time_splitted[3].split(':')[0])
    min = int(tweet_time_splitted[3].split(':')[1])
    sec = int(tweet_time_splitted[3].split(':')[2])

    post_text = parse_text(post['text'])
    post_type = 'text'
    source = []

    # append quoted_status text if quoted
    if post.get('quoted_status'):

        if post['quoted_status']['entities'].get('media'):
            post_type = 'photo'
            if (post['quoted_status']['entities']['media'][0]['type'] ==
                    'video'):
                post_type = 'video'

            source = [
                media['media_url'] for media in post['quoted_status'][
                    'entities']['media']
            ]

        post_text += '"%s"' % parse_text(post['quoted_status']['text'])

    # dont bother appending text if re-tweeted.
    if post.get('retweeted_status'):
        if post['retweeted_status']['entities'].get('media'):
            post_type = 'photo'
            if (post['retweeted_status']['entities']['media'][0]['type'] ==
                    'video'):
                post_type = 'video'

            source = [
                media['media_url'] for media in post['retweeted_status'][
                    'entities']['media']
            ]

    if post['entities'].get('media'):
        post_type = 'photo'
        if post['entities']['media'][0]['type'] == 'video':
            post_type = 'video'

        source = [
            media['media_url'] for media in post['entities']['media']
        ]
        
    if len(source) == 1:
        source = source[0]
        
    content = {'text': post_text, 'type': post_type}
    if source:
        content['source'] = source

    parsed_tweet = dict(
        content=str(content),
        profile_photo_url=post['user']['profile_image_url_https'],
        author=post['user']['screen_name'].lower(),
        created_at=datetime(year, month, day, hour, min, sec),
        external_engagements=str({
            'favourite': {
                'including_user': post['favorited'],
                'total': post['favorite_count']
            },
            'retweet': {
                'including_user': post['retweeted'],
                'total': post['retweet_count']
            }
        }),
        app='twitter'
    )

    return parsed_tweet


def get_timeline(twitter_username):
    t = create_client(username=twitter_username)

    # t.status.home_timeline for user's actual timeline
    the_statuses = t.statuses.home_timeline(
        screen_name=twitter_username
    )

    parsed_statuses = []

    for status in the_statuses:
        try:
            parsed_post = parse_post(status)

            entry = create_entry_from_parsed('twitter', parsed_post)
            save_entry_if_new(entry)

            # parsed_post['created_at'] = str(parsed_post['created_at'])
            parsed_statuses.append(entry.header)

        except InternalError:
            continue

    return parsed_statuses


def get_user_posts(twitter_username):
    t = create_client(twitter_username, default=True)

    # t.status.home_timeline for user's actual timeline
    the_statuses = t.statuses.user_timeline(
        screen_name=twitter_username
    )#[:EXT_APP_POSTS_PER_USER_LIMIT]

    parsed_statuses = []

    for status in the_statuses:
        parsed_post = parse_post(status)
        parsed_statuses.append(parsed_post)

        entry = create_entry_from_parsed('twitter', parsed_post)
        save_entry_if_new(entry)

    return parsed_statuses


def update_status(title, body, img=None, source_url=None, tags=None):
    t = create_client(default=True)

    message = title

    if tags:
        for tag in tags:
            if tag:
                message += ' #{}'.format(tag)

    if source_url:
        message += ' {}'.format(source_url)

    if img:
        try:
            url_ = img
            imagedata = requests.get(url_).content

            t_upload = create_upload_client(default=True)
            id_img1 = t_upload.media.upload(media=imagedata)["media_id_string"]

            try:
                return t.statuses.update(
                    status=message, media_ids=",".join([id_img1]))
            except:
                return 'Error in Twitter.updateStatus'

        except ImportError:
            print('Error in Twitter.updateStatus')

    else:
        try:
            return t.statuses.update(status=message)
        except:
            return 'Error in Twitter.updateStatus'


def follow_user(username):
    'we should follow users on their sign-in'


def send_dm(message, recipient):
    'do send dm'


def get_avatar(ext_app_trails_user_username, username, save_as):
    t = create_client(ext_app_trails_user_username)

    the_statuses = t.statuses.user_timeline(screen_name=username)

    full_url = the_statuses[0]['user']['profile_image_url_https']
    the_page = requests.get(full_url).content

    a = open(save_as, 'wb')
    a.write(the_page)


def get_trends():
    # TODO not implemented twitter get_trends
    raise NotImplementedError
