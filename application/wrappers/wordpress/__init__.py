from datetime import datetime
import json
import logging
import requests


NIGERIAN_FASHION_LINK = (
    'https://www.bellanaija.com/wp-json/wp/v2/posts?per_page=30',)
NIGERIAN_ENTERTAINMENT_LINK = (
    'http://www.tooxclusive.com/wp-json/wp/v2/posts?per_page=30',
    'http://www.notjustok.com/wp-json/wp/v2/posts?per_page=30')
TECHNOLOGY_LINK = (
    'https://www.techcrunch.com/wp-json/wp/v2/posts?per_page=30',
    'https://www.techpoint.africa/wp-json/wp/v2/posts?per_page=30')


def _get_datetime_from_timestamp(timestamp):
    date, time = timestamp.split('T')
    int_date_units = [int(date_unit) for date_unit in date.split('-')]
    int_time_units = [int(time_unit) for time_unit in time.split(':')]

    return datetime(*int_date_units, *int_time_units)


def _get_domain_name(url):
    return url.split('://')[1].split('/')[0]


def _get_media_blob_link(domain_name, media_id):
    media_json = requests.get(
        "http://{}/wp-json/wp/v2/media/{}".format(
            domain_name, media_id)
    ).json()

    data = media_json.get('data')
    if data is not None and data['status'] == 404:
        raise ValueError

    media_type = media_json['media_type']

    fields_media_type = {
        'image': 'image_url',
        'video': 'video_url'
    }

    return (fields_media_type[media_type], media_json['source_url'])


def save_post(json, news_source_id, section_id):
    from app import application
    from application.articles.helpers import create_new_article

    title = json['title']['rendered']
    body = json['content']['rendered']
    link = json['link']
    date_created = _get_datetime_from_timestamp(json['date_gmt'])

    tags = []

    media = {'image_url': str(), 'video_url': str()}

    domain_name = _get_domain_name(link)

    try:
        media_type, media_link = (
            _get_media_blob_link(domain_name, json['featured_media'])
        )

        media[media_type] = media_link
    except (KeyError, ValueError):
        print('ERROR GETTING MEDIA URL: %s' % json['featured_media'])

    # with application.app_context():
    return create_new_article(
        title, body, link, tags=tags, news_source_id=news_source_id,
        section_id=section_id, created_at=date_created, **media)


def fetch_from_url(url, news_source_id, section_id):
    url = url.format(per_page=10)

    try:
        response = requests.get(url).json()
    except json.decoder.JSONDecodeError:
        return

    return response

    for post in response:
        # print(post, type(post))
        save_post(post, news_source_id, section_id)

        # if saved_post is not None:
        #     break  # so only websites' articles are scraped evenly
