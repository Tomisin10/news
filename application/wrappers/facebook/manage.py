import requests

from application.core.constants import LANDING_MESSAGE


# ACCESS_TOKEN = os.environ['ACCESS_TOKEN']
ACCESS_TOKEN = 'EAAJUedSagbsBABK7pU94lpizszxiLqwy3Yvbq6TNful3SXx8qawhVCFbug8XGRZB5MLo1r0pIQMxaujfwDB51Ev7FyzDRJ2201FPjrvKSBFtAcZCbrWR9mvaY1U1cweVteqNr75VEF55eZAcpvZA9bm0j9ZCi3kZAhl3kbLtz1rAZDZD'

# VERIFY_TOKEN = os.environ['VERIFY_TOKEN']
VERIFY_TOKEN = 'some_verify_token'

MESSENGER_PROFILE_URL = (
    'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=%s' %
    ACCESS_TOKEN)


side_menu_payload = {
    "persistent_menu": [
        {
            "locale": "default",
            "composer_input_disabled": False,
            "call_to_actions": [
                {
                    "title": "Channels",
                    "type": "postback",
                    "payload": "DISPLAY_ALL_NEWS_SOURCES"
                },
                {
                    "title": "Sections",
                    "type": "postback",
                    "payload": "DISPLAY_ALL_SECTIONS"
                },
                {
                    "title": "Briefs",
                    "type":"nested",
                    "call_to_actions":[
                        {
                            "title":"Edit",
                            "type":"postback",
                            "payload":"DISPLAY_BRIEF_SOURCES"
                        }
                    ]
                },
            ]
        }
    ]
}
def add_side_menu():

    return requests.post(MESSENGER_PROFILE_URL, json=side_menu_payload).content


def add_get_started():
    payload = {
        "get_started": {
            "payload": "GET_STARTED_PAYLOAD"
        }
    }

    return requests.post(MESSENGER_PROFILE_URL, json=payload).content


def add_greeting():
    payload = {
        "greeting": [
            {
                "locale": "default",
                "text": LANDING_MESSAGE
            }, {
                "locale": "en_US",
                "text": LANDING_MESSAGE
            }
        ]
    }

    return requests.post(MESSENGER_PROFILE_URL, json=payload).content


if __name__ == '__main__':
    print(add_side_menu())
    # print(add_get_started())
    # print(add_greeting())
