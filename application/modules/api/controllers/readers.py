from flask.views import MethodView

from application.core.constants import MIN_NAME_LENGTH
from application.core.utils.validators import (
    validate_email_field, validate_field_length, validate_phone_field)
from application.core import errors
from application.core.models import DisruptivPlatform, Reader
from application.core.utils.contexts import current_request_data
from application.core.utils.request_response_helpers import (
    api_created_response,
    api_deleted_response,
    api_success_response)


class Readers(MethodView):
    @staticmethod
    def check_reader_fields(**params):
        first_name = params.get('first_name')
        last_name = params.get('last_name')
        email = params.get('email')
        phone = params.get('phone')

        if first_name:
            validate_field_length(first_name, greater=MIN_NAME_LENGTH)
        if last_name:
            validate_field_length(last_name, greater=MIN_NAME_LENGTH)
        if email:
            validate_email_field(email)
        if phone:
            validate_phone_field(phone)

        disruptiv_platform_id = params.get('disruptiv_platform_id')
        if DisruptivPlatform.get(id=disruptiv_platform_id) is None:
            raise errors.ResourceNotFound(
                '`DisruptivPlatform` does not exist')

    @staticmethod
    def validate_reader_creation_fields(**params):
        required_params = {'disruptiv_platform_id'}
        missing_params = required_params - set(params)

        if bool(params.get('phone')) == bool(params.get('email')) == False:
            raise errors.BadRequest('Include one of `phone` or `email`.')

        if missing_params:
            raise errors.BadRequest('Send all of {}'.format(missing_params))

        Readers.check_reader_fields(**params)

    @staticmethod
    def validate_reader_update_fields(**params):
        Readers.check_reader_fields(**params)

    @classmethod
    def create_reader(cls, **request_data):
        reader = Reader(
            first_name=request_data.get('first_name'),
            last_name=request_data.get('last_name'),
            email=request_data.get('email'),
            phone=request_data.get('phone'),
            disruptiv_platform_id=request_data['disruptiv_platform_id']
        )

        disruptiv_platform_id = request_data['disruptiv_platform_id']
        if DisruptivPlatform.get(id=disruptiv_platform_id).name == 'USSD':
            pass

            # ngn_bulk_sms.send_sms(
            #     'WELCOME TO DISRUPTIVITY! Dial {payment_ussd_shortcode} '
            #     'to pay the N{subscription_fee}/month fee and let\'s get the '
            #     'News rolling in!',
            #     request_data.get('phone')
            # )

        reader.save()

        return reader

    @classmethod
    def update_reader(cls, reader, **request_data):
        reader.first_name = request_data.get('first_name', reader.first_name)
        reader.last_name = request_data.get('last_name', reader.last_name)
        reader.phone = request_data.get('phone', reader.phone)
        reader.email = request_data.get('email', reader.email)
        reader.disruptiv_platform_id = request_data.get(
            'disruptiv_platform_id', reader.disruptiv_platform_id)

        reader.save()


    def post(self):
        request_data = current_request_data()

        self.validate_reader_creation_fields(**request_data)

        reader = self.create_reader(**request_data)

        return api_created_response(reader.as_json())

    def get(self, reader_uid):
        reader = Reader.get(uid=reader_uid)
        if reader is None:
            raise errors.ResourceNotFound

        return api_success_response(reader.as_json())

    def patch(self, reader_uid):
        request_data = current_request_data()

        self.validate_reader_update_fields(**request_data)

        reader = Reader.get(uid=reader_uid)
        if reader is None:
            raise errors.ResourceNotFound

        self.update_reader(reader, **request_data)

        return api_success_response(reader.as_json())

    def delete(self, reader_uid):
        reader = Reader.get(uid=reader_uid)
        if reader is None:
            raise errors.ResourceNotFound

        reader.delete()

        return api_deleted_response()
