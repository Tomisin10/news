def save_social_media_account(
        app_name, username, meta, client_token, client_token_secret,
        oauth_token, oauth_token_secret=None):
    from application.core.models import SocialMediaAccount

    social_media = SocialMediaAccount(
        app_name=app_name, username=username,
        client_token=client_token, client_token_secret=client_token_secret,
        oauth_token=oauth_token, oauth_token_secret=oauth_token_secret,
        meta=str(meta)
    )

    social_media.save()

    return social_media


def get_auth_data(app_name, username):
    from application.core.models import SocialMediaAccount

    account = SocialMediaAccount.prepare_query_for_active(
        app_name=app_name, username=username).first()

    if account:
        return account.oauth_token, account.oauth_token_secret


def create_entry_from_parsed():
    pass


def save_entry_if_new():
    pass
