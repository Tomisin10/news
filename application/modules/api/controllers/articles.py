from flask.views import MethodView

from application.core import errors
from application.core.models import Article
from application.core.models.helpers import (
    orm_get_disruptiv_platform_by_name,
    orm_get_author_by_name)
from application.articles.helpers import (
    create_new_article,
    get_general_headlines)
from application.core.utils.contexts import current_request_data
from application.core.utils.request_response_helpers import api_success_response


class Articles(MethodView):
    # @api_blueprint.route('/articles/<int:article_id>', methods=['GET'])
    def get(self, article_uid=None):
        if article_uid is None:
            articles = [
                article.as_json() for article in get_general_headlines()
            ]

            return api_success_response(articles)

        article = Article.get(uid=article_uid)
        if article is None:
            raise errors.ResourceNotFound('Article not found')

        return api_success_response(article.as_json())

    # @api_blueprint.route('/articles', methods=['POST'])
    def post(self):
        request_data = current_request_data()

        platform = request_data['platform']
        author = request_data['author']
        title = request_data['title']
        body = request_data['body']
        blobs = request_data['blobs']

        platform_orm = orm_get_disruptiv_platform_by_name(platform)
        author_orm = orm_get_author_by_name(author)

        # create_new_article(
        #     title, body, link, news_source_id, section_id, platform_id=None,
        #     author_id=None, share_to=[], tags=[], created_at=datetime.now,
        #     image_url=None, video_url=None)
        create_new_article(
            title=title, body=body, platform_id=platform_orm.id,
            author_id=author_orm.id
        )

        return True

    # @api_blueprint.route('/articles/article_uid', methods=['PATCH'])
    def patch(self, article_uid):
        # TODO implement edit article
        raise NotImplementedError
