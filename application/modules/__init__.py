from .api import api_blueprint
from .bots import bots_blueprint
from .documentation import documentation_blueprint
from .oauth import oauth_blueprint
from .ussd import ussd_blueprint
from .web import web_blueprint
