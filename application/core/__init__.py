from random import SystemRandom

from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy

from application.core.logs import logger, setup_logging
from application.wrappers.ng_bulk_sms.contrib.flask import NGBulkSMS


rand_gen = SystemRandom()
db = SQLAlchemy()
mailer = Mail()
ngn_bulk_sms = NGBulkSMS()


def setup_core_tools(app):
    from application.core.errors.handlers import setup_error_handling

    setup_logging(app)

    setup_error_handling(app)

    db.init_app(app)
    mailer.init_app(app)
    ngn_bulk_sms.init_app(app)
