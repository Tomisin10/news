import random
from datetime import datetime

from sqlalchemy.exc import DataError

from application.core import errors
from application.core import db
from application.core.constants import ARTICLE_FETCH_CAP
from application.core.models import Article, NewsSource, Section, Tag
from application.wrappers import twitter
from tasks import task_push_to_social_media


def create_new_article(
        title, body, link=None, news_source_id=None, section_id=None,
        tags=[], created_at=datetime.now, image_url=None, video_url=None):

    section_id = section_id
    news_source_id = news_source_id

    try:
        article = Article(
            article_title=title, article_body=body, article_image_url=image_url,
            article_url=link, article_video_url=video_url,
            created_at=created_at, section_id=section_id,
            news_source_id=news_source_id
        )

        article.save()

        print('Successfully saved: %s' % title)

    except:
        print('Error saving article: %s... Copy already exists in DB!.' % title)
        return

    for tag in tags:
        Tag(name=tag, article_id=article.id).save()

    try:
        task_push_to_social_media.delay(title, body, image_url, tags)
    except AttributeError:
        pass
        # task_push_to_social_media(title, body, image_url, tags)

    return article


# TODO make this paginatable
def get_compilation(page=None, per_page=None):
    sections = Section.prepare_query_for_active().all()

    random.shuffle(sections)

    compilation = []

    for section in sections:
        latest_section_article = Article.prepare_query_for_active(
            section_id=section.id
        ).order_by(
            Article.id.desc()
        ).first()

        if latest_section_article is not None:
            compilation.append(
                latest_section_article.as_json()
            )

    return compilation


def get_existing_section_headlines(section_orm, page=1, per_page=9):
    articles = Article.query.filter_by(
        section_id=section_orm.id
    ).order_by(
        Article.id.desc()
    ).paginate(
        page=page, per_page=ARTICLE_FETCH_CAP
    ).items

    random.shuffle(articles)

    return [article.as_json() for article in articles[:per_page]]


def get_existing_news_source_headlines(news_source_orm, page=1, per_page=9):
    articles = Article.query.filter_by(
        news_source_id=news_source_orm.id
    ).order_by(
        Article.id.desc()
    ).paginate(
        page=page, per_page=min(per_page, ARTICLE_FETCH_CAP)
    ).items

    # random.shuffle(articles)

    return [article.as_json() for article in articles]


def get_existing_tag_headlines(tag):
    return


def get_existing_author_headlines(author, page, per_page):
    return


def get_existing_trends():
    return twitter.get_trends()


def append_similar_posts(article):
    similar_posts = get_existing_news_source_headlines(
        news_source_orm=Article.get(id=article['id']).news_source, per_page=20)

    random.shuffle(similar_posts)

    article['similar_posts'] = similar_posts[:3]

    return article


def get_world_headlines(page=1, per_page=10, _desc=True):
    """Return the World News headlines"""

    articles = Article.query.join(
        Section
    ).filter(
        Section.name == 'WORLD'
    )

    if _desc:
        articles = articles.order_by(
            Article.id.desc()
        )

    pagination = articles.paginate(page=page, per_page=per_page)

    article_items = [article.as_json() for article in pagination.items]

    article_items = list(map(append_similar_posts, article_items))

    return article_items


def get_general_headlines():
    latest_articles = [
        section._latest_article
        for section in Section.prepare_query_for_active().all()
    ]

    return latest_articles
