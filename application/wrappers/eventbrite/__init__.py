import calendar

from dateutil import parser as date_parser
from flask import current_app
import requests


API_BASE_URL = (
    'https://www.eventbriteapi.com/v3')
USER_SCOPE_BASE_URL = (
    'https://www.eventbriteapi.com/v3/users/me/?token={access_token}')


def get_venue_details(venue_id, oauth_token=None):
    oauth_token = oauth_token or current_app.config['EVENTBRITE_OAUTH_TOKEN']

    response = requests.get(
        API_BASE_URL + '/venues/{}'.format(venue_id),
        headers={
            'authorization': 'Bearer {}'.format(oauth_token)
        }).json()

    return response


def get_events_for_location(address=None, latitude=None, longitude=None,
        oauth_token=None):

    oauth_token = oauth_token or current_app.config['EVENTBRITE_OAUTH_TOKEN']

    response = requests.get(
        API_BASE_URL + '/events/search',
        headers={
            'authorization': 'Bearer {}'.format(oauth_token)
        },
        params={
            'location.address': address,
            'location.latitude': latitude,
            'location.longitude': longitude
        }).json()

    events = response['events']

    for event in events:
        start_date = date_parser.parse(event['start']['local'])
        end_date = date_parser.parse(event['end']['local'])

        event['start_date'] = {
            'day': start_date.day,
            'month': calendar.month_name[start_date.month],
            'year': start_date.year
        }

        event['end_date'] = {
            'day': end_date.day,
            'month': calendar.month_name[end_date.month],
            'year': end_date.year
        }

    return events


if __name__ == '__main__':
    # print(
    #     get_events_for_location(
    #         address='Benin, Nigeria',
    #         oauth_token='DOYGDUJFP74EVYSCO573 '
    #     )[0]
    # )

    print(
        get_venue_details(
            venue_id='27741146',
            oauth_token='DOYGDUJFP74EVYSCO573'),
    )
