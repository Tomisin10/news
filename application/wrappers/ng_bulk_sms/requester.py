import requests

from .exceptions import NGBulkSMSGatewayError


GET_METHOD = 'GET'


class APIRequester(object):

    _DEFAULT_REQ_TIMEOUT = 30.0
    session = requests.sessions.Session()

    def __init__(self, base_url, username, password, timeout=None):
        self.base_url = base_url
        self.username = username
        self.password = password

        self.timeout = self._DEFAULT_REQ_TIMEOUT if timeout is None else timeout

    def call_api(self, payload):
        payload.update(username=self.username,
                       password=self.password)
        params = {
            'method': GET_METHOD,
            'url': self.base_url,
            'params': payload,
            'timeout': self.timeout
        }

        try:
            return self.session.request(**params)
        except requests.RequestException:
            raise NGBulkSMSGatewayError
