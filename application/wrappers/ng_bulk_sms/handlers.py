from .exceptions import (
    NGBulkSMSGatewayError,
    NGBulkSMSAPIError,
    NGBulkSMSUnhandledError)


def _handle_known_error(data):
    # TODO: Fully handle API errors

    raise NGBulkSMSAPIError(api_msg=data['error'],
                            api_code=data['errno'])


def _handle_unknown_error(http_code):
    # TODO: Notify developer!

    raise NGBulkSMSUnhandledError(http_code)


def handle_response(response):
    http_code = response.status_code

    try:
        data = response.json()
    except ValueError:
        raise NGBulkSMSGatewayError('Could not parse gateway response.')

    if http_code != 200:
        return _handle_unknown_error(http_code)

    try:
        if 'status' not in data:
            return _handle_known_error(data)

        if data['status'] != 'OK':
            raise NGBulkSMSGatewayError

        return data
    except:
        # TODO: Temp code
        from application.core import logger
        # from tacore.core import logger
        logger.critical('SMS ERROR! Response: %s, %s', response.text, http_code,
                        exc_info=True)
        raise

# try:
#     api_status = data['status']
# except (KeyError):
#     raise GatewayError
#
#
# if http_code == 400:
#     if api_code == 99:
#         raise LoggedPayment(data)
#
#     if api_code == 101:
#         raise CustomerNotFound(data)
#
#     if api_code == 106:
#         raise PaymentNotFound(data)
#
#     if api_code == 107:
#         raise DuplicatePayref(data)
#
#     if api_code == 108:
#         raise PendingPayment(data)

# raise GatewayError(data)
