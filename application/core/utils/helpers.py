import os

import shortuuid


def detect_configuration_mode():
    return os.environ['RUNNING_MODE'] or 'development'


def generate_unique_reference():
    return shortuuid.uuid()
