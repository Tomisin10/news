import calendar

from dateutil import parser as date_parser


def setup_template_filters(app):

    @app.template_filter('capitalise')
    def capitalise_string(s):
        return s.upper()

    @app.template_filter('titleize')
    def titleize_string(s):
        return s.title()

    @app.template_filter('format_date')
    def format_date(iso_date):
        date_ = date_parser.parse(iso_date)

        return '{day_no} {month_name} {year_no} {hour}:{min}'.format(
            day_no=date_.day, month_name=calendar.month_name[date_.month],
            year_no=date_.year, hour=date_.hour, min=(
                date_.minute if len(str(date_.minute)) == 2
                else '0{}'.format(date_.minute)
            )
        )

    @app.context_processor
    def utility_processor():
        def find_next_article(headlines, article):
            article_index = headlines.index(article)

            try:
                return headlines[article_index + 1]
            except IndexError:
                return None

        return dict(find_next_article=find_next_article)

    @app.context_processor
    def calculate_length():
        def calculate_length(obj):
            return len(obj)

        return dict(calculate_length=calculate_length)

    @app.context_processor
    def check_if_active_page():
        def check_if_active_page(active_page, some_nav_item):
            if active_page == some_nav_item:
                return 'active'

            return 'inactive'

        return dict(check_if_active_page=check_if_active_page)
