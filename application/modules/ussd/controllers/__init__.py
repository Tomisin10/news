from flask import request
from flask.views import MethodView

from .. import ussd_blueprint
from application.briefs.helpers import (
    create_brief_for_reader,
    send_brief_to_sms_reader)
from application.core.constants import APP_NAME, SUBSCRIPTION_FEE
from application.core.models import (
    DisruptivPlatform, EventLocation, NewsSource, PendingSubscription, Section,
    Subscription, USSDConversation)
from application.core.utils.contexts import current_request_headers
from application.modules.api.controllers.payments import Payments
from application.modules.api.controllers.readers import Readers


class USSD(MethodView):
    def subscribe_user(self, phone_number, section=None, news_source=None,
            event_location=None):
        USSD_PLATFORM_ID = DisruptivPlatform.get(name='USSD').id

        reader = Readers.create_reader(
            phone=phone_number,
            disruptiv_platform_id=USSD_PLATFORM_ID
        )

        Subscription(
            reader_id=reader.id,
            section=section,
            news_source=news_source,
            event_location=event_location
        ).save()

        brief = create_brief_for_reader(reader)
        send_brief_to_sms_reader(brief, reader)

    @property
    def events_menu(self):
        count = 0
        events_menu = ''

        for event_location in EventLocation.query.limit(9).all():
            count += 1
            events_menu += '{}. {}\n'.format(count, event_location.name.title())

        return events_menu

    @property
    def news_menu(self):
        return '1. Sections\n2. Channels'

    @property
    def root_menu(self):
        return '1. Events\n2. News'

    @property
    def sections_menu(self):
        count = 0
        sections_menu = ''

        for section in Section.query.limit(9).all():
            count += 1
            sections_menu += '{}. {}\n'.format(count, section.name.title())

        return sections_menu

    @property
    def sources_menu(self):
        count = 0
        sources_menu = ''

        for source in NewsSource.query.limit(9).all():
            count += 1
            sources_menu += '{}. {}\n'.format(count, source.title.title())

        return sources_menu

    def save_response(self, response):
        USSDConversation(
            phone=self.phone_number,
            request=self.text,
            response=response,
            session_id=self.session_id
        ).save()

    def post(self):
        request_headers = current_request_headers()
        request_data = request.form

        self.service_code = request_data['serviceCode']
        self.session_id = request_data['sessionId'][:32]
        self.phone_number = request_data['phoneNumber']
        self.text = request_data['text']

        if not self.text:
            options = self.root_menu

            USSDConversation(
                phone=self.phone_number,
                request=self.text,
                response=options,
                session_id=self.session_id
            ).save()

            return 'CON Welcome to {}.\n\n{}'.format(APP_NAME, self.root_menu)

        else:
            previous_monologue = USSDConversation.get(
                session_id=self.session_id,
                _desc=True
            )

            if self.root_menu in previous_monologue.response:
                try:
                    key = int(self.text)
                except:
                    self.save_response(self.root_menu)

                    return 'CON Welcome to {}.\n\n{}'.format(
                        APP_NAME, self.root_menu)

                sub_menus = [
                    lambda: self.events_menu,
                    lambda: self.news_menu
                ]

                response = sub_menus[key-1]()

                self.save_response(response)

                return 'CON {}'.format(response)

            elif self.events_menu in previous_monologue.response:
                try:
                    key = int(self.text.split('*')[-1])
                except:
                    self.save_response(self.events_menu)
                    return 'CON {}'.format(self.events_menu)

                Payments.create_pending_mobile_payment(
                    username='sandbox',
                    product_name='My Online Store',
                    phone=self.phone_number,
                    currency_code='NGN',
                    amount=SUBSCRIPTION_FEE,
                    meta={}, environment='sandbox'
                )

                event_location = EventLocation.get(id=key)
                # self.subscribe_user(self.phone_number, section=section)
                PendingSubscription(
                    event_location=event_location,
                    phone=self.phone_number).save()

                self.save_response('END')

                return 'END This service will cost you N ABC/xyz.', 200

            elif self.news_menu in previous_monologue.response:
                try:
                    key = int(self.text.split('*')[-1])
                except:
                    self.save_response(self.root_menu)

                    return 'CON Welcome to {}.\n\n{}'.format(
                        APP_NAME, self.root_menu)

                sub_menus = [
                    lambda: self.sections_menu,
                    lambda: self.sources_menu
                ]

                response = sub_menus[key-1]()

                self.save_response(response)

                return 'CON {}'.format(response)

            elif self.sections_menu in previous_monologue.response:
                try:
                    key = int(self.text.split('*')[-1])
                except:
                    self.save_response(self.sections_menu)
                    return 'CON {}'.format(self.sections_menu)

                Payments.create_pending_mobile_payment(
                    username='sandbox',
                    product_name='My Online Store',
                    phone=self.phone_number,
                    currency_code='NGN',
                    amount=SUBSCRIPTION_FEE,
                    meta={}, environment='sandbox'
                )

                section = Section.get(id=key)
                # self.subscribe_user(self.phone_number, section=section)
                PendingSubscription(
                    section=section,
                    phone=self.phone_number).save()

                self.save_response('END')

                return 'END This service will cost you N ABC/xyz.', 200

            elif self.sources_menu in previous_monologue.response:
                try:
                    key = int(self.text.split('*')[-1])
                except:
                    self.save_response(self.sources_menu)
                    return 'CON {}'.format(self.sources_menu)

                Payments.create_pending_mobile_payment(
                    username='sandbox',
                    product_name='My Online Store',
                    phone=self.phone_number,
                    currency_code='NGN',
                    amount=SUBSCRIPTION_FEE,
                    meta={}, environment='sandbox'
                )

                news_source = NewsSource.get(id=key)
                # self.subscribe_user(self.phone_number, news_source=source)
                PendingSubscription(
                    news_source=news_source,
                    phone=self.phone_number).save()

                self.save_response('END')

                return 'END This service will cost you N ABC/xyz.', 200

                # return 'END You have subscribed for {}.'.format(
                #     source.title.title())

        return 'CON Done!'


mappings = [
    ('', USSD, 'ussd')
]


for url in mappings:
    path, view, name = url

    ussd_blueprint.add_url_rule(path, view_func=view.as_view(name))
