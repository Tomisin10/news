"""Front-end of Disruptiv, where the WordPress theme will be served."""
from flask import Blueprint

web_blueprint = Blueprint('web', __name__, url_prefix='/')

from application.modules.web.controllers import *
