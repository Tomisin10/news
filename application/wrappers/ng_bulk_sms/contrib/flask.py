from .. import NGBulkSMSClient
from ..exceptions import *


class NGBulkSMS(NGBulkSMSClient):

    def __init__(self, app=None):
        self.app = app
        self.sms_enabled = False

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        client_params = {
            'base_url': app.config['NGBULKSMS_BASE_URL'],
            'username': app.config['NGBULKSMS_USERNAME'],
            'password': app.config['NGBULKSMS_PASSWORD'],
            'default_sender': app.config['NGBULKSMS_SENDER'],
            'timeout': app.config.get('NGBULKSMS_TIMEOUT')
        }

        self.sms_enabled = app.config['SMS_ENABLED']
        super(NGBulkSMS, self).__init__(**client_params)

    def send_sms(self, message, recipients_list, sender=None):
        if self.sms_enabled:
            return super(NGBulkSMS, self).send_sms(
                message, recipients_list, sender)

        print('SMS SENDING DISABLED!!!')
