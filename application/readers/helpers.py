from flask import g, request

from application import cache
from application.core.constants import CACHE_LIFESPAN
from application.core.models import (
    Subscription, Conversation, DisruptivPlatform, Reader)
from application.wrappers.eventbrite import get_events_for_location
from application.wrappers.geoip import get_location
from application.wrappers.openweathermap import get_weather


def create_new_reader(disruptiv_platform, uid=None, first_name=None,
        last_name=None, email=None, phone=None, meta=None):

    reader = Reader(
        first_name=first_name, last_name=last_name, email=email,
        phone=phone, meta=meta, disruptiv_platform_id=disruptiv_platform.id
    )

    if uid is not None:
        reader.uid = uid

    disruptiv_platform = DisruptivPlatform.query.get(disruptiv_platform.id)
    if disruptiv_platform.disruptiv_platform_category.name == 'Bot':
        Conversation(reader_id=reader.id).save()

    g.reader = reader

    reader.save()

    return reader


def add_source_to_reader_briefs(news_source):
    subscription = Subscription(
        news_source_id=news_source.id,
        reader_id=g.reader.id)
    subscription.save()

    return subscription


def add_section_to_reader_briefs(section):
    subscription = Subscription(
        section_id=section.id,
        reader_id=g.reader.id)
    subscription.save()

    return subscription


def drop_subscription(subscription):
    subscription.delete()


def get_reader_subscriptions():
    return Subscription.prepare_query_for_active().filter_by(reader_id=g.reader.id).all()


def get_current_reader_location():
    cached_location = cache.get('{}-location'.format(request.remote_addr))

    if cached_location is None:
        location = get_location(request.remote_addr)
        cache.set(
            '{}-location'.format(request.remote_addr), location, CACHE_LIFESPAN)

        return location

    return cached_location


def get_current_reader_weather():
    reader_location = get_current_reader_location()

    cached_weather = cache.get('{}-weather'.format(reader_location))

    if cached_weather is None:
        weather = get_weather(
            city=reader_location['city'],
            country_shortform=reader_location['country_code'])

        cache.set(
            '{}-weather'.format(reader_location), weather,
            CACHE_LIFESPAN)

        return weather

    return cached_weather


def get_current_reader_events():
    reader_location = get_current_reader_location()

    cached_events = cache.get('{}-events'.format(
        reader_location['city'], reader_location['country_name']))
    if cached_events is None:

        events = get_events_for_location(
            address='{}, {}'.format(
                reader_location['city'], reader_location['country_name'])
        )

        cache.set(
            '{}-events'.format(
                reader_location['city'], reader_location['country_name']),
            events, CACHE_LIFESPAN)

        return events

    return cached_events
