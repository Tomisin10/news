from .handlers import handle_response
from .requester import APIRequester


class NGBulkSMSClient(object):

    def __init__(self, base_url, username, password, default_sender,
                 timeout=None):
        self.check_sender_value(default_sender)
        self.sender = default_sender

        self.api_requester = APIRequester(base_url=base_url,
                                          username=username,
                                          password=password,
                                          timeout=timeout)

    @staticmethod
    def check_sender_value(sender):
        if len(sender) > 11:
            raise ValueError('Value for sender cannot be longer than 11')

    def call_api(self, *args, **kwargs):
        return handle_response(self.api_requester.call_api(*args, **kwargs))

    def send_sms(self, message, recipients_list, sender=None):
        sender = sender or self.sender
        self.check_sender_value(sender)

        recipients = recipients_list
        if isinstance(recipients_list, list):
            recipients = ','.join(recipients_list)

        payload = {
            'message': message,
            'sender': sender,
            'mobiles': recipients
        }
        return self.call_api(payload=payload)


# Text Message
# --
#
# http://portal.nigerianbulksms.com/api/?username=user&password=pass&message=
# test&sender=welcome&mobiles=2348030000000,2348020000000
#
#
# Example Responses:
#
# Success Response: {"status":"OK","count":1,"price":2}
#
# Failed Response:  {"error":"Login denied.","errno":"103"}
#
#
# Other optional Parameter
#
# 1. contacts = The list of contact ids to send the message
#
# 2. groups   = The list of group ids to send the message
#
# 3. numbers  = The list of number ids to send the message
#
# 4. type     = The type (text, call, tts ) of the message
#
# 5. message  = message text or audio reference number
#
#
#
# Call Message
# --
#
# http://portal.nigerianbulksms.com/api/?username=user&password=pass&message=
# 391000&sender=2348030000000&mobiles=2348020000000&type=call
#
#
#
# Fetching Data
# --
#
# http://portal.nigerianbulksms.com/api/?username=user&password=pass&action=
# balance
#
#
# Other Parameter
#
#
#  1. login     = Perform account login to validate a user
#
#  2. profile   = The customer profile of the account
#
#  3. balance   = The current balance on the account
#
#  4. contacts  = The type (text, call, tts ) of the message
#
#  5. numbers   = The saved numbers on the account
#
#  6. groups    = The groups on the account
#
#  7. audios    = The saved audios on the account
#
#  8. history   = The message history on the account
#
#  9. scheduled = The scheduled messages on the account
#
# 10. reports   = The delivery reports on the account
#
# 11. payments  = The payment history on the account
#
#
#
# Error Codes:
#
# 000 = Request successful
# 100 = Incomplete request parameters
# 101 = Request denied
#
# 110 = Login status failed
# 111 = Login status denied
#
# 120 = Message limit reached
# 121 = Mobile limit reached
# 122 = Sender limit reached
#
# 130 = Sender prohibited
# 131 = Message prohibited
#
# 140 = Invalid price setup
# 141 = Invalid route setup
# 142 = Invalid schedule date
#
# 150 = Insufficient funds
# 151 = Gateway denied access
# 152 = Service denied access
#
# 160 = File upload error
# 161 = File upload limit
# 162 = File restricted
#
# 190 = Maintenance in progress
# 191 = Internal error
#
