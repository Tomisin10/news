from flask import Flask
from werkzeug.contrib.fixers import ProxyFix
from werkzeug.contrib.cache import SimpleCache

from application.core import setup_core_tools
from application.core.async import make_celery
from application.core.utils.template_helpers import setup_template_filters
from application.core.utils.contexts.handlers import after_every_request
from application.core.utils.contexts.handlers import before_every_request
from config import load_configuration


cache = SimpleCache()


def _bind_request_contexts_handlers(app, blueprint):
    # Bind before and after request context handlers to blueprint

    app.before_request_funcs.setdefault(
        blueprint.name, []).insert(0, before_every_request)
    app.after_request_funcs.setdefault(
        blueprint.name, []).insert(0, after_every_request)


def _setup_blueprints(app):
    from application.modules import (
        api_blueprint, bots_blueprint, oauth_blueprint,
        documentation_blueprint, web_blueprint, ussd_blueprint)

    app.register_blueprint(api_blueprint)
    app.register_blueprint(bots_blueprint)
    app.register_blueprint(documentation_blueprint)
    app.register_blueprint(oauth_blueprint)
    app.register_blueprint(ussd_blueprint)
    app.register_blueprint(web_blueprint)

    _bind_request_contexts_handlers(app, api_blueprint)
    _bind_request_contexts_handlers(app, bots_blueprint)
    _bind_request_contexts_handlers(app, documentation_blueprint)
    _bind_request_contexts_handlers(app, oauth_blueprint)
    _bind_request_contexts_handlers(app, ussd_blueprint)
    _bind_request_contexts_handlers(app, web_blueprint)


def create_app(config_mode):
    """Create a Flask application instance."""

    # Create application instance;
    # Get the configuration settings
    # for instance and update it
    app = Flask(__name__, instance_relative_config=True,
                template_folder='../static/templates',
                static_folder='../static')

    app.wsgi_app = ProxyFix(app.wsgi_app)

    load_configuration(app, config_mode)
    setup_core_tools(app)
    setup_template_filters(app)
    celery = make_celery(app)
    _setup_blueprints(app)

    return app, celery
