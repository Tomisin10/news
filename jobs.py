from dateutil import parser as date_parser
import json
import random

from apscheduler.schedulers.blocking import BlockingScheduler
from pymysql.err import IntegrityError as PyMySQLIntegrityError
from sqlalchemy.orm.exc import DetachedInstanceError
from sqlalchemy.exc import (
    InternalError,
    IntegrityError as SQLAlchemyIntegrityError,
    SQLAlchemyError,
    DataError)

from app import application
from application.articles.helpers import get_general_headlines
from application.briefs.helpers import (
    create_brief_for_reader,
    send_brief_to_sms_reader,
    send_brief_to_email_reader,
    push_to_social_media)
from application.core import db
from application.core.constants import (
    ACTIVE_STATUS_ID, APP_NAME, NEWS_SOURCES_CATEGORIES, PENDING_STATUS_ID)
from application.core.models import (
    Event, EventLocation, Payment, Reader, Section, Video, VideoChannel,
    VideoSource)
from application.core.models.helpers import orm_get_new_sources_by_category
from application.gateways.africas_talking_wrapper import initiate_mobile_payment
from application.wrappers import rss, wordpress, news_api
from application.wrappers.eventbrite import (
    get_events_for_location, get_venue_details)
from application.wrappers.youtube import youtube_search


SOURCE_CATEGORY_MODULE = {
    'RSS': rss,
    'WORDPRESS': wordpress,
    'NEWS_API': news_api
}


sched = BlockingScheduler()


# @sched.scheduled_job('interval', seconds=10)
# def timed_job():
#     print('This job is run every 10 seconds.')


# @sched.scheduled_job('cron', day_of_week='mon-fri', hour=17)
# def scheduled_job():
#     print('This job is run every weekday at 5pm.')


@sched.scheduled_job('cron', hour=7)
def push_quote_of_the_day():
    pass


@sched.scheduled_job('cron', hour=14)
def push_word_of_the_day():
    pass


@sched.scheduled_job('cron', hour=7)
def push_morning_headlines():
    with application.app_context():
        brief_sender_funcs = {
            'USSD': send_brief_to_sms_reader,
            'Email': send_brief_to_email_reader
        }

        for reader in Reader.prepare_query_for_active().all():
            brief = create_brief_for_reader(reader)

            brief_sender_funcs[reader.disruptiv_platform.name](brief, reader)


@sched.scheduled_job('cron', hour=18)
# @sched.scheduled_job('interval', minutes=1)
def push_evening_headlines():
    with application.app_context():
        brief_sender_funcs = {
            'USSD': send_brief_to_sms_reader,
            'Email': send_brief_to_email_reader
        }

        for reader in Reader.prepare_query_for_active().all():
            brief = create_brief_for_reader(reader)

            brief_sender_funcs[reader.disruptiv_platform.name](brief, reader)


@sched.scheduled_job(
    'interval', minutes=application.config['LATEST_HEADLINES_PUSH_INTERVAL'])
def push_latest_headlines():
    with application.app_context():
        headlines = get_general_headlines()

        print('Gathered %s' % headlines)

        for article in headlines:
            push_to_social_media(article)

    # push_to_email_subscribers()
    # push_to_bot_users()


# @sched.scheduled_job(
#     'interval', minutes=application.config['NEWS_CRAWL_INTERVAL'])
def crawl_all_sites_old():
    with application.app_context():
        db.init_app(application)
        db.Model.metadata.reflect(db.engine)  # load existing DB schema
        # db.create_all()

        for category in NEWS_SOURCES_CATEGORIES:
            news_sources = orm_get_new_sources_by_category(category)

            for source in news_sources:

                try:
                    url = source.url
                    news_source_id = source.id
                    section_id = source.section.id

                    print('FETCHING FROM %s' % url)
                    module = SOURCE_CATEGORY_MODULE[category]
                    module.fetch_from_url(url, news_source_id, section_id)

                except (
                    InternalError, SQLAlchemyError,
                    DetachedInstanceError, DataError
                ):
                    print('FAILED (SQLALCHEMY SESSION ISSUE: %s)' % url)

                except KeyError:
                    print('KEY ERROR at %s' % url)


@sched.scheduled_job('interval', minutes=application.config[
    'EVENTS_CRAWL_INTERVAL'])
def crawl_events():
    with application.app_context():
        db.init_app(application)
        db.Model.metadata.reflect(db.engine)  # load existing DB schema
        # db.create_all()

        event_locations = EventLocation.query.all()

        for location in event_locations:
            events_in_location = get_events_for_location(location.name)

            for event in events_in_location:
                try:
                    Event(
                        event_location=location,
                        name=event['name']['text'],
                        date=date_parser.parse(event['start']['utc']),
                        venue=json.dumps(
                            get_venue_details(event['venue_id']))
                    ).save()
                except (PyMySQLIntegrityError, SQLAlchemyIntegrityError):
                    pass


@sched.scheduled_job(
    'interval', minutes=application.config['NEWS_CRAWL_INTERVAL'])
def crawl_youtube_channels():
    with application.app_context():
        db.init_app(application)
        db.Model.metadata.reflect(db.engine)  # load existing DB schema

        video_source_youtube_orm = VideoSource.get(name='YouTube')

        youtube_channels = VideoChannel.prepare_query_for_active(
            video_source_id=video_source_youtube_orm.id
        ).all()

        for channel in youtube_channels:
            videos, channels, playlists = list(
                youtube_search(channel_id=channel.channel_uid).values()
            )

            for video in videos:
                try:
                    Video(
                        title=video['title'],
                        description=video['description'],
                        thumbnail_url=video['thumbnail'],
                        created_at=date_parser.parse(video['date']),
                        video_channel_id=channel.id,
                        video_uid=video['id'],
                        channel_name=video['channel_title']
                    ).save()

                except (PyMySQLIntegrityError, SQLAlchemyIntegrityError):
                    pass


@sched.scheduled_job(
    'interval', minutes=application.config['NEWS_CRAWL_INTERVAL'])
def crawl_all_sites():
    with application.app_context():
        db.init_app(application)
        db.Model.metadata.reflect(db.engine)  # load existing DB schema

        sections = Section.prepare_query_for_active().all()

        for section in sections:
            print('Fetching for {}'.format(section.name))

            fetched_articles = []

            for source in section.news_sources:
                url = source.url

                module = SOURCE_CATEGORY_MODULE[source.category]
                if 'reachnaija' in url:
                    continue

                articles = module.fetch_from_url(url, source.id, section.id)

                if articles is None:
                    continue

                for article in articles:
                    fetched_articles.append((module, article, source.id))

            random.shuffle(fetched_articles)

            for module, article, source_id in fetched_articles:
                module.save_post(article, source_id, section.id)


# @sched.scheduled_job(
#     'interval', seconds=application.config['MOBILE_PAYMENT_PUSH_INTERVAL']
# )
def push_all_pending_mobile_payment_requests():
    with application.app_context():
        try:
            db.init_app(application)
            db.Model.metadata.reflect(db.engine)  # load existing DB schema

            pending_payments = Payment.query.filter_by(
                status_id=PENDING_STATUS_ID).all()

            for payment in pending_payments:
                print('About pushing {amount} to {phone}'.format(
                    amount=payment.amount, phone=payment.phone))

                initiate_mobile_payment(
                    payment.username,
                    payment.product_name,
                    payment.phone,
                    payment.currency_code,
                    float(payment.amount),
                    json.loads(payment.meta),
                    payment.environment,
                    payment.provider_channel
                )

        except:
            pass


# push_latest_headlines()
sched.start()
# crawl_all_sites()

# crawl_youtube_channels()
# push_morning_headlines()
# push_all_payment_requests()
# push_events()
