import requests


URL_PREFIX = 'api/v1.0'
BASE_URL = 'http://127.0.0.1:5000/{}'.format(URL_PREFIX)


def create_reader(disruptiv_platform_id, first_name=None, last_name=None,
        email=None, phone=None):

    url = '{}/readers'.format(BASE_URL)

    payload = {
        'phone': phone,
        'disruptiv_platform_id': disruptiv_platform_id,
        'first_name': first_name,
        'last_name': last_name,
        'email': email
    }

    return requests.post(url, json=payload).content.decode()


def edit_reader(reader_uid, **payload):
    url = '{}/readers/{}'.format(BASE_URL, reader_uid)

    return requests.patch(url, json=payload).content.decode()


def create_subscription(reader_uid, section_uid):
    url = '{}/readers/{}/subscriptions'.format(BASE_URL, reader_uid)

    payload = {
        'section_uid': section_uid
    }

    return requests.post(url, json=payload).content.decode()


def get_section_headlines(section):
    url = '{}/sections/{}/headlines'.format(BASE_URL, section)

    return requests.get(url).content.decode()


def get_source_headlines(source):
    url = '{}/sources/{}/headlines'.format(BASE_URL, source)

    return requests.get(url).content.decode()


if __name__ == '__main__':
    print(
        edit_reader(
            reader_uid='8bzGmSSoYcnSoeHAaTqJY2',
            phone='07085121508',
            disruptiv_platform_id=1
        )
    )

    # get_section_headlines('WORLD')
    # get_source_headlines('WORLD')
    # print(
    #     create_subscription(reader_uid='SJmwVrTzaEie77NAJnYMFb',
    #                         section_uid=2)
    # )
